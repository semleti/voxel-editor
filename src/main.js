var renderer, scene, camera;
var stage,colorPalette;
var stats;
var container = document.getElementById('container');

var cameraPitch = 0;
var cameraYaw = 0;
var invertPitch = true;
var invertYaw = true;
var pitchSensivity = 1;
var yawSensivity = 1;
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2(1,1);

var CUBEvertices = new Float32Array( [
    -1.0,-1.0,-1.0,
    -1.0,-1.0, 1.0,
    -1.0, 1.0, 1.0,
    1.0, 1.0,-1.0,
    -1.0,-1.0,-1.0,
    -1.0, 1.0,-1.0,
    1.0,-1.0, 1.0,
    -1.0,-1.0,-1.0,
    1.0,-1.0,-1.0,
    1.0, 1.0,-1.0,
    1.0,-1.0,-1.0,
    -1.0,-1.0,-1.0,
    -1.0,-1.0,-1.0,
    -1.0, 1.0, 1.0,
    -1.0, 1.0,-1.0,
    1.0,-1.0, 1.0,
    -1.0,-1.0, 1.0,
    -1.0,-1.0,-1.0,
    -1.0, 1.0, 1.0,
    -1.0,-1.0, 1.0,
    1.0,-1.0, 1.0,
    1.0, 1.0, 1.0,
    1.0,-1.0,-1.0,
    1.0, 1.0,-1.0,
    1.0,-1.0,-1.0,
    1.0, 1.0, 1.0,
    1.0,-1.0, 1.0,
    1.0, 1.0, 1.0,
    1.0, 1.0,-1.0,
    -1.0, 1.0,-1.0,
    1.0, 1.0, 1.0,
    -1.0, 1.0,-1.0,
    -1.0, 1.0, 1.0,
    1.0, 1.0, 1.0,
    -1.0, 1.0, 1.0,
    1.0,-1.0, 1.0
] );


function init(){
    // on initialise le moteur de rendu
    renderer = new THREE.WebGLRenderer();

     if ( renderer.extensions.get( 'ANGLE_instanced_arrays' ) === false ) {
				console.log("NOT SUPPORTED");
				return;
			}

    // si WebGL ne fonctionne pas sur votre navigateur vous pouvez utiliser le moteur de rendu Canvas à la place
    // renderer = new THREE.CanvasRenderer();
    renderer.setClearColor( 0x101010 );
    renderer.setSize( container.offsetWidth, container.offsetHeight );
    //useless?
    //renderer.setFaceCulling(THREE.CullFaceFront,THREE.FrontFaceDirectionCw);

    container.appendChild(renderer.domElement);

    // on initialise la scène
    scene = new THREE.Scene();

    // on initialise la camera que l’on place ensuite sur la scène
    camera = new THREE.PerspectiveCamera(50, container.offsetWidth / container.offsetHeight, 1, 10000 );
    camera.position.set(0, 0, 1000);
    scene.add(camera);

    // on créé un  cube au quel on définie un matériau puis on l’ajoute à la scène
    var cubeGeometry = new THREE.CubeGeometry( 100, 100, 100 );
    var wireframeMaterial = new THREE.MeshPhongMaterial( { color: 0xff0000, wireframe: false } );
    var cubeMesh = new THREE.Mesh( cubeGeometry, wireframeMaterial );
    //scene.add( cubeMesh );

    var planeGeometry = new THREE.PlaneGeometry( 2000, 2000, 20, 20 );
    var planeMaterial = new THREE.MeshPhongMaterial( { color: 0x0000ff, wireframe: true } );

    var planeMeshBack = new THREE.Mesh( planeGeometry, planeMaterial );
    planeMeshBack.position.set(0,0,-1000);
    planeMeshBack.setRotationFromAxisAngle ( new THREE.Vector3(0,0,1), Math.PI );
    scene.add( planeMeshBack );

    var planeMeshRight = new THREE.Mesh( planeGeometry, planeMaterial );
    planeMeshRight.position.set(1000,0,0);
    planeMeshRight.setRotationFromAxisAngle ( new THREE.Vector3(0,1,0), -Math.PI/2 );
    scene.add( planeMeshRight );

    var planeMeshBottom = new THREE.Mesh( planeGeometry, planeMaterial );
    planeMeshBottom.position.set(0,-1000,0);
    planeMeshBottom.setRotationFromAxisAngle ( new THREE.Vector3(1,0,0), -Math.PI/2 );
    scene.add( planeMeshBottom );

    //lights
    var ambientLight = new THREE.AmbientLight( 0xA0A080 );
    scene.add( ambientLight );

    var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
    directionalLight.position.set(500,500,500);
    scene.add( directionalLight );



    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    stats.domElement.style.right = '0px';
    container.appendChild(stats.domElement);

    //createInstancedCubes(100,100,100,new THREE.Vector3(0,0,0));
    //createInstancedCubes(100,100,100,new THREE.Vector3(240,0,0));

   //var chunk = new Chunk(100,100,100,0,0,0);
   //var chunk2 = new Chunk(100,100,100,200,0,0);

   //scene.add(chunk.mesh);
   //scene.add(chunk2.mesh);

    stage = new Stage();
    //the more individual chunks, the more time intensive (bottleneck transfer of data?)
    stage.addModel(4,4,4,32,32,32);

    // on effectue le rendu de la scène
    window.requestAnimationFrame(render);
}

var SCALE = 100;

//worst case scenario: >12 million cubes (>2/3 space filled so no cubes are culled(2 full layers, 1 air, 2 ful, 1 air....) + some on the edges) if terrain is 256*256*256
//problem: only <10 FPS
//maybe if subdivided in chunks FPS boost?
//if not, might enable to cull entire chunks (definitely able to cull what is off-screen, so that's a huge+)





function createInstancedCubes(x,y,z,pos)
{
    var triangles = 12;
    var instances = x*y*z;

    var geometry = new THREE.InstancedBufferGeometry();

    geometry.maxInstancedCount = instances;


    var verticesArray = [0,0,0, 0,0,1, 0,1,0, 0,1,1, 1,0,0, 1,0,1, 1,1,0, 1,1,1];
    var vertices = new THREE.BufferAttribute( new Float32Array( verticesArray, 0, verticesArray.length ), 3 );
    geometry.addAttribute( 'position', vertices );


    var indicesArray = [0,1,3, 6,0,2, 5,0,4, 6,4,0, 0,3,2, 5,1,0, 3,1,5, 7,4,6, 4,7,5, 7,6,2, 7,2,3, 7,3,5];
    var indices = new Uint16Array(indicesArray, 0, indicesArray.length);
    geometry.setIndex(new THREE.BufferAttribute(indices, 1));

 
    var offsets = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    var i = 0;
    for(var ix = 0; ix < x; ix++)
    {
        for(var iy = 0; iy < y; iy++)
        {
            for(var iz = 0; iz < z; iz++)
            {
                //ix*y*z+iy*z+iz
                offsets.setXYZ( i, ix, iy, iz );
                i++;
            }
        }
    }
    geometry.addAttribute( 'offset', offsets );



    var colors = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    for ( var i = 0, ul = colors.count; i < ul; i++ ) {
        var r = Math.random();
        var g = Math.random();
        var b = Math.random();
        colors.setXYZ( i, r, g, b);
    }
    geometry.addAttribute( 'color', colors );


    // material
    var material = new THREE.RawShaderMaterial( {
 
        /*uniforms: {
            time: { value: 1.0 },
            sineTime: { value: 1.0 }
        },*/
        vertexShader: document.getElementById( 'vertexShader' ).textContent,
        fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
        side: THREE.BackSide,
        transparent: false

    } );

    var mesh = new THREE.Mesh( geometry, material );
    mesh.geometry.computeBoundingSphere ();
    scene.add(mesh);
    mesh.position.set(pos.x,pos.y,pos.z);

}



//probably only worth it if divides triangles by over 6(could happen if a lot of cubes have 4 neighbours(difficult))
//maybe optimize rotation code?
//maybe one buffer per rotation?
//one issue is the duplication of data: color, offset -> instancedBufferAttribute?
//not worth investigating right now, maybe later when I try to squeeze every drop of power of the gpu
function createInstancedTriangles(x,y,z,pos)
{
    var triangles = 12;
    var instances = x*y*z*6;

    var geometry = new THREE.InstancedBufferGeometry();

    geometry.maxInstancedCount = instances;

    var verticesArray = [-1,-1,-1, -1,-1,1, -1,1,-1, -1,-1,1, -1,1,1, -1,1,-1];
    var vertices = new THREE.BufferAttribute( new Float32Array( verticesArray, 0, verticesArray.length ), 3 );
    geometry.addAttribute( 'position', vertices );

 
    var offsets = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    var i = 0;
    for(var ix = 0; ix < x; ix++)
    {
        for(var iy = 0; iy < y; iy++)
        {
            for(var iz = 0; iz < z; iz++)
            {
                //ix*y*z+iy*z+iz
                offsets.setXYZ( i, ix, iy, iz );
                offsets.setXYZ( i+1, ix, iy, iz );
                offsets.setXYZ( i+2, ix, iy, iz );
                offsets.setXYZ( i+3, ix, iy, iz );
                offsets.setXYZ( i+4, ix, iy, iz );
                offsets.setXYZ( i+5, ix, iy, iz );
                i+=6;
            }
        }
    }
    geometry.addAttribute( 'offset', offsets );



    var colors = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    for ( var i = 0, ul = colors.count; i < ul; i+=6 ) {
        var r = Math.random();
        var g = Math.random();
        var b = Math.random();
        colors.setXYZ( i, r, g, b);
        colors.setXYZ( i+1, r, g, b);
        colors.setXYZ( i+2, r, g, b);
        colors.setXYZ( i+3, r, g, b);
        colors.setXYZ( i+4, r, g, b);
        colors.setXYZ( i+5, r, g, b);
    }
    geometry.addAttribute( 'color', colors );

    var rotations = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    for ( var i = 0, ul = rotations.count; i < ul; i+=6 ) {
        rotations.setXYZ( i, 0, 0, 0);
        rotations.setXYZ( i+1, 1, 0, 0);
        rotations.setXYZ( i+2, 0, 1, 0);
        rotations.setXYZ( i+3, 0, 0, 1);
        rotations.setXYZ( i+4, 0, 0, 0);
        rotations.setXYZ( i+5, 0, 0, 0);
    }
    geometry.addAttribute( 'rotation', rotations );


    // material
    var material = new THREE.RawShaderMaterial( {
 
        /*uniforms: {
            time: { value: 1.0 },
            sineTime: { value: 1.0 }
        },*/
        vertexShader: document.getElementById( 'vertexShaderTriangles' ).textContent,
        fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
        side: THREE.BackSide,
        transparent: false

    } );

    var mesh = new THREE.Mesh( geometry, material );
    mesh.geometry.computeBoundingSphere ();
    scene.add(mesh);
    mesh.position.set(pos.x,pos.y,pos.z);

}









var previousIntersects = [];
function raycastHover()
{
    raycaster.setFromCamera( mouse, camera );

	// calculate objects intersecting the picking ray
	var intersects = raycaster.intersectObjects( scene.children );

    for ( var i = 0; i < previousIntersects.length; i++ ) {

		previousIntersects[ i ].object.material.color.set( 0x0000ff );
	}

	for ( i = 0; i < intersects.length && i < 1; i++ ) {

		intersects[ i ].object.material.color.set( 0xffff00 );
        console.log(intersects[ i ].point);
	}
    previousIntersects = intersects;
}



function render()
{
    //console.time("renderingTheGame");
    renderer.render( scene, camera );
    //console.timeEnd("renderingTheGame");
    requestAnimationFrame( render );

    stats.update();
}


class Stage {
    constructor()
    {
        this.models = [];
    }

    addModel(width,height,depth, chunkWidth,chunkHeight,chunkDepth)
    {
        this.models.push(new Model(width,height,depth, chunkWidth,chunkHeight,chunkDepth));
    }

    raycast()
    {
        raycaster.setFromCamera( mouse, camera );

        // calculate objects intersecting the picking ray
        var intersects = raycaster.intersectObjects( this.models);
    }
}



class Model {

    constructor(width,height,depth, chunkWidth,chunkHeight,chunkDepth)
    {
        this.chunks = [];
        var cubeGeometry = new THREE.CubeGeometry( 10, 10, 10 );
        var material = new THREE.MeshPhongMaterial( { color: 0xff0000, wireframe: false } );
        this.cube = new THREE.Mesh( cubeGeometry, material );
        scene.add(this.cube);

        //the models Object3D
        this.object3D = new THREE.Object3D();
        this.object3D.position.set(0,0,0);
        scene.add(this.object3D);

        //store the chunks preferred dims
        this.chunkWidth = chunkWidth;
        this.chunkHeight = chunkHeight;
        this.chunkDepth = chunkDepth;
        //store the desired dims
        //TODO: alllow for dimensions in voxels, not only chunks
        this.width = width;
        this.height = height;
        this.depth = depth;

        this.init();
    }

    addChunk(width,height,depth, x,y,z)
    {
        var chunk = new Chunk(width,height,depth, x,y,z);
        this.chunks.push(chunk);
        this.object3D.add(chunk.mesh);
    }

    raycast()
    {
        raycaster.setFromCamera( mouse, camera );

        // calculate objects intersecting the picking ray
        



        var inverseMatrix = new THREE.Matrix4();
		var ray = new THREE.Ray();
		var matrixWorld = this.object3D.matrixWorld;
		var sphere = new THREE.Sphere();

        sphere.copy( this.boundingSphere );
        sphere.applyMatrix4( matrixWorld );

        if ( raycaster.ray.intersectsSphere( sphere ) !== false )
        {

            inverseMatrix.getInverse( matrixWorld );
            ray.copy( raycaster.ray ).applyMatrix4( inverseMatrix );

            // Check boundingBox before continuing

            if ( this.boundingBox !== null ) {

                if ( ray.intersectsBox( this.boundingBox ))
                {
                    var intersects = raycaster.intersectObjects( this.chunks);
                    if(intersects.length > 0)
                        this.cube.position.set(intersects[0].hit.x,intersects[0].hit.y,intersects[0].hit.z);

                    for ( var i = 0; i < intersects.length; i++ ) {

                        /*console.log("intersected: " + i);
                        console.log(intersects[i]);*/
            
                    }
                }

            }


            
        }





    }

    init()
    {
        for(var x = 0; x < this.width; x++)
        {
            for(var y = 0; y < this.height; y++)
            {
                for(var z = 0; z < this.depth; z++)
                {
                    this.addChunk(this.chunkWidth,this.chunkHeight,this.chunkDepth,x*this.chunkWidth,y*this.chunkHeight,z*this.chunkDepth);
                }
            }
        }
        this.setBoundingBox();
    }

    getChunkIndexAt(x,y,z)
    {
        return x * this.height * this.depth + y * this.depth + z;
    }

    getChunkAt(x,y,z)
    {
        return this.chunks[this.getChunkIndexAt(x,y,z)];
    }

    setBoundingBox()
    {
        var width = this.width*this.chunkWidth;
        var height = this.height*this.chunkHeight;
        var depth = this.depth*this.chunkDepth;
        this.boundingBox = new THREE.Box3(new THREE.Vector3(0.0,0.0,0.0), new THREE.Vector3(width,height,depth));
        //required to prevent too early culling
        this.boundingSphere = new THREE.Sphere(new THREE.Vector3(width/2.0,height/2.0,depth/2.0), Math.sqrt(width*width+height*height+depth*depth)/2.0);
    }

}









class Chunk {

    



    constructor(width, height, depth, x, y, z) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.c_voxelCount = this.width*this.height*this.depth;
        this.voxels = new Uint8Array(this.c_voxelCount);
        this.voxelsSolid = [this.c_voxelCount];
        //might be smaller if chunks have a max size (ie: Uint16 if chunk:32*32*32 which sound reasonnable)
        //used to reduce memory swapping when voxels are added/removedfrom/to the gpu_buffer
        //might be a pain in the ass to resize, so maybe do a "virtual" resize if the user wants a smaller chunk to work with?
        this.voxelsBufferIndices = new Uint32Array(this.c_voxelCount);
        this.voxelsReverseBufferIndices = new Uint32Array(this.c_voxelCount);
        this.voxelsVisible = 0;
        //might want to change to a Vector3 array
        //this.palette = new Float32Array(255 * 3);
        this.palette = [0.2,0.2,0.2, 1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0, 1.0,1.0,1.0, 1.0,1.0,0.0, 1.0,0.0,1.0, 0.0,1.0,1.0];
        //used to track wether the chunk has been dirtied or not
        this.dirty = true;


        //worst case scenario: 1/2 of the space is filled (checker pattern) so at max 1/2 of cubes visible
        //GOD DAMMIT!: if 2 full layers + 1 air layer + 2 full layers... a lot more than 1/2! (> 2/3, even with smart culling(not accounting for rotation, but accounting for rotation might be too much work for cpu + constant gpu buffer updating is bad))
        //might need an advanced culling feature
        //let's ignore culling for now!
        this.g_maxInstances = this.width*this.height*this.depth;


        this.geometry = new THREE.InstancedBufferGeometry();
        this.geometry.maxInstancedCount = this.g_maxInstances;
        //the "position"(offest) of each voxel relative to the chunk, needs 3 Uint8 per voxel but glsl wants floats, so there we go
        this.offsets = new THREE.InstancedBufferAttribute( new Float32Array( this.g_maxInstances * 3 ), 3, 1 );
        this.geometry.addAttribute( 'position', Chunk.vertices );
        this.geometry.setIndex(Chunk.indices);
        this.geometry.addAttribute( 'offset', this.offsets );
        //color for each voxel
        //need 3 floats per color
        //this.colors = new THREE.InstancedBufferAttribute( new Float32Array( this.g_maxInstances * 3 ), 3, 1 );
        //this.geometry.addAttribute( 'color', this.colors );

        this.colorIndices = new THREE.InstancedBufferAttribute( new Uint8Array( this.g_maxInstances), 1, 1 );
        this.geometry.addAttribute('colorIndex', this.colorIndices);

        this.mesh = new THREE.Mesh( this.geometry, Chunk.material );

        //doesn't seem to work, might have to do it manually
        //it's probably because THREEjs isn't aware of the offset
        //this.mesh.geometry.computeBoundingBox();

        this.mesh.position.set(x, y, z);

        this.init();

        
    }

    //initialize some values for testing //temporary
    init()
    {

        //initialize offsets //temporary for testing
        //console.time("first pass");
        for(var ix = 0; ix < this.width; ix++)
        {
            for(var iy = 0; iy < this.height; iy++)
            {
                for(var iz = 0; iz < this.depth; iz++)
                {
                    this.setVoxelAt(ix,iy,iz,THREE.Math.randInt(0,1)*THREE.Math.randInt(1,256)-1);
                }
            }
        }
        //console.timeEnd("first pass");
        this.setBoundingBox();
        this.cleanUp();

        
    }



    //index cpu side
    C_getVoxelIndexAt(x,y,z)
    {
        return x*this.height*this.depth + y*this.depth + z;
    }

    //index gpu side
    G_getVoxelBufferIndexAt(x,y,z)
    {
        return this.voxelsBufferIndices[this.C_getVoxelIndexAt(x,y,z)];
    }

    //returns the color of the voxel (0 is air)
    C_getVoxelAt(x,y,z)
    {
        return this.voxels[getVoxelIndexAt(x,y,z)];
    }

    //color 0 is invisible/disabled/air
    //up to 255 colors since voxels is Uint8Array
    //maybe later on more? gotta check performance
    setVoxelAt(x,y,z,color)
    {
        //get and store the cpu index
        var c_index = this.C_getVoxelIndexAt(x,y,z);
        if(color == -1)
        {
            //don't want to remove an already removed voxel
            //if(this.voxels[c_index] != 0)
            if(this.voxelsSolid[c_index])
            {
                this.removeVoxelAt(x,y,z);
            }
        }
        else
        {
            //voxel was previously air, add it
            //if(this.voxels[c_index] == 0)
            if(!this.voxelsSolid[c_index])
            {
                this.addVoxelAt(x,y,z,color);
            }
            //voxel already had a color, update it
            else
            {
               this.setVoxelColorAt(x,y,z,color);
            }
        }
        
        
    }



    addVoxelAt(x,y,z,color)
    {
        this.G_addVoxelAt(x,y,z,color);
        var c_index = this.C_getVoxelIndexAt(x,y,z);
        //set the voxel to solid
        this.voxelsSolid[c_index] = true;
        //set the color cpu side
        this.voxels[c_index] = color;

    }

    //do not call directly
    //safety checks are executed by setVoxelAt
    G_addVoxelAt(x,y,z,color)
    {
        //TODO
        //check if visible, else cull it
        //check neighbour culling
        var c_index = this.C_getVoxelIndexAt(x,y,z);
        this.offsets.setXYZ(this.voxelsVisible, x, y, z);
        var vecColor = this.getColorFromPalette(color);
        //this.colors.setXYZ(this.voxelsVisible, vecColor.x,vecColor.y,vecColor.z);
        this.colorIndices.setX(this.voxelsVisible,color);

        this.voxelsBufferIndices[c_index] = this.voxelsVisible+1;
        this.voxelsReverseBufferIndices[this.voxelsVisible+1] = c_index;

        /*this.voxelsBufferIndices[this.voxelsVisible+1] = c_index;
        this.voxelsReverseBufferIndices[c_index] = this.voxelsVisible+1;*/
        this.voxelsVisible++;
        this.dirty = true;
    }

    setVoxelColorAt(x,y,z,color)
    {
        this.G_setVoxelColorAt(x,y,z,color);
        var c_index = this.C_getVoxelIndexAt(x,y,z);
        //set the color cpu side
        this.voxels[c_index] = color;
    }

    //do not call directly
    //safety checks are executed by setVoxelAt
    G_setVoxelColorAt(x,y,z,color)
    {
         var g_index = this.G_getVoxelBufferIndexAt(x,y,z);
        //if index is 0, the voxel has been culled or is air, so no need to set it's color
        if(g_index != 0)
        {
            var col = this.getColorFromPalette(color)
            //this.colors.setXYZ(g_index-1, col.x, col.y, col.z);
            this.colorIndices.setX(g_index-1,color);
            this.dirty = true;
        }

    }

    removeVoxelAt(x,y,z)
    {
        this.G_removeVoxelAt(x,y,z);
        var c_index = this.C_getVoxelIndexAt(x,y,z);
        //set the voxel to air
        this.voxelsSolid[c_index] = false;
        //no need to change the color since it's air
    }

    //do not call directly
    //safety checks are executed by setVoxelAt
    G_removeVoxelAt(x,y,z)
    {
        var g_index = this.G_getVoxelBufferIndexAt(x,y,z);
        var c_index = this.C_getVoxelIndexAt(x,y,z);
        //if index is 0, the voxel has been culled or is air, so no need to remove it
        if(g_index != 0 )
        {
            //g_index of last voxel rendered
            var g_index2 = this.voxelsVisible-1;
            //don't forget that g_buffer indices are 1 too big
            this.offsets.setXYZ(g_index-1, this.offsets.getX(g_index2), this.offsets.getY(g_index2), this.offsets.getZ(g_index2));
            //this.colors.setXYZ(g_index-1, this.colors.getX(g_index2), this.colors.getY(g_index2), this.colors.getZ(g_index2));
            this.colorIndices.setX(this.colorIndices.getX(g_index2));

            this.voxelsBufferIndices[this.voxelsReverseBufferIndices[g_index2+1]] = this.voxelsBufferIndices[c_index];
            this.voxelsReverseBufferIndices[this.voxelsBufferIndices[c_index]] = this.voxelsReverseBufferIndices[g_index2+1];

            this.voxelsBufferIndices[c_index] = 0;
            this.voxelsReverseBufferIndices[0] = c_index;
            
            this.voxelsVisible--;
            this.dirty = true;
        }
    }


    raycast( raycaster, intersects )
    {

        var inverseMatrix = new THREE.Matrix4();
		var ray = new THREE.Ray();
		var matrixWorld = this.mesh.matrixWorld;
		var sphere = new THREE.Sphere();

        sphere.copy( this.geometry.boundingSphere );
        sphere.applyMatrix4( matrixWorld );

        if ( raycaster.ray.intersectsSphere( sphere ) !== false )
        {

            inverseMatrix.getInverse( matrixWorld );
            ray.copy( raycaster.ray ).applyMatrix4( inverseMatrix );

            // Check boundingBox before continuing

            if ( this.mesh.geometry.boundingBox !== null ) {
                var hit = ray.intersectBox( this.mesh.geometry.boundingBox );

                if ( hit !== null )
                {
                    var distance = new THREE.Vector3().subVectors(ray.origin,hit).lengthSq();
                    //TODO: reduce compation cost by replacing Number(toFixed()) by something better
                    //might only send local hit, if anyone needs the world one, could convert themselves
                    //keep local hit
                    //hit.set(Number(hit.x.toFixed(8)),Number(hit.y.toFixed(8)),Number(hit.z.toFixed(8)));
                    var hitLocal = hit.clone();
                    //hit coordinates to world
                    hit.applyMatrix4(matrixWorld);
                    //hit.set(Number(hit.x.toFixed(8)),Number(hit.y.toFixed(8)),Number(hit.z.toFixed(8)));
                    intersects.push({object:this,distance:distance,hit:hit,hitLocal:hitLocal});

                    this.rayToVoxels(hitLocal,ray.direction);
                }

            }


            
        }
    }


    
    rayToVoxels(origin, direction)
    {
        //to make sure origin is an in bound voxel
        direction.normalize();
        origin.add(direction.clone().multiplyScalar(0.00000001))


        //the c * vector at which there is the next coordinate jump
        var cx,cy,cz;

        //store if ascending or descending coordinates
        //either -1 or 1
        var xunit,yunit,zunit;

        if(direction.x > 0)
        {
            cx = ((Math.ceil(origin.x) - origin.x)/direction.x);
            xunit = 1;
        }
        else
        {
            cx = ((origin.x-Math.floor(origin.x))/-direction.x);
            xunit = -1;
        }

        if(direction.y > 0)
        {
            cy = ((Math.ceil(origin.y) - origin.y)/direction.y);
            yunit = 1;
        }
        else
        {
            cy = ((origin.y-Math.floor(origin.y))/-direction.y);
            yunit = -1;
        }

        if(direction.z > 0)
        {
            cz = ((Math.ceil(origin.z) - origin.z)/direction.z);
            zunit = 1;
        }
        else
        {
            cz = ((origin.z-Math.floor(origin.z))/-direction.z);
            zunit = -1;
        }

        //get the first voxel coordinate
        var x = Math.floor(origin.x);
        var y = Math.floor(origin.y);
        var z = Math.floor(origin.z);

        //get the c unit to jump to the next coordinate
        var cxunit = 1.0/Math.abs(direction.x);
        var cyunit = 1.0/Math.abs(direction.y);
        var czunit = 1.0/Math.abs(direction.z);

        //the face hit by the ray
        var face;
        //get the first face
        //step back a voxel
        var icx = cx - cxunit;
        var icy = cy - cyunit;
        var icz = cz - czunit;
        //look for the closest voxel coord step (here the biggest since all < 0)
        if(icx >= icy && icx >= icz)face = xunit * 1;
        else if(icy >= icz)face = yunit * 2;
        else face = zunit * 3;
        


        var voxels = [];

        while(x >= 0 && x < this.width && y >= 0 && y < this.height && z >=0 && z < this.depth)
        {
            //push the previous voxel, it is in bounds
            //but only if it is solid
            if(this.voxelsSolid[this.C_getVoxelIndexAt(x,y,z)])
                voxels.push({coord:new THREE.Vector3(x,y,z),face:face});
            //check which direction is the nex to jump
            if(cx < cy && cx < cz)
            {
                x += xunit;
                cx += cxunit;
                //-1 or 1
                face = xunit * 1;
            }
            else if(cy < cz)
            {
                y += yunit;
                cy += cyunit;
                //-2 or 2
                face = yunit * 2;
            }
            else
            {
                z += zunit;
                cz += czunit;
                //-3 or 2
                face = zunit * 3;
            }
        }

        if(mouse.left)
        {
            // temp
            for(var i = 0; i < voxels.length; i++)
            {
                this.setVoxelAt(voxels[i].coord.x,voxels[i].coord.y,voxels[i].coord.z,colorPalette.selectedColor);
            }
            this.cleanUp();
            //temp
        }

        return voxels;
    }




    isVoxelVisibleAt(x,y,z)
    {

    }

    //get the color from the palette and return it as Vector3
    getColorFromPalette(color)
    {
        //color--;
        return colorPalette.colors[color];
    }

    //called after the chunk has been dirtied to clean it up
    //has a list of tasks to perform/check
    //updates the gpu buffers
    cleanUp()
    {
        if(this.dirty)
        {
            this.offsets.needsUpdate = true;
            //this.colors.needsUpdate = true;
            this.colorIndices.needsUpdate = true;
            this.geometry.maxInstancedCount = this.voxelsVisible;
            this.dirty = false;
            //might want to update bounding box
        }
    }

    setBoundingBox()
    {
        this.geometry.boundingBox = new THREE.Box3(new THREE.Vector3(0.0,0.0,0.0), new THREE.Vector3(this.width,this.height,this.depth));
        //required to prevent too early culling
        this.geometry.boundingSphere = new THREE.Sphere(new THREE.Vector3(this.width/2.0,this.height/2.0,this.depth/2.0), Math.sqrt(this.width*this.width+this.height*this.height+this.depth*this.depth)/2.0);
    }

}

//might change to triangles if culling is worth performance wise

//cube geometry, static because not going to change
//might give possibility to change that later on to give users the ability to customize voxel shape
//but boundingbox would need update, as well as exporting, so a LOT of work
Chunk.verticesArray = [0,0,0, 0,0,1, 0,1,0, 0,1,1, 1,0,0, 1,0,1, 1,1,0, 1,1,1];
Chunk.vertices = new THREE.BufferAttribute( new Float32Array( Chunk.verticesArray, 0, Chunk.verticesArray.length ), 3 );


Chunk.indicesArray = [0,1,3, 6,0,2, 5,0,4, 6,4,0, 0,3,2, 5,1,0, 3,1,5, 7,4,6, 4,7,5, 7,6,2, 7,2,3, 7,3,5];
Chunk.indices = new THREE.BufferAttribute(new Uint16Array(Chunk.indicesArray, 0, Chunk.indicesArray.length),1);






var movementSpeed = 10;

function onContainerKeyboard( event ) {
    //event.preventDefault();
    //left
    var mov = new THREE.Vector3(0,0,0);
    if(event.key == 'q')
    {
        mov.add(new THREE.Vector3(-1,0,0));
    }
    //right
    if(event.key == 'd') {
        mov.add(new THREE.Vector3(1,0,0));
    }
    //up
    if(event.key == 'a')
    {
        mov.add(new THREE.Vector3(0,1,0));
    }
    //down
    if(event.key == 'e') {
        mov.add(new THREE.Vector3(0,-1,0));
    }
    //forward
    if(event.key == 'z')
    {
        mov.add(new THREE.Vector3(0,0,-1));
    }
    //backward
    if(event.key == 's') {
        mov.add(new THREE.Vector3(0,0,1));
    }
    mov.multiplyScalar(movementSpeed);
    mov.applyEuler(camera.rotation);

    camera.position.add(mov);

}

function onContainerMouseWheel( event ) {
    //event.preventDefault();
    /*var zoom = -event.wheelDelta/120 * 4;
    camera.fov += zoom;
    camera.fov=clamp(camera.fov,1,170);
    camera.updateProjectionMatrix();*/
    var mov = new THREE.Vector3( 0, 0, -event.wheelDelta/120 * 40 );
    mov.applyEuler(camera.rotation);
    camera.position.add(mov);
}

//TODO:
//when mouse pressed and in container border, keep moving
function onContainerMouseMove( event ) {
    event.preventDefault();

    //middle mouse button
    //move camera left/right up/down
    if ( event.buttons & 4  ) {
        var mov = new THREE.Vector3( -(event.movementX), event.movementY, 0 );
        mov.applyEuler(camera.rotation);
        camera.position.add(mov);

    }
    //right mouse button
    //rotate camera
    if(event.buttons & 2) {
        cameraPitch = clamp(cameraPitch+event.movementY/100.0*pitchSensivity*(invertPitch?-1:1),-Math.PI/2,Math.PI/2);
        cameraYaw += event.movementX/100.0*yawSensivity*(invertYaw?-1:1);
        camera.setRotationFromEuler( new THREE.Euler(cameraPitch,cameraYaw,0,'YXZ'));
    }
    //left mouse button
    //move camera forward/backwards
    /*if(event.buttons & 1) {
        var move = new THREE.Vector3( 0,0, -event.movementX + event.movementY);
        move.applyEuler(camera.rotation);
        camera.position.add(move);
    }*/



	mouse.x = ( event.offsetX / renderer.domElement.width ) * 2 - 1;
	mouse.y = - ( event.offsetY / renderer.domElement.height ) * 2 + 1;


    //raycastHover();
    stage.raycast();
}


function onContainerMouseDown(event)
{
    if(event.button == 0)
        mouse.left = true;
    else if(event.button == 1)
        mouse.middle = true;
    else if(event.button == 2 )
        mouse.right = true;
}

function onContainerMouseUp(event)
{
    if(event.button == 0)
        mouse.left = false;
    else if(event.button == 1)
        mouse.middle = false;
    else if(event.button == 2 )
        mouse.right = false;
}


function setupUI()
{
    renderer.domElement.addEventListener("mousedown",onContainerMouseDown);
    renderer.domElement.addEventListener("mouseup",onContainerMouseUp);

    renderer.domElement.addEventListener("mousemove",onContainerMouseMove);

    renderer.domElement.addEventListener("mousewheel", onContainerMouseWheel);

    //needs to be window to get key events
    window.onkeydown = onContainerKeyboard;

    //disable context menu in container
    renderer.domElement.addEventListener("contextmenu", function(e){
        e.preventDefault();
    }, false);

    
}


class ColorPalette
{
    constructor()
    {
        this.domElement = document.getElementById("colorPalette");
        this.colorInputs = [256];
        this.colors = [256];
        this.selectedColor = 0;
        var color = 0;
        for(var i = 0; i < 32; i++)
        {
            var line = document.createElement("div");
            line.className = "colorPaletteLine";
            this.domElement.appendChild(line);
            for(var j = 0; j < 8; j++)
            {
                var red = Math.floor(color/36)*36;
                var green = Math.floor(color%36/6)*51;
                var blue = (color%6)*51;

                var vecColor = new THREE.Vector3(red/255.0,green/255.0,blue/255.0);

                var colorInput = document.createElement("input");
                colorInput.type = "color";
                colorInput.className = "colorPaletteButton";
                colorInput.style.backgroundColor= "rgb(" + red +","+ green +","+ blue +")";
                colorInput.style.borderColor= "rgb(" + red +","+ green +","+ blue +")";
                colorInput.value = rgbToHex(red,green,blue);
                colorInput.vecColor = vecColor;
                colorInput.isSelected = false;


                line.appendChild(colorInput);
                colorInput.index = color;
                var thisref = this;
                colorInput.onchange = function(){
                    this.style.backgroundColor = this.value;
                    var col = hexToRGB(this.value);
                    this.vecColor = new THREE.Vector3(col.red/255.0,col.green/255.0,col.blue/255.0);
                    thisref.colors[this.index]=this.vecColor;
                };
                //first left click: select, second: open color picker
                //would prefer left select, right open BUT only able to use real left click event :/
                colorInput.addEventListener('click', function(ev) {
                    if(this.isSelected)
                    {
                        return true;
                    }
                    else
                    {
                        ev.returnValue = false;
                        if(ev.preventDefault)
                            ev.preventDefault();
                        thisref.setSelectedColor(this.index);
                        return false;
                    }
                    
                },false);
                //right click select
                colorInput.addEventListener('contextmenu', function(ev) {
                    ev.preventDefault();
                    thisref.setSelectedColor(this.index);
                    return false;
                }, false);
                this.colorInputs[color] = colorInput;

                this.colors[color] = vecColor;

                color++;
            }
        }
        this.colorInputs[0].click();
    }

    setSelectedColor(color)
    {
        this.colorInputs[this.selectedColor].classList.remove("colorPaletteSelected");
        this.colorInputs[this.selectedColor].isSelected = false;
        this.colorInputs[color].classList.add("colorPaletteSelected"); 
        this.colorInputs[color].isSelected = true;
        this.selectedColor = color;
    }

    

}


function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function hexToRGB(hex){
    hex = hex.replace('#','');
    r = parseInt(hex.substring(0,2), 16);
    g = parseInt(hex.substring(2,4), 16);
    b = parseInt(hex.substring(4,6), 16);
    return {red:r,green:g,blue:b};
}


function clamp(number,min,max)
{
    if(number<min)
        return min;
    if(number>max)
        return max;
    return number;
}

//helper function for arguments with default value
function pickValue(arg,def)
{
    return (typeof arg == 'undefined' ? def : arg);
}













colorPalette = new ColorPalette();

console.log(colorPalette.colors);
// static material
Chunk.material = new THREE.RawShaderMaterial( {
    uniforms: {
        colorPalette: {type: 'v3v', value: colorPalette.colors}
    },
    vertexShader: document.getElementById( 'vertexShader' ).textContent,
    fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
    side: THREE.FrontSide,
    transparent: false

} );



init();
setupUI();
window.requestAnimationFrame(render);