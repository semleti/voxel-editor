/// <reference path="Mouse.ts" />
/// <reference path="ColorPalette.ts" />
/// <reference path="ColorInput.ts" />
/// <reference path="Utils.ts" />
/// <reference path="Model.ts" />
/// <reference path="Stage.ts" />
/// <reference path="Globals.ts" />
/// <reference path="Inputs.ts" />
namespace VoxelEditor
{

let stats;
let container = document.getElementById('container');




/*let CUBEvertices = new Float32Array( [
    -1.0,-1.0,-1.0,
    -1.0,-1.0, 1.0,
    -1.0, 1.0, 1.0,
    1.0, 1.0,-1.0,
    -1.0,-1.0,-1.0,
    -1.0, 1.0,-1.0,
    1.0,-1.0, 1.0,
    -1.0,-1.0,-1.0,
    1.0,-1.0,-1.0,
    1.0, 1.0,-1.0,
    1.0,-1.0,-1.0,
    -1.0,-1.0,-1.0,
    -1.0,-1.0,-1.0,
    -1.0, 1.0, 1.0,
    -1.0, 1.0,-1.0,
    1.0,-1.0, 1.0,
    -1.0,-1.0, 1.0,
    -1.0,-1.0,-1.0,
    -1.0, 1.0, 1.0,
    -1.0,-1.0, 1.0,
    1.0,-1.0, 1.0,
    1.0, 1.0, 1.0,
    1.0,-1.0,-1.0,
    1.0, 1.0,-1.0,
    1.0,-1.0,-1.0,
    1.0, 1.0, 1.0,
    1.0,-1.0, 1.0,
    1.0, 1.0, 1.0,
    1.0, 1.0,-1.0,
    -1.0, 1.0,-1.0,
    1.0, 1.0, 1.0,
    -1.0, 1.0,-1.0,
    -1.0, 1.0, 1.0,
    1.0, 1.0, 1.0,
    -1.0, 1.0, 1.0,
    1.0,-1.0, 1.0
] );*/


function init():void
{
    // on initialise le moteur de rendu
    Globals.renderer = new THREE.WebGLRenderer();

     if ( Globals.renderer.extensions.get( 'ANGLE_instanced_arrays' ) === false ) {
				console.log("NOT SUPPORTED");
				return;
			}

    // si WebGL ne fonctionne pas sur votre navigateur vous pouvez utiliser le moteur de rendu Canvas à la place
    // renderer = new THREE.CanvasRenderer();
    Globals.renderer.setClearColor( 0x101010 );
    Globals.renderer.setSize( container.offsetWidth, container.offsetHeight );
    //useless?
    //renderer.setFaceCulling(THREE.CullFaceFront,THREE.FrontFaceDirectionCw);

    container.appendChild(Globals.renderer.domElement);


    // on initialise la camera que l’on place ensuite sur la scène
    Globals.camera = new THREE.PerspectiveCamera(50, container.offsetWidth / container.offsetHeight, 1, 10000 );
    Globals.camera.position.set(0, 0, 1000);
    Globals.scene.add(Globals.camera);

    // on créé un  cube au quel on définie un matériau puis on l’ajoute à la scène
    let cubeGeometry = new THREE.CubeGeometry( 100, 100, 100 );
    let wireframeMaterial = new THREE.MeshPhongMaterial( { color: 0xff0000, wireframe: false } );
    let cubeMesh = new THREE.Mesh( cubeGeometry, wireframeMaterial );
    //scene.add( cubeMesh );

    let planeGeometry = new THREE.PlaneGeometry( 2000, 2000, 20, 20 );
    let planeMaterial = new THREE.MeshPhongMaterial( { color: 0x0000ff, wireframe: true } );

    let planeMeshBack = new THREE.Mesh( planeGeometry, planeMaterial );
    planeMeshBack.position.set(0,0,-1000);
    planeMeshBack.setRotationFromAxisAngle ( new THREE.Vector3(0,0,1), Math.PI );
    Globals.scene.add( planeMeshBack );

    let planeMeshRight = new THREE.Mesh( planeGeometry, planeMaterial );
    planeMeshRight.position.set(1000,0,0);
    planeMeshRight.setRotationFromAxisAngle ( new THREE.Vector3(0,1,0), -Math.PI/2 );
    Globals.scene.add( planeMeshRight );

    let planeMeshBottom = new THREE.Mesh( planeGeometry, planeMaterial );
    planeMeshBottom.position.set(0,-1000,0);
    planeMeshBottom.setRotationFromAxisAngle ( new THREE.Vector3(1,0,0), -Math.PI/2 );
    Globals.scene.add( planeMeshBottom );

    //lights
    let ambientLight = new THREE.AmbientLight( 0xA0A080 );
    Globals.scene.add( ambientLight );

    let directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
    directionalLight.position.set(500,500,500);
    Globals.scene.add( directionalLight );



    stats = new Stats();
    //one or the other works, depending on Stats version?
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    stats.domElement.style.right = '0px';
    stats.domElement.style = "position:absolute;right:0px;top:0px;";
    container.appendChild(stats.domElement);

    //the more individual chunks, the more time intensive (bottleneck transfer of data?)
    //big chunks better?
    Globals.stage.addModel(32,32,32);

    // on effectue le rendu de la scène
    window.requestAnimationFrame(render);
}

//worst case scenario: >12 million cubes (>2/3 space filled so no cubes are culled(2 full layers, 1 air, 2 ful, 1 air....) + some on the edges) if terrain is 256*256*256
//problem: only <10 FPS
//maybe if subdivided in chunks FPS boost?
//if not, might enable to cull entire chunks (definitely able to cull what is off-screen, so that's a huge+)





function createInstancedCubes(x:number,y:number,z:number,pos:THREE.Vector3):void
{
    let triangles = 12;
    let instances = x*y*z;

    let geometry = new THREE.InstancedBufferGeometry();

    geometry.maxInstancedCount = instances;

    let verticesArray = new Float32Array([0,0,0, 0,0,1, 0,1,0, 0,1,1, 1,0,0, 1,0,1, 1,1,0, 1,1,1]);
    let vertices = new THREE.BufferAttribute( verticesArray, 3 );
    geometry.addAttribute( 'position', vertices );


    let indicesArray = new Uint16Array([0,1,3, 6,0,2, 5,0,4, 6,4,0, 0,3,2, 5,1,0, 3,1,5, 7,4,6, 4,7,5, 7,6,2, 7,2,3, 7,3,5]);
    geometry.setIndex(new THREE.BufferAttribute(indicesArray, 1));

 
    let offsets = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    let i = 0;
    for(let ix = 0; ix < x; ix++)
    {
        for(let iy = 0; iy < y; iy++)
        {
            for(let iz = 0; iz < z; iz++)
            {
                //ix*y*z+iy*z+iz
                offsets.setXYZ( i, ix, iy, iz );
                i++;
            }
        }
    }
    geometry.addAttribute( 'offset', offsets );



    let colors = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    for ( let i = 0, ul = colors.count; i < ul; i++ ) {
        let r = Math.random();
        let g = Math.random();
        let b = Math.random();
        colors.setXYZ( i, r, g, b);
    }
    geometry.addAttribute( 'color', colors );


    // material
    let material = new THREE.RawShaderMaterial( {
 
        /*uniforms: {
            time: { value: 1.0 },
            sineTime: { value: 1.0 }
        },*/
        vertexShader: document.getElementById( 'vertexShader' ).textContent,
        fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
        side: THREE.BackSide,
        transparent: false

    } );

    let mesh = new THREE.Mesh( geometry, material );
    mesh.geometry.computeBoundingSphere ();
    Globals.scene.add(mesh);
    mesh.position.set(pos.x,pos.y,pos.z);

}



//probably only worth it if divides triangles by over 6(could happen if a lot of cubes have 4 neighbours(difficult))
//maybe optimize rotation code?
//maybe one buffer per rotation?
//one issue is the duplication of data: color, offset -> instancedBufferAttribute?
//not worth investigating right now, maybe later when I try to squeeze every drop of power of the gpu
function createInstancedTriangles(x:number,y:number,z:number,pos:THREE.Vector3):void
{
    let triangles = 12;
    let instances = x*y*z*6;

    let geometry = new THREE.InstancedBufferGeometry();

    geometry.maxInstancedCount = instances;

    let verticesArray = new Float32Array([-1,-1,-1, -1,-1,1, -1,1,-1, -1,-1,1, -1,1,1, -1,1,-1]);
    let vertices = new THREE.BufferAttribute( verticesArray, 3 );
    geometry.addAttribute( 'position', vertices );

 
    let offsets = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    let i = 0;
    for(let ix = 0; ix < x; ix++)
    {
        for(let iy = 0; iy < y; iy++)
        {
            for(let iz = 0; iz < z; iz++)
            {
                //ix*y*z+iy*z+iz
                offsets.setXYZ( i, ix, iy, iz );
                offsets.setXYZ( i+1, ix, iy, iz );
                offsets.setXYZ( i+2, ix, iy, iz );
                offsets.setXYZ( i+3, ix, iy, iz );
                offsets.setXYZ( i+4, ix, iy, iz );
                offsets.setXYZ( i+5, ix, iy, iz );
                i+=6;
            }
        }
    }
    geometry.addAttribute( 'offset', offsets );



    let colors = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    for ( let i = 0, ul = colors.count; i < ul; i+=6 ) {
        let r = Math.random();
        let g = Math.random();
        let b = Math.random();
        colors.setXYZ( i, r, g, b);
        colors.setXYZ( i+1, r, g, b);
        colors.setXYZ( i+2, r, g, b);
        colors.setXYZ( i+3, r, g, b);
        colors.setXYZ( i+4, r, g, b);
        colors.setXYZ( i+5, r, g, b);
    }
    geometry.addAttribute( 'color', colors );

    let rotations = new THREE.InstancedBufferAttribute( new Float32Array( instances * 3 ), 3, 1 );
    for ( let i = 0, ul = rotations.count; i < ul; i+=6 ) {
        rotations.setXYZ( i, 0, 0, 0);
        rotations.setXYZ( i+1, 1, 0, 0);
        rotations.setXYZ( i+2, 0, 1, 0);
        rotations.setXYZ( i+3, 0, 0, 1);
        rotations.setXYZ( i+4, 0, 0, 0);
        rotations.setXYZ( i+5, 0, 0, 0);
    }
    geometry.addAttribute( 'rotation', rotations );


    // material
    let material = new THREE.RawShaderMaterial( {
 
        /*uniforms: {
            time: { value: 1.0 },
            sineTime: { value: 1.0 }
        },*/
        vertexShader: document.getElementById( 'vertexShaderTriangles' ).textContent,
        fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
        side: THREE.BackSide,
        transparent: false

    } );

    let mesh = new THREE.Mesh( geometry, material );
    mesh.geometry.computeBoundingSphere ();
    Globals.scene.add(mesh);
    mesh.position.set(pos.x,pos.y,pos.z);

}





function render():void
{
    //console.time("renderingTheGame");
    Globals.renderer.render( Globals.scene, Globals.camera );
    //console.timeEnd("renderingTheGame");
    requestAnimationFrame( render );

    stats.update();
}












init();
setupInputs();
window.requestAnimationFrame(render);

}