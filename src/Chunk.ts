/// <reference path="Globals.ts" />
/// <reference path="ColorPalette.ts" />
/// <reference path="Mouse.ts" />
namespace VoxelEditor{
//stores a chunk of voxels
//is responsible for displaying the voxels
export interface VoxelCoords
{
    x:number,
    y:number,
    z:number
}

export interface VoxelHit
{
    voxel: VoxelCoords,
    face: number
}

export class InstancedBufferGeometryMaxInstancedCount extends THREE.InstancedBufferGeometry
{
    public maxInstancedCount: number;
}

//should ONLY get used by Element
//might get replaced at some point, so nothing except Element should use it
//should be "invisible" to the user/end_user
export class Chunk extends THREE.Mesh{
    width: number;
    height: number;
    depth: number;
    c_voxelCount: number;
    voxels: Uint8Array;
    voxelsSolid: boolean[]
    voxelsBufferIndices: Uint32Array;
    voxelsReverseBufferIndices: Uint32Array;
    voxelsVisible: number;
    palette: number[] = [];
    dirty: boolean;
    g_maxInstances: number;
    offsets: THREE.InstancedBufferAttribute;
    colorIndices: THREE.InstancedBufferAttribute;
    geometry: InstancedBufferGeometryMaxInstancedCount;
    //no need for a set, voxels should only be added once per frame if the tools don't mess up!
    //and if they do, the computation cost for the few repeats will be <<<< the cost of checking uniqueness of voxels in a set
    voxelsToCheckAndCull = [];

    //cube geometry, static because not going to change
    //might give possibility to change that later on to give users the ability to customize voxel shape
    //but boundingbox would need update, as well as exporting, so a LOT of work
    static verticesArray = [0,0,0, 0,0,1, 0,1,0, 0,1,1, 1,0,0, 1,0,1, 1,1,0, 1,1,1];
    static vertices = new THREE.BufferAttribute( new Float32Array(Chunk.verticesArray), 3 );


    static indicesArray = [0,1,3, 6,0,2, 5,0,4, 6,4,0, 0,3,2, 5,1,0, 3,1,5, 7,4,6, 4,7,5, 7,6,2, 7,2,3, 7,3,5];
    static indices = new THREE.BufferAttribute(new Uint16Array(Chunk.indicesArray),1);

    static staticMaterial = new THREE.RawShaderMaterial( {
        uniforms: {
            colorPalette: {type: 'v3v', value: Globals.colorPalette.colors}
        },
        vertexShader: document.getElementById( 'vertexShader' ).textContent,
        fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
        side: THREE.FrontSide,
        transparent: false

    } );


    constructor(width, height, depth, x, y, z) {
        super();
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.c_voxelCount = this.width*this.height*this.depth;
        this.voxels = new Uint8Array(this.c_voxelCount);
        this.voxelsSolid = [];
        //might be smaller if chunks have a max size (ie: Uint16 if chunk:32*32*32 which sound reasonnable)
        //used to reduce memory swapping when voxels are added/removedfrom/to the gpu_buffer
        //might be a pain in the ass to resize, so maybe do a "virtual" resize if the user wants a smaller chunk to work with?
        this.voxelsBufferIndices = new Uint32Array(this.c_voxelCount);
        this.voxelsReverseBufferIndices = new Uint32Array(this.c_voxelCount);
        this.voxelsVisible = 0;
        //might want to change to a Vector3 array
        //this.palette = new Float32Array(255 * 3);
        this.palette = [0.2,0.2,0.2, 1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0, 1.0,1.0,1.0, 1.0,1.0,0.0, 1.0,0.0,1.0, 0.0,1.0,1.0];
        //used to track wether the chunk has been dirtied or not
        this.dirty = true;


        //worst case scenario: 1/2 of the space is filled (checker pattern) so at max 1/2 of cubes visible
        //GOD DAMMIT!: if 2 full layers + 1 air layer + 2 full layers... a lot more than 1/2! (> 2/3, even with smart culling(not accounting for rotation, but accounting for rotation might be too much work for cpu + constant gpu buffer updating is bad))
        //might need an advanced culling feature
        //let's ignore culling for now!
        this.g_maxInstances = this.width*this.height*this.depth;


        this.geometry = new InstancedBufferGeometryMaxInstancedCount();
        this.geometry.maxInstancedCount = this.g_maxInstances;
        //the "position"(offest) of each voxel relative to the chunk, needs 3 Uint8 per voxel but glsl wants floats, so there we go
        this.offsets = new THREE.InstancedBufferAttribute( new Float32Array( this.g_maxInstances * 3 ), 3, 1 );
        this.geometry.addAttribute( 'position', Chunk.vertices );
        this.geometry.setIndex(Chunk.indices);
        this.geometry.addAttribute( 'offset', this.offsets );
        //color for each voxel
        //need 3 floats per color
        //this.colors = new THREE.InstancedBufferAttribute( new Float32Array( this.g_maxInstances * 3 ), 3, 1 );
        //this.geometry.addAttribute( 'color', this.colors );

        this.colorIndices = new THREE.InstancedBufferAttribute( new Uint8Array( this.g_maxInstances), 1, 1 );
        this.geometry.addAttribute('colorIndex', this.colorIndices);

        this.material =  Chunk.staticMaterial ;

        //doesn't seem to work, might have to do it manually
        //it's probably because THREEjs isn't aware of the offset
        //this.mesh.geometry.computeBoundingBox();

        this.position.set(x, y, z);

        this.init();

        
    }

    //initialize some values for testing //temporary
    init():void
    {

        //initialize offsets //temporary for testing
        //console.time("first pass");
        /*for(let ix = 0; ix < this.width; ix++)
        {
            for(let iy = 0; iy < this.height; iy++)
            {
                for(let iz = 0; iz < this.depth; iz++)
                {
                    this.setVoxelAt(ix,iy,iz,THREE.Math.randInt(1,1)*THREE.Math.randInt(1,256)-1);
                }
            }
        }*/
        //console.timeEnd("first pass");

        this.setBoundingBox();
        this.cleanUp();
    }



    //index cpu side
    C_getVoxelIndexAt(x:number,y:number,z:number):number
    {
        return x*this.height*this.depth + y*this.depth + z;
    }

    //index gpu side
    G_getVoxelBufferIndexAt(x:number,y:number,z:number):number
    {
        return this.voxelsBufferIndices[this.C_getVoxelIndexAt(x,y,z)];
    }

    //returns the color of the voxel
    C_getVoxelAt(x:number,y:number,z:number):number
    {
        return this.voxels[this.C_getVoxelIndexAt(x,y,z)];
    }

    //returns the solidity of the voxel
    C_getVoxelSolidAt(x:number,y:number,z:number):boolean
    {
        return this.voxelsSolid[this.C_getVoxelIndexAt(x,y,z)];
    }

    //color 0 is invisible/disabled/air
    //up to 255 colors since voxels is Uint8Array
    //maybe later on more? gotta check performance
    setVoxelAt(x:number,y:number,z:number,color:number):void
    {
        //get and store the cpu index
        let c_index = this.C_getVoxelIndexAt(x,y,z);
        if(color == -1)
        {
            //don't want to remove an already removed voxel
            //if(this.voxels[c_index] != 0)
            if(this.voxelsSolid[c_index])
            {
                this.removeVoxelAt(x,y,z);
            }
        }
        else
        {
            //voxel was previously air, add it
            //if(this.voxels[c_index] == 0)
            if(!this.voxelsSolid[c_index])
            {
                this.addVoxelAt(x,y,z,color);
            }
            //voxel already had a color, update it
            else
            {
               this.setVoxelColorAt(x,y,z,color);
            }
        }
        
        
    }



    addVoxelAt(x:number,y:number,z:number,color:number):void
    {
        //gonna get added to the G_ in the cleanup phase
        //this.G_addVoxelAt(x,y,z,color);
        let c_index = this.C_getVoxelIndexAt(x,y,z);
        //set the voxel to solid
        this.voxelsSolid[c_index] = true;
        //set the color cpu side
        this.voxels[c_index] = color;
        this.addVoxelAndNeighboursToCullCheck({x:x,y:y,z:z});
    }

    //do not call directly
    //safety checks are executed by setVoxelAt
    G_addVoxelAt(x:number,y:number,z:number,color:number):void
    {
        //TODO
        //check if visible, else cull it
        //check neighbour culling
        let c_index = this.C_getVoxelIndexAt(x,y,z);
        this.offsets.setXYZ(this.voxelsVisible, x, y, z);
        let vecColor = this.getColorFromPalette(color);
        //this.colors.setXYZ(this.voxelsVisible, vecColor.x,vecColor.y,vecColor.z);
        this.colorIndices.setX(this.voxelsVisible,color);

        this.voxelsBufferIndices[c_index] = this.voxelsVisible+1;
        this.voxelsReverseBufferIndices[this.voxelsVisible+1] = c_index;

        /*this.voxelsBufferIndices[this.voxelsVisible+1] = c_index;
        this.voxelsReverseBufferIndices[c_index] = this.voxelsVisible+1;*/
        this.voxelsVisible++;
        this.dirty = true;
    }

    setVoxelColorAt(x:number,y:number,z:number,color:number):void
    {
        this.G_setVoxelColorAt(x,y,z,color);
        let c_index = this.C_getVoxelIndexAt(x,y,z);
        //set the color cpu side
        this.voxels[c_index] = color;
    }

    //do not call directly
    //safety checks are executed by setVoxelAt
    G_setVoxelColorAt(x:number,y:number,z:number,color:number):void
    {
         let g_index = this.G_getVoxelBufferIndexAt(x,y,z);
        //if index is 0, the voxel has been culled or is air, so no need to set it's color
        if(g_index != 0)
        {
            let col = this.getColorFromPalette(color)
            //this.colors.setXYZ(g_index-1, col.x, col.y, col.z);
            this.colorIndices.setX(g_index-1,color);
            this.dirty = true;
        }

    }

    removeVoxelAt(x:number,y:number,z:number):void
    {
        //gonna get removed from G_ in the cleanup phase
        //this.G_removeVoxelAt(x,y,z);
        let c_index = this.C_getVoxelIndexAt(x,y,z);
        //set the voxel to air
        this.voxelsSolid[c_index] = false;
        //no need to change the color since it's air
        this.addVoxelAndNeighboursToCullCheck({x:x,y:y,z:z});
    }

    //do not call directly
    //safety checks are executed by setVoxelAt
    G_removeVoxelAt(x:number,y:number,z:number):void
    {
        let g_index = this.G_getVoxelBufferIndexAt(x,y,z);
        let c_index = this.C_getVoxelIndexAt(x,y,z);
        //if index is 0, the voxel has been culled or is air, so no need to remove it
        if(g_index != 0 )
        {
            //g_index of last voxel rendered
            let g_index2 = this.voxelsVisible-1;
            //don't forget that g_buffer indices are 1 too big
            this.offsets.setXYZ(g_index-1, this.offsets.getX(g_index2), this.offsets.getY(g_index2), this.offsets.getZ(g_index2));
            //this.colors.setXYZ(g_index-1, this.colors.getX(g_index2), this.colors.getY(g_index2), this.colors.getZ(g_index2));
            this.colorIndices.setX(g_index-1,this.colorIndices.getX(g_index2));

            this.voxelsBufferIndices[this.voxelsReverseBufferIndices[g_index2+1]] = this.voxelsBufferIndices[c_index];
            this.voxelsReverseBufferIndices[this.voxelsBufferIndices[c_index]] = this.voxelsReverseBufferIndices[g_index2+1];

            this.voxelsBufferIndices[c_index] = 0;
            this.voxelsReverseBufferIndices[0] = c_index;
            
            this.voxelsVisible--;
            this.dirty = true;
        }
    }


    /*raycast( raycaster: THREE.Raycaster, intersects:any[] ):void
    {

        let inverseMatrix = new THREE.Matrix4();
		let ray = new THREE.Ray();
		let matrixWorld = this.matrixWorld;
		let sphere = new THREE.Sphere();
        let hit = null;

        sphere.copy( this.geometry.boundingSphere );
        sphere.applyMatrix4( matrixWorld );

        inverseMatrix.getInverse( matrixWorld );
        ray.copy( raycaster.ray ).applyMatrix4( inverseMatrix );

        //allows ray to hit even if inside the chunk
        if(this.geometry.boundingBox !== null  && this.geometry.boundingBox.containsPoint(ray.origin))
        {
            hit = ray.origin;
        }
        //outside of chunk, normal raycasting
        else if ( raycaster.ray.intersectsSphere( sphere ) !== false )
        {
            // Check boundingBox before continuing

            if ( this.geometry.boundingBox !== null ) {
                hit = ray.intersectBox( this.geometry.boundingBox );
            }
        }

        if ( hit !== null )
        {
            let distance = new THREE.Vector3().subVectors(ray.origin,hit).lengthSq();
            let hitLocal = hit.clone();
            //hit coordinates to world
            hit.applyMatrix4(matrixWorld);
            //TODO: implement THREE.Intersection!!!
            intersects.push({object:this,distance:distance,point:hit,hitLocal:hitLocal});

            this.rayToVoxels(hitLocal,ray.direction);
        }
    }*/


    
    /*rayToVoxels(origin:THREE.Vector3, direction:THREE.Vector3, onlyFirst = false):VoxelHit[]
    {
        //to make sure origin is an in bound voxel
        direction.normalize();
        origin.add(direction.clone().multiplyScalar(0.00000001))


        //the c * vector at which there is the next coordinate jump
        let cx,cy,cz;

        //store if ascending or descending coordinates
        //either -1 or 1
        let xunit,yunit,zunit;

        if(direction.x > 0)
        {
            cx = ((Math.ceil(origin.x) - origin.x)/direction.x);
            xunit = 1;
        }
        else
        {
            cx = ((origin.x-Math.floor(origin.x))/-direction.x);
            xunit = -1;
        }

        if(direction.y > 0)
        {
            cy = ((Math.ceil(origin.y) - origin.y)/direction.y);
            yunit = 1;
        }
        else
        {
            cy = ((origin.y-Math.floor(origin.y))/-direction.y);
            yunit = -1;
        }

        if(direction.z > 0)
        {
            cz = ((Math.ceil(origin.z) - origin.z)/direction.z);
            zunit = 1;
        }
        else
        {
            cz = ((origin.z-Math.floor(origin.z))/-direction.z);
            zunit = -1;
        }

        //get the first voxel coordinate
        let x = Math.floor(origin.x);
        let y = Math.floor(origin.y);
        let z = Math.floor(origin.z);

        //get the c unit to jump to the next coordinate
        let cxunit = 1.0/Math.abs(direction.x);
        let cyunit = 1.0/Math.abs(direction.y);
        let czunit = 1.0/Math.abs(direction.z);

        //the face hit by the ray
        let face:number;
        //get the first face
        //step back a voxel
        let icx = cx - cxunit;
        let icy = cy - cyunit;
        let icz = cz - czunit;
        //look for the closest voxel coord step (here the biggest since all < 0)
        if(icx >= icy && icx >= icz)face = xunit * 1;
        else if(icy >= icz)face = yunit * 2;
        else face = zunit * 3;
        


        let voxels : VoxelHit[] = [];

        while(x >= 0 && x < this.width && y >= 0 && y < this.height && z >=0 && z < this.depth)
        {
            //push the previous voxel, it is in bounds
            //but only if it is solid
            if(this.voxelsSolid[this.C_getVoxelIndexAt(x,y,z)])
            {
                voxels.push({voxel:{x:x,y:y,z:z},face:face});
                if(onlyFirst)
                    return voxels;
            }
            //check which direction is the nex to jump
            if(cx < cy && cx < cz)
            {
                x += xunit;
                cx += cxunit;
                //-1 or 1
                face = xunit * 1;
            }
            else if(cy < cz)
            {
                y += yunit;
                cy += cyunit;
                //-2 or 2
                face = yunit * 2;
            }
            else
            {
                z += zunit;
                cz += czunit;
                //-3 or 2
                face = zunit * 3;
            }
        }

        //TEMP!!!
        if(Globals.mouse.left)
        {
            // temp
            for(let i = 0; i < voxels.length; i++)
            {
                this.setVoxelAt(voxels[i].voxel.x,voxels[i].voxel.y,voxels[i].voxel.z,Globals.colorPalette.selectedColor);
            }
            this.cleanUp();
            //temp
        }

        return voxels;
    }*/




    isVoxelVisibleAt(x:number,y:number,z:number):boolean
    {
        return false;
    }

    //get the color from the palette and return it as Vector3
    getColorFromPalette(color:number):THREE.Vector3
    {
        //color--;
        return Globals.colorPalette.colors[color];
    }

    //called after the chunk has been dirtied to clean it up
    //has a list of tasks to perform/check
    //updates the gpu buffers
    cleanUp():void
    {
        //go over all voxels to check and checks them
        this.voxelsToCheckAndCull.forEach((voxel)=> {
            this.checkAndCullVoxel(voxel);
        });
        this.voxelsToCheckAndCull = [];
        if(this.dirty)
        {
            this.offsets.needsUpdate = true;
            this.colorIndices.needsUpdate = true;
            this.geometry.maxInstancedCount = this.voxelsVisible;
            this.dirty = false;
            //might want to update bounding box
        }
    }

    addVoxelAndNeighboursToCullCheck(v: VoxelCoords):void
    {
        this.voxelsToCheckAndCull.push(v);
    }

    checkAndCullVoxel(v: VoxelCoords):void
    {
        let neighbours = false;
        let g_index = this.G_getVoxelBufferIndexAt(v.x,v.y,v.z);
        if(v.x > 0 && v.x < this.width-1 && v.y > 0 && v.y < this.height -1 && v.z > 0 && v.z < this.depth -1)
        {
            neighbours = this.C_getVoxelSolidAt(v.x-1,v.y,v.z) && this.C_getVoxelSolidAt(v.x+1,v.y,v.z) && this.C_getVoxelSolidAt(v.x,v.y-1,v.z) && this.C_getVoxelSolidAt(v.x,v.y+1,v.z) && this.C_getVoxelSolidAt(v.x,v.y,v.z-1) && this.C_getVoxelSolidAt(v.x,v.y,v.z+1);
        }
        else
        {
            //might want to check the neighbour chunks to increase culling even more
        }
        if(neighbours)
        {
            if(g_index !=0)
            {
                this.G_removeVoxelAt(v.x,v.y,v.z);
            }
        }
        else 
        {
            if(g_index == 0)
            {
                this.G_addVoxelAt(v.x,v.y,v.z,this.C_getVoxelAt(v.x,v.y,v.z));
            }
        }
    }

    setBoundingBox():void
    {
        this.geometry.boundingBox = new THREE.Box3(new THREE.Vector3(0.0,0.0,0.0), new THREE.Vector3(this.width,this.height,this.depth));
        //required to prevent too early culling
        this.geometry.boundingSphere = new THREE.Sphere(new THREE.Vector3(this.width/2.0,this.height/2.0,this.depth/2.0), Math.sqrt(this.width*this.width+this.height*this.height+this.depth*this.depth)/2.0);
    }

}
}