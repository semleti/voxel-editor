//might want to bake the HTML for the native Sections and just fetch priority
//or maybe not, because it looses a lot of flexibility

namespace VoxelEditor{
export class Section{
    domElement: HTMLElement;
    priority = 0;
}

export class SectionContainer extends Section{
    sections: Section[] = [];
    addSection(section: Section):void{
        this.sections.push(section);
        this.domElement.appendChild(section.domElement);
        //needs optimisation maybe to only add elements all at once?
        this.sortSections();
    }
    sortSections()
    {
        this.sections.sort(function(a:Section,b:Section){return a.priority - b.priority;});
        this.sections.forEach((section)=>{this.domElement.appendChild(section.domElement);});
    }
}

export class SectionSelectableContainer extends SectionContainer{
    selectedSection: Section = null;

    addSection(section: Section):void{
        super.addSection(section);
        this.selectSection(this.sections[0]);
        section.domElement.onclick = ()=>{this.selectSection(section);};
    }

    selectSection(section: Section):void{
        //first remove then add, so if brush allready selected, no problem
        if(this.selectedSection !== null)this.selectedSection.domElement.classList.remove("sectionSelected");
        section.domElement.classList.add("sectionSelected");
        this.selectedSection = section;
    }
}

export class SectionRegisterer extends SectionContainer{
    static instance = new SectionRegisterer();
    constructor()
    {
        super();
        this.domElement = document.getElementById("sections");
    }

    registerSection(section: Section)
    {
        this.addSection(section);
    }
}
}