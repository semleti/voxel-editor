/// <reference path="SectionRegisterer.ts" />
namespace VoxelEditor{
export class Modifier{
    domElement: HTMLElement;
}
export class ModifierSection extends Section{
    static instance = new ModifierSection();
    modifiers : Modifier[] = [];
    constructor()
    {
        super();
        this.priority = 30;
        this.domElement = document.createElement("div");
        this.domElement.textContent = "ModifierSection";
        SectionRegisterer.instance.registerSection(this);
    }

    registerModifier(modifier: Modifier)
    {
        this.modifiers.push(modifier);
        this.domElement.appendChild(modifier.domElement);
    }
}
}