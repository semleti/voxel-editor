/// <reference path="SectionRegisterer.ts" />
namespace VoxelEditor{
export class Brush extends Section{
    getVoxels():any{};
}
export class BrushSection extends SectionSelectableContainer{
    static instance = new BrushSection();
    sections : Brush[] = [];
    constructor()
    {
        super();
        this.priority = 10;
        this.domElement = document.createElement("div");
        this.domElement.textContent = "BrushSection";
        SectionRegisterer.instance.registerSection(this);
    }

    registerBrush(brush: Brush)
    {
        this.addSection(brush);
    }
}
}