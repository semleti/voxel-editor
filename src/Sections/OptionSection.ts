/// <reference path="SectionRegisterer.ts" />
namespace VoxelEditor{
export class Option{
    domElement: HTMLElement;
}
export class OptionSection extends Section{
    static instance = new OptionSection();
    option : Option[] = [];
    constructor()
    {
        super();
        this.priority = 40;
        this.domElement = document.createElement("div");
        this.domElement.textContent = "OptionSection";
        SectionRegisterer.instance.registerSection(this);
    }

    registerBrush(option: Option)
    {
        this.option.push(option);
        this.domElement.appendChild(option.domElement);
    }
}
}