/// <reference path="../ModifierSection.ts" />
namespace VoxelEditor{
export class Mirror extends Modifier{
    static instance = new Mirror();
    domElement: HTMLInputElement;
    constructor()
    {
        super();
        this.domElement = document.createElement("input");
        this.domElement.type = "radio";
        //this.domElement.textContent = "Mirror";
        ModifierSection.instance.registerModifier(this);
    }
}
}