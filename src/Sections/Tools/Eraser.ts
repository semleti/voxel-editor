/// <reference path="../ToolSection.ts" />
namespace VoxelEditor{
export class Eraser extends Tool{
    static instance = new Eraser();
    constructor()
    {
        super();
        this.domElement = document.createElement("button");
        this.domElement.textContent = "Eraser";
        ToolSection.instance.registerTool(this);
    }

    apply(elt:Element,coords:VoxelCoords):void{
        elt.setVoxelAt(coords.x,coords.y,coords.z,-1);
    }
}
}