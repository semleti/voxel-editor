/// <reference path="SectionRegisterer.ts" />
namespace VoxelEditor{
export class Tool{
    domElement: HTMLElement;
    priority: number = 0;
    apply(elt:Element,coords:VoxelCoords):void{};
}
export class ToolSection extends SectionSelectableContainer{
    static instance = new ToolSection();
    sections: Tool[] = [];
    selectedSection: Tool;
    constructor()
    {
        super();
        this.priority = 20;
        this.domElement = document.createElement("div");
        this.domElement.textContent = "ToolSection";
        SectionRegisterer.instance.registerSection(this);
    }

    registerTool(tool: Tool)
    {
        super.addSection(tool);
    }


    mouseMove()
    {
        Globals.raycaster.setFromCamera( Globals.mouse.position, Globals.camera );

        // calculate objects intersecting the picking ray
        let intersects = Globals.raycaster.intersectObjects( Globals.stage.models);
    }

    getSelectedTool():Tool{
        return this.selectedSection;
    }

}
}