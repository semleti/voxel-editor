/// <reference path="../BrushSection.ts" />
namespace VoxelEditor{
export class SingleVoxel extends Brush{
    static instance = new SingleVoxel();
    constructor()
    {
        super();
        this.priority = 10;
        this.domElement = document.createElement("button");
        this.domElement.textContent = "SingleVoxel";
        BrushSection.instance.registerBrush(this);
    }
}
}