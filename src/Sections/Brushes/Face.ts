/// <reference path="../BrushSection.ts" />
namespace VoxelEditor{
export class Face extends Brush{
    static instance = new Face();
    constructor()
    {
        super();
        this.priority = 20;
        this.domElement = document.createElement("button");
        this.domElement.textContent = "Face";
        BrushSection.instance.registerBrush(this);
    }
}
}