namespace VoxelEditor{
export class Mouse{
    position = new THREE.Vector2(1,1);
    previousPosition = new THREE.Vector2(1,1);
    left: boolean = false;
    middle: boolean = false;
    right: boolean = false;
    constructor()
    {

    }
    setPos(x:number,y:number):void
    {
        this.previousPosition.x = this.position.x;
        this.previousPosition.y = this.position.y;
        this.position.x = x;
        this.position.y = y;
    }
}
}