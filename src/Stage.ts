namespace VoxelEditor{
export class Stage {
    models:Model[] = [];
    constructor()
    {
        
    }

    addModel(chunkWidth,chunkHeight,chunkDepth)
    {
        this.models.push(new Model(chunkWidth,chunkHeight,chunkDepth));
    }

    raycast():void
    {
        Globals.raycaster.setFromCamera( Globals.mouse.position, Globals.camera );

        // calculate objects intersecting the picking ray
        let intersects = Globals.raycaster.intersectObjects( this.models);
    }
}
}