System.register(["Mouse"], function (exports_1, context_1) {
    "use strict";
    var __extends = (this && this.__extends) || (function () {
        var extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return function (d, b) {
            extendStatics(d, b);
            function __() { this.constructor = d; }
            d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
    })();
    var __moduleName = context_1 && context_1.id;
    function init() {
        // on initialise le moteur de rendu
        renderer = new THREE.WebGLRenderer();
        if (renderer.extensions.get('ANGLE_instanced_arrays') === false) {
            console.log("NOT SUPPORTED");
            return;
        }
        // si WebGL ne fonctionne pas sur votre navigateur vous pouvez utiliser le moteur de rendu Canvas à la place
        // renderer = new THREE.CanvasRenderer();
        renderer.setClearColor(0x101010);
        renderer.setSize(container.offsetWidth, container.offsetHeight);
        //useless?
        //renderer.setFaceCulling(THREE.CullFaceFront,THREE.FrontFaceDirectionCw);
        container.appendChild(renderer.domElement);
        // on initialise la scène
        scene = new THREE.Scene();
        // on initialise la camera que l’on place ensuite sur la scène
        camera = new THREE.PerspectiveCamera(50, container.offsetWidth / container.offsetHeight, 1, 10000);
        camera.position.set(0, 0, 1000);
        scene.add(camera);
        // on créé un  cube au quel on définie un matériau puis on l’ajoute à la scène
        var cubeGeometry = new THREE.CubeGeometry(100, 100, 100);
        var wireframeMaterial = new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: false });
        var cubeMesh = new THREE.Mesh(cubeGeometry, wireframeMaterial);
        //scene.add( cubeMesh );
        var planeGeometry = new THREE.PlaneGeometry(2000, 2000, 20, 20);
        var planeMaterial = new THREE.MeshPhongMaterial({ color: 0x0000ff, wireframe: true });
        var planeMeshBack = new THREE.Mesh(planeGeometry, planeMaterial);
        planeMeshBack.position.set(0, 0, -1000);
        planeMeshBack.setRotationFromAxisAngle(new THREE.Vector3(0, 0, 1), Math.PI);
        scene.add(planeMeshBack);
        var planeMeshRight = new THREE.Mesh(planeGeometry, planeMaterial);
        planeMeshRight.position.set(1000, 0, 0);
        planeMeshRight.setRotationFromAxisAngle(new THREE.Vector3(0, 1, 0), -Math.PI / 2);
        scene.add(planeMeshRight);
        var planeMeshBottom = new THREE.Mesh(planeGeometry, planeMaterial);
        planeMeshBottom.position.set(0, -1000, 0);
        planeMeshBottom.setRotationFromAxisAngle(new THREE.Vector3(1, 0, 0), -Math.PI / 2);
        scene.add(planeMeshBottom);
        //lights
        var ambientLight = new THREE.AmbientLight(0xA0A080);
        scene.add(ambientLight);
        var directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
        directionalLight.position.set(500, 500, 500);
        scene.add(directionalLight);
        stats = new Stats();
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.top = '0px';
        stats.domElement.style.right = '0px';
        container.appendChild(stats.domElement);
        //createInstancedCubes(100,100,100,new THREE.Vector3(0,0,0));
        //createInstancedCubes(100,100,100,new THREE.Vector3(240,0,0));
        //var chunk = new Chunk(100,100,100,0,0,0);
        //var chunk2 = new Chunk(100,100,100,200,0,0);
        //scene.add(chunk.mesh);
        //scene.add(chunk2.mesh);
        stage = new Stage();
        //the more individual chunks, the more time intensive (bottleneck transfer of data?)
        stage.addModel(4, 4, 4, 32, 32, 32);
        // on effectue le rendu de la scène
        window.requestAnimationFrame(render);
    }
    //worst case scenario: >12 million cubes (>2/3 space filled so no cubes are culled(2 full layers, 1 air, 2 ful, 1 air....) + some on the edges) if terrain is 256*256*256
    //problem: only <10 FPS
    //maybe if subdivided in chunks FPS boost?
    //if not, might enable to cull entire chunks (definitely able to cull what is off-screen, so that's a huge+)
    function createInstancedCubes(x, y, z, pos) {
        var triangles = 12;
        var instances = x * y * z;
        var geometry = new THREE.InstancedBufferGeometry();
        geometry.maxInstancedCount = instances;
        var verticesArray = new Float32Array([0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1]);
        var vertices = new THREE.BufferAttribute(verticesArray, 3);
        geometry.addAttribute('position', vertices);
        var indicesArray = new Uint16Array([0, 1, 3, 6, 0, 2, 5, 0, 4, 6, 4, 0, 0, 3, 2, 5, 1, 0, 3, 1, 5, 7, 4, 6, 4, 7, 5, 7, 6, 2, 7, 2, 3, 7, 3, 5]);
        geometry.setIndex(new THREE.BufferAttribute(indicesArray, 1));
        var offsets = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        var i = 0;
        for (var ix = 0; ix < x; ix++) {
            for (var iy = 0; iy < y; iy++) {
                for (var iz = 0; iz < z; iz++) {
                    //ix*y*z+iy*z+iz
                    offsets.setXYZ(i, ix, iy, iz);
                    i++;
                }
            }
        }
        geometry.addAttribute('offset', offsets);
        var colors = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        for (var i = 0, ul = colors.count; i < ul; i++) {
            var r = Math.random();
            var g = Math.random();
            var b = Math.random();
            colors.setXYZ(i, r, g, b);
        }
        geometry.addAttribute('color', colors);
        // material
        var material = new THREE.RawShaderMaterial({
            /*uniforms: {
                time: { value: 1.0 },
                sineTime: { value: 1.0 }
            },*/
            vertexShader: document.getElementById('vertexShader').textContent,
            fragmentShader: document.getElementById('fragmentShader').textContent,
            side: THREE.BackSide,
            transparent: false
        });
        var mesh = new THREE.Mesh(geometry, material);
        mesh.geometry.computeBoundingSphere();
        scene.add(mesh);
        mesh.position.set(pos.x, pos.y, pos.z);
    }
    //probably only worth it if divides triangles by over 6(could happen if a lot of cubes have 4 neighbours(difficult))
    //maybe optimize rotation code?
    //maybe one buffer per rotation?
    //one issue is the duplication of data: color, offset -> instancedBufferAttribute?
    //not worth investigating right now, maybe later when I try to squeeze every drop of power of the gpu
    function createInstancedTriangles(x, y, z, pos) {
        var triangles = 12;
        var instances = x * y * z * 6;
        var geometry = new THREE.InstancedBufferGeometry();
        geometry.maxInstancedCount = instances;
        var verticesArray = new Float32Array([-1, -1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, -1, 1, 1, -1, 1, -1]);
        var vertices = new THREE.BufferAttribute(verticesArray, 3);
        geometry.addAttribute('position', vertices);
        var offsets = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        var i = 0;
        for (var ix = 0; ix < x; ix++) {
            for (var iy = 0; iy < y; iy++) {
                for (var iz = 0; iz < z; iz++) {
                    //ix*y*z+iy*z+iz
                    offsets.setXYZ(i, ix, iy, iz);
                    offsets.setXYZ(i + 1, ix, iy, iz);
                    offsets.setXYZ(i + 2, ix, iy, iz);
                    offsets.setXYZ(i + 3, ix, iy, iz);
                    offsets.setXYZ(i + 4, ix, iy, iz);
                    offsets.setXYZ(i + 5, ix, iy, iz);
                    i += 6;
                }
            }
        }
        geometry.addAttribute('offset', offsets);
        var colors = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        for (var i = 0, ul = colors.count; i < ul; i += 6) {
            var r = Math.random();
            var g = Math.random();
            var b = Math.random();
            colors.setXYZ(i, r, g, b);
            colors.setXYZ(i + 1, r, g, b);
            colors.setXYZ(i + 2, r, g, b);
            colors.setXYZ(i + 3, r, g, b);
            colors.setXYZ(i + 4, r, g, b);
            colors.setXYZ(i + 5, r, g, b);
        }
        geometry.addAttribute('color', colors);
        var rotations = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        for (var i = 0, ul = rotations.count; i < ul; i += 6) {
            rotations.setXYZ(i, 0, 0, 0);
            rotations.setXYZ(i + 1, 1, 0, 0);
            rotations.setXYZ(i + 2, 0, 1, 0);
            rotations.setXYZ(i + 3, 0, 0, 1);
            rotations.setXYZ(i + 4, 0, 0, 0);
            rotations.setXYZ(i + 5, 0, 0, 0);
        }
        geometry.addAttribute('rotation', rotations);
        // material
        var material = new THREE.RawShaderMaterial({
            /*uniforms: {
                time: { value: 1.0 },
                sineTime: { value: 1.0 }
            },*/
            vertexShader: document.getElementById('vertexShaderTriangles').textContent,
            fragmentShader: document.getElementById('fragmentShader').textContent,
            side: THREE.BackSide,
            transparent: false
        });
        var mesh = new THREE.Mesh(geometry, material);
        mesh.geometry.computeBoundingSphere();
        scene.add(mesh);
        mesh.position.set(pos.x, pos.y, pos.z);
    }
    function render() {
        //console.time("renderingTheGame");
        renderer.render(scene, camera);
        //console.timeEnd("renderingTheGame");
        requestAnimationFrame(render);
        stats.update();
    }
    function onContainerKeyboard(event) {
        //event.preventDefault();
        //left
        var mov = new THREE.Vector3(0, 0, 0);
        if (event.key == 'q') {
            mov.add(new THREE.Vector3(-1, 0, 0));
        }
        //right
        if (event.key == 'd') {
            mov.add(new THREE.Vector3(1, 0, 0));
        }
        //up
        if (event.key == 'a') {
            mov.add(new THREE.Vector3(0, 1, 0));
        }
        //down
        if (event.key == 'e') {
            mov.add(new THREE.Vector3(0, -1, 0));
        }
        //forward
        if (event.key == 'z') {
            mov.add(new THREE.Vector3(0, 0, -1));
        }
        //backward
        if (event.key == 's') {
            mov.add(new THREE.Vector3(0, 0, 1));
        }
        mov.multiplyScalar(movementSpeed);
        mov.applyEuler(camera.rotation);
        camera.position.add(mov);
    }
    function onContainerMouseWheel(event) {
        //event.preventDefault();
        /*var zoom = -event.wheelDelta/120 * 4;
        camera.fov += zoom;
        camera.fov=clamp(camera.fov,1,170);
        camera.updateProjectionMatrix();*/
        var mov = new THREE.Vector3(0, 0, -event.wheelDelta / 120 * 40);
        mov.applyEuler(camera.rotation);
        camera.position.add(mov);
    }
    //TODO:
    //when mouse pressed and in container border, keep moving
    function onContainerMouseMove(event) {
        event.preventDefault();
        //middle mouse button
        //move camera left/right up/down
        if (event.buttons & 4) {
            var mov = new THREE.Vector3(-(event.movementX), event.movementY, 0);
            mov.applyEuler(camera.rotation);
            camera.position.add(mov);
        }
        //right mouse button
        //rotate camera
        if (event.buttons & 2) {
            cameraPitch = clamp(cameraPitch + event.movementY / 100.0 * pitchSensivity * (invertPitch ? -1 : 1), -Math.PI / 2, Math.PI / 2);
            cameraYaw += event.movementX / 100.0 * yawSensivity * (invertYaw ? -1 : 1);
            camera.setRotationFromEuler(new THREE.Euler(cameraPitch, cameraYaw, 0, 'YXZ'));
        }
        //left mouse button
        //move camera forward/backwards
        /*if(event.buttons & 1) {
            var move = new THREE.Vector3( 0,0, -event.movementX + event.movementY);
            move.applyEuler(camera.rotation);
            camera.position.add(move);
        }*/
        mouse.position.x = (event.offsetX / renderer.domElement.width) * 2 - 1;
        mouse.position.y = -(event.offsetY / renderer.domElement.height) * 2 + 1;
        //raycastHover();
        stage.raycast();
    }
    function onContainerMouseDown(event) {
        if (event.button == 0)
            mouse.left = true;
        else if (event.button == 1)
            mouse.middle = true;
        else if (event.button == 2)
            mouse.right = true;
    }
    function onContainerMouseUp(event) {
        if (event.button == 0)
            mouse.left = false;
        else if (event.button == 1)
            mouse.middle = false;
        else if (event.button == 2)
            mouse.right = false;
    }
    function setupUI() {
        renderer.domElement.addEventListener("mousedown", onContainerMouseDown);
        renderer.domElement.addEventListener("mouseup", onContainerMouseUp);
        renderer.domElement.addEventListener("mousemove", onContainerMouseMove);
        renderer.domElement.addEventListener("mousewheel", onContainerMouseWheel);
        //needs to be window to get key events
        window.onkeydown = onContainerKeyboard;
        //disable context menu in container
        renderer.domElement.addEventListener("contextmenu", function (e) {
            e.preventDefault();
        }, false);
    }
    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }
    function hexToRGB(hex) {
        hex = hex.replace('#', '');
        var r = parseInt(hex.substring(0, 2), 16);
        var g = parseInt(hex.substring(2, 4), 16);
        var b = parseInt(hex.substring(4, 6), 16);
        return { red: r, green: g, blue: b };
    }
    function clamp(number, min, max) {
        if (number < min)
            return min;
        if (number > max)
            return max;
        return number;
    }
    //helper function for arguments with default value
    function pickValue(arg, def) {
        return (typeof arg == 'undefined' ? def : arg);
    }
    var Mouse_1, renderer, scene, camera, stage, colorPalette, stats, container, cameraPitch, cameraYaw, invertPitch, invertYaw, pitchSensivity, yawSensivity, raycaster, mouse, CUBEvertices, SCALE, Stage, Model, InstancedBufferGeometryMaxInstancedCount, Chunk, movementSpeed, ColorInput, ColorPalette;
    return {
        setters: [
            function (Mouse_1_1) {
                Mouse_1 = Mouse_1_1;
            }
        ],
        execute: function () {
            console.log("hi-this is mainTypeScript");
            container = document.getElementById('container');
            cameraPitch = 0;
            cameraYaw = 0;
            invertPitch = true;
            invertYaw = true;
            pitchSensivity = 1;
            yawSensivity = 1;
            raycaster = new THREE.Raycaster();
            /*class Mouse{
                position: THREE.Vector2 = new THREE.Vector2(1,1);
                left: boolean = false;
                middle: boolean = false;
                right: boolean = false;
                constructor()
                {
            
                }
            }   */
            mouse = new Mouse_1.default();
            CUBEvertices = new Float32Array([
                -1.0, -1.0, -1.0,
                -1.0, -1.0, 1.0,
                -1.0, 1.0, 1.0,
                1.0, 1.0, -1.0,
                -1.0, -1.0, -1.0,
                -1.0, 1.0, -1.0,
                1.0, -1.0, 1.0,
                -1.0, -1.0, -1.0,
                1.0, -1.0, -1.0,
                1.0, 1.0, -1.0,
                1.0, -1.0, -1.0,
                -1.0, -1.0, -1.0,
                -1.0, -1.0, -1.0,
                -1.0, 1.0, 1.0,
                -1.0, 1.0, -1.0,
                1.0, -1.0, 1.0,
                -1.0, -1.0, 1.0,
                -1.0, -1.0, -1.0,
                -1.0, 1.0, 1.0,
                -1.0, -1.0, 1.0,
                1.0, -1.0, 1.0,
                1.0, 1.0, 1.0,
                1.0, -1.0, -1.0,
                1.0, 1.0, -1.0,
                1.0, -1.0, -1.0,
                1.0, 1.0, 1.0,
                1.0, -1.0, 1.0,
                1.0, 1.0, 1.0,
                1.0, 1.0, -1.0,
                -1.0, 1.0, -1.0,
                1.0, 1.0, 1.0,
                -1.0, 1.0, -1.0,
                -1.0, 1.0, 1.0,
                1.0, 1.0, 1.0,
                -1.0, 1.0, 1.0,
                1.0, -1.0, 1.0
            ]);
            SCALE = 100;
            Stage = (function () {
                function Stage() {
                    this.models = [];
                }
                Stage.prototype.addModel = function (width, height, depth, chunkWidth, chunkHeight, chunkDepth) {
                    this.models.push(new Model(width, height, depth, chunkWidth, chunkHeight, chunkDepth));
                };
                Stage.prototype.raycast = function () {
                    raycaster.setFromCamera(mouse.position, camera);
                    // calculate objects intersecting the picking ray
                    var intersects = raycaster.intersectObjects(this.models);
                };
                return Stage;
            }());
            Model = (function (_super) {
                __extends(Model, _super);
                function Model(width, height, depth, chunkWidth, chunkHeight, chunkDepth) {
                    var _this = _super.call(this) || this;
                    _this.chunks = [];
                    var cubeGeometry = new THREE.CubeGeometry(10, 10, 10);
                    var material = new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: false });
                    _this.cube = new THREE.Mesh(cubeGeometry, material);
                    scene.add(_this.cube);
                    //the models Object3D
                    _this.position.set(0, 0, 0);
                    //store the chunks preferred dims
                    _this.chunkWidth = chunkWidth;
                    _this.chunkHeight = chunkHeight;
                    _this.chunkDepth = chunkDepth;
                    //store the desired dims
                    //TODO: alllow for dimensions in voxels, not only chunks
                    _this.width = width;
                    _this.height = height;
                    _this.depth = depth;
                    //needed to prevent a WebGL warning
                    _this.geometry = new THREE.PlaneGeometry(0, 0);
                    _this.material = new THREE.MeshBasicMaterial();
                    _this.material.visible = false;
                    _this.init();
                    scene.add(_this);
                    return _this;
                }
                Model.prototype.addChunk = function (width, height, depth, x, y, z) {
                    var chunk = new Chunk(width, height, depth, x, y, z);
                    this.chunks.push(chunk);
                    this.add(chunk);
                };
                Model.prototype.raycast = function () {
                    raycaster.setFromCamera(mouse.position, camera);
                    // calculate objects intersecting the picking ray
                    var inverseMatrix = new THREE.Matrix4();
                    var ray = new THREE.Ray();
                    var matrixWorld = this.matrixWorld;
                    var sphere = new THREE.Sphere();
                    sphere.copy(this.geometry.boundingSphere);
                    sphere.applyMatrix4(matrixWorld);
                    if (raycaster.ray.intersectsSphere(sphere) !== false) {
                        inverseMatrix.getInverse(matrixWorld);
                        ray.copy(raycaster.ray).applyMatrix4(inverseMatrix);
                        // Check boundingBox before continuing
                        if (this.geometry.boundingBox !== null) {
                            if (ray.intersectsBox(this.geometry.boundingBox)) {
                                var intersects = raycaster.intersectObjects(this.chunks);
                                if (intersects.length > 0)
                                    this.cube.position.set(intersects[0].point.x, intersects[0].point.y, intersects[0].point.z);
                                for (var i = 0; i < intersects.length; i++) {
                                    /*console.log("intersected: " + i);
                                    console.log(intersects[i]);*/
                                }
                            }
                        }
                    }
                };
                Model.prototype.init = function () {
                    for (var x = 0; x < this.width; x++) {
                        for (var y = 0; y < this.height; y++) {
                            for (var z = 0; z < this.depth; z++) {
                                this.addChunk(this.chunkWidth, this.chunkHeight, this.chunkDepth, x * this.chunkWidth, y * this.chunkHeight, z * this.chunkDepth);
                            }
                        }
                    }
                    this.setBoundingBox();
                };
                Model.prototype.getChunkIndexAt = function (x, y, z) {
                    return x * this.height * this.depth + y * this.depth + z;
                };
                Model.prototype.getChunkAt = function (x, y, z) {
                    return this.chunks[this.getChunkIndexAt(x, y, z)];
                };
                Model.prototype.setBoundingBox = function () {
                    var width = this.width * this.chunkWidth;
                    var height = this.height * this.chunkHeight;
                    var depth = this.depth * this.chunkDepth;
                    this.geometry.boundingBox = new THREE.Box3(new THREE.Vector3(0.0, 0.0, 0.0), new THREE.Vector3(width, height, depth));
                    //required to prevent too early culling
                    this.geometry.boundingSphere = new THREE.Sphere(new THREE.Vector3(width / 2.0, height / 2.0, depth / 2.0), Math.sqrt(width * width + height * height + depth * depth) / 2.0);
                };
                return Model;
            }(THREE.Mesh));
            InstancedBufferGeometryMaxInstancedCount = (function (_super) {
                __extends(InstancedBufferGeometryMaxInstancedCount, _super);
                function InstancedBufferGeometryMaxInstancedCount() {
                    return _super !== null && _super.apply(this, arguments) || this;
                }
                return InstancedBufferGeometryMaxInstancedCount;
            }(THREE.InstancedBufferGeometry));
            Chunk = (function (_super) {
                __extends(Chunk, _super);
                function Chunk(width, height, depth, x, y, z) {
                    var _this = _super.call(this) || this;
                    _this.palette = [];
                    _this.width = width;
                    _this.height = height;
                    _this.depth = depth;
                    _this.c_voxelCount = _this.width * _this.height * _this.depth;
                    _this.voxels = new Uint8Array(_this.c_voxelCount);
                    _this.voxelsSolid = [];
                    //might be smaller if chunks have a max size (ie: Uint16 if chunk:32*32*32 which sound reasonnable)
                    //used to reduce memory swapping when voxels are added/removedfrom/to the gpu_buffer
                    //might be a pain in the ass to resize, so maybe do a "virtual" resize if the user wants a smaller chunk to work with?
                    _this.voxelsBufferIndices = new Uint32Array(_this.c_voxelCount);
                    _this.voxelsReverseBufferIndices = new Uint32Array(_this.c_voxelCount);
                    _this.voxelsVisible = 0;
                    //might want to change to a Vector3 array
                    //this.palette = new Float32Array(255 * 3);
                    _this.palette = [0.2, 0.2, 0.2, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0];
                    //used to track wether the chunk has been dirtied or not
                    _this.dirty = true;
                    //worst case scenario: 1/2 of the space is filled (checker pattern) so at max 1/2 of cubes visible
                    //GOD DAMMIT!: if 2 full layers + 1 air layer + 2 full layers... a lot more than 1/2! (> 2/3, even with smart culling(not accounting for rotation, but accounting for rotation might be too much work for cpu + constant gpu buffer updating is bad))
                    //might need an advanced culling feature
                    //let's ignore culling for now!
                    _this.g_maxInstances = _this.width * _this.height * _this.depth;
                    _this.geometry = new InstancedBufferGeometryMaxInstancedCount();
                    _this.geometry.maxInstancedCount = _this.g_maxInstances;
                    //the "position"(offest) of each voxel relative to the chunk, needs 3 Uint8 per voxel but glsl wants floats, so there we go
                    _this.offsets = new THREE.InstancedBufferAttribute(new Float32Array(_this.g_maxInstances * 3), 3, 1);
                    _this.geometry.addAttribute('position', Chunk.vertices);
                    _this.geometry.setIndex(Chunk.indices);
                    _this.geometry.addAttribute('offset', _this.offsets);
                    //color for each voxel
                    //need 3 floats per color
                    //this.colors = new THREE.InstancedBufferAttribute( new Float32Array( this.g_maxInstances * 3 ), 3, 1 );
                    //this.geometry.addAttribute( 'color', this.colors );
                    _this.colorIndices = new THREE.InstancedBufferAttribute(new Uint8Array(_this.g_maxInstances), 1, 1);
                    _this.geometry.addAttribute('colorIndex', _this.colorIndices);
                    _this.material = Chunk.staticMaterial;
                    //doesn't seem to work, might have to do it manually
                    //it's probably because THREEjs isn't aware of the offset
                    //this.mesh.geometry.computeBoundingBox();
                    _this.position.set(x, y, z);
                    _this.init();
                    return _this;
                }
                //initialize some values for testing //temporary
                Chunk.prototype.init = function () {
                    //initialize offsets //temporary for testing
                    //console.time("first pass");
                    for (var ix = 0; ix < this.width; ix++) {
                        for (var iy = 0; iy < this.height; iy++) {
                            for (var iz = 0; iz < this.depth; iz++) {
                                this.setVoxelAt(ix, iy, iz, THREE.Math.randInt(0, 1) * THREE.Math.randInt(1, 256) - 1);
                            }
                        }
                    }
                    //console.timeEnd("first pass");
                    this.setBoundingBox();
                    this.cleanUp();
                };
                //index cpu side
                Chunk.prototype.C_getVoxelIndexAt = function (x, y, z) {
                    return x * this.height * this.depth + y * this.depth + z;
                };
                //index gpu side
                Chunk.prototype.G_getVoxelBufferIndexAt = function (x, y, z) {
                    return this.voxelsBufferIndices[this.C_getVoxelIndexAt(x, y, z)];
                };
                //returns the color of the voxel (0 is air)
                Chunk.prototype.C_getVoxelAt = function (x, y, z) {
                    return this.voxels[this.C_getVoxelIndexAt(x, y, z)];
                };
                //color 0 is invisible/disabled/air
                //up to 255 colors since voxels is Uint8Array
                //maybe later on more? gotta check performance
                Chunk.prototype.setVoxelAt = function (x, y, z, color) {
                    //get and store the cpu index
                    var c_index = this.C_getVoxelIndexAt(x, y, z);
                    if (color == -1) {
                        //don't want to remove an already removed voxel
                        //if(this.voxels[c_index] != 0)
                        if (this.voxelsSolid[c_index]) {
                            this.removeVoxelAt(x, y, z);
                        }
                    }
                    else {
                        //voxel was previously air, add it
                        //if(this.voxels[c_index] == 0)
                        if (!this.voxelsSolid[c_index]) {
                            this.addVoxelAt(x, y, z, color);
                        }
                        else {
                            this.setVoxelColorAt(x, y, z, color);
                        }
                    }
                };
                Chunk.prototype.addVoxelAt = function (x, y, z, color) {
                    this.G_addVoxelAt(x, y, z, color);
                    var c_index = this.C_getVoxelIndexAt(x, y, z);
                    //set the voxel to solid
                    this.voxelsSolid[c_index] = true;
                    //set the color cpu side
                    this.voxels[c_index] = color;
                };
                //do not call directly
                //safety checks are executed by setVoxelAt
                Chunk.prototype.G_addVoxelAt = function (x, y, z, color) {
                    //TODO
                    //check if visible, else cull it
                    //check neighbour culling
                    var c_index = this.C_getVoxelIndexAt(x, y, z);
                    this.offsets.setXYZ(this.voxelsVisible, x, y, z);
                    var vecColor = this.getColorFromPalette(color);
                    //this.colors.setXYZ(this.voxelsVisible, vecColor.x,vecColor.y,vecColor.z);
                    this.colorIndices.setX(this.voxelsVisible, color);
                    this.voxelsBufferIndices[c_index] = this.voxelsVisible + 1;
                    this.voxelsReverseBufferIndices[this.voxelsVisible + 1] = c_index;
                    /*this.voxelsBufferIndices[this.voxelsVisible+1] = c_index;
                    this.voxelsReverseBufferIndices[c_index] = this.voxelsVisible+1;*/
                    this.voxelsVisible++;
                    this.dirty = true;
                };
                Chunk.prototype.setVoxelColorAt = function (x, y, z, color) {
                    this.G_setVoxelColorAt(x, y, z, color);
                    var c_index = this.C_getVoxelIndexAt(x, y, z);
                    //set the color cpu side
                    this.voxels[c_index] = color;
                };
                //do not call directly
                //safety checks are executed by setVoxelAt
                Chunk.prototype.G_setVoxelColorAt = function (x, y, z, color) {
                    var g_index = this.G_getVoxelBufferIndexAt(x, y, z);
                    //if index is 0, the voxel has been culled or is air, so no need to set it's color
                    if (g_index != 0) {
                        var col = this.getColorFromPalette(color);
                        //this.colors.setXYZ(g_index-1, col.x, col.y, col.z);
                        this.colorIndices.setX(g_index - 1, color);
                        this.dirty = true;
                    }
                };
                Chunk.prototype.removeVoxelAt = function (x, y, z) {
                    this.G_removeVoxelAt(x, y, z);
                    var c_index = this.C_getVoxelIndexAt(x, y, z);
                    //set the voxel to air
                    this.voxelsSolid[c_index] = false;
                    //no need to change the color since it's air
                };
                //do not call directly
                //safety checks are executed by setVoxelAt
                Chunk.prototype.G_removeVoxelAt = function (x, y, z) {
                    var g_index = this.G_getVoxelBufferIndexAt(x, y, z);
                    var c_index = this.C_getVoxelIndexAt(x, y, z);
                    //if index is 0, the voxel has been culled or is air, so no need to remove it
                    if (g_index != 0) {
                        //g_index of last voxel rendered
                        var g_index2 = this.voxelsVisible - 1;
                        //don't forget that g_buffer indices are 1 too big
                        this.offsets.setXYZ(g_index - 1, this.offsets.getX(g_index2), this.offsets.getY(g_index2), this.offsets.getZ(g_index2));
                        //this.colors.setXYZ(g_index-1, this.colors.getX(g_index2), this.colors.getY(g_index2), this.colors.getZ(g_index2));
                        this.colorIndices.setX(g_index - 1, this.colorIndices.getX(g_index2));
                        this.voxelsBufferIndices[this.voxelsReverseBufferIndices[g_index2 + 1]] = this.voxelsBufferIndices[c_index];
                        this.voxelsReverseBufferIndices[this.voxelsBufferIndices[c_index]] = this.voxelsReverseBufferIndices[g_index2 + 1];
                        this.voxelsBufferIndices[c_index] = 0;
                        this.voxelsReverseBufferIndices[0] = c_index;
                        this.voxelsVisible--;
                        this.dirty = true;
                    }
                };
                Chunk.prototype.raycast = function (raycaster, intersects) {
                    var inverseMatrix = new THREE.Matrix4();
                    var ray = new THREE.Ray();
                    var matrixWorld = this.matrixWorld;
                    var sphere = new THREE.Sphere();
                    sphere.copy(this.geometry.boundingSphere);
                    sphere.applyMatrix4(matrixWorld);
                    if (raycaster.ray.intersectsSphere(sphere) !== false) {
                        inverseMatrix.getInverse(matrixWorld);
                        ray.copy(raycaster.ray).applyMatrix4(inverseMatrix);
                        // Check boundingBox before continuing
                        if (this.geometry.boundingBox !== null) {
                            var hit = ray.intersectBox(this.geometry.boundingBox);
                            if (hit !== null) {
                                var distance = new THREE.Vector3().subVectors(ray.origin, hit).lengthSq();
                                var hitLocal = hit.clone();
                                //hit coordinates to world
                                hit.applyMatrix4(matrixWorld);
                                //TODO: implement THREE.Intersection!!!
                                intersects.push({ object: this, distance: distance, point: hit, hitLocal: hitLocal });
                                this.rayToVoxels(hitLocal, ray.direction);
                            }
                        }
                    }
                };
                Chunk.prototype.rayToVoxels = function (origin, direction) {
                    //to make sure origin is an in bound voxel
                    direction.normalize();
                    origin.add(direction.clone().multiplyScalar(0.00000001));
                    //the c * vector at which there is the next coordinate jump
                    var cx, cy, cz;
                    //store if ascending or descending coordinates
                    //either -1 or 1
                    var xunit, yunit, zunit;
                    if (direction.x > 0) {
                        cx = ((Math.ceil(origin.x) - origin.x) / direction.x);
                        xunit = 1;
                    }
                    else {
                        cx = ((origin.x - Math.floor(origin.x)) / -direction.x);
                        xunit = -1;
                    }
                    if (direction.y > 0) {
                        cy = ((Math.ceil(origin.y) - origin.y) / direction.y);
                        yunit = 1;
                    }
                    else {
                        cy = ((origin.y - Math.floor(origin.y)) / -direction.y);
                        yunit = -1;
                    }
                    if (direction.z > 0) {
                        cz = ((Math.ceil(origin.z) - origin.z) / direction.z);
                        zunit = 1;
                    }
                    else {
                        cz = ((origin.z - Math.floor(origin.z)) / -direction.z);
                        zunit = -1;
                    }
                    //get the first voxel coordinate
                    var x = Math.floor(origin.x);
                    var y = Math.floor(origin.y);
                    var z = Math.floor(origin.z);
                    //get the c unit to jump to the next coordinate
                    var cxunit = 1.0 / Math.abs(direction.x);
                    var cyunit = 1.0 / Math.abs(direction.y);
                    var czunit = 1.0 / Math.abs(direction.z);
                    //the face hit by the ray
                    var face;
                    //get the first face
                    //step back a voxel
                    var icx = cx - cxunit;
                    var icy = cy - cyunit;
                    var icz = cz - czunit;
                    //look for the closest voxel coord step (here the biggest since all < 0)
                    if (icx >= icy && icx >= icz)
                        face = xunit * 1;
                    else if (icy >= icz)
                        face = yunit * 2;
                    else
                        face = zunit * 3;
                    var voxels = [];
                    while (x >= 0 && x < this.width && y >= 0 && y < this.height && z >= 0 && z < this.depth) {
                        //push the previous voxel, it is in bounds
                        //but only if it is solid
                        if (this.voxelsSolid[this.C_getVoxelIndexAt(x, y, z)])
                            voxels.push({ coord: new THREE.Vector3(x, y, z), face: face });
                        //check which direction is the nex to jump
                        if (cx < cy && cx < cz) {
                            x += xunit;
                            cx += cxunit;
                            //-1 or 1
                            face = xunit * 1;
                        }
                        else if (cy < cz) {
                            y += yunit;
                            cy += cyunit;
                            //-2 or 2
                            face = yunit * 2;
                        }
                        else {
                            z += zunit;
                            cz += czunit;
                            //-3 or 2
                            face = zunit * 3;
                        }
                    }
                    if (mouse.left) {
                        // temp
                        for (var i = 0; i < voxels.length; i++) {
                            this.setVoxelAt(voxels[i].coord.x, voxels[i].coord.y, voxels[i].coord.z, colorPalette.selectedColor);
                        }
                        this.cleanUp();
                        //temp
                    }
                    return voxels;
                };
                Chunk.prototype.isVoxelVisibleAt = function (x, y, z) {
                };
                //get the color from the palette and return it as Vector3
                Chunk.prototype.getColorFromPalette = function (color) {
                    //color--;
                    return colorPalette.colors[color];
                };
                //called after the chunk has been dirtied to clean it up
                //has a list of tasks to perform/check
                //updates the gpu buffers
                Chunk.prototype.cleanUp = function () {
                    if (this.dirty) {
                        this.offsets.needsUpdate = true;
                        //this.colors.needsUpdate = true;
                        this.colorIndices.needsUpdate = true;
                        this.geometry.maxInstancedCount = this.voxelsVisible;
                        this.dirty = false;
                        //might want to update bounding box
                    }
                };
                Chunk.prototype.setBoundingBox = function () {
                    this.geometry.boundingBox = new THREE.Box3(new THREE.Vector3(0.0, 0.0, 0.0), new THREE.Vector3(this.width, this.height, this.depth));
                    //required to prevent too early culling
                    this.geometry.boundingSphere = new THREE.Sphere(new THREE.Vector3(this.width / 2.0, this.height / 2.0, this.depth / 2.0), Math.sqrt(this.width * this.width + this.height * this.height + this.depth * this.depth) / 2.0);
                };
                return Chunk;
            }(THREE.Mesh));
            //cube geometry, static because not going to change
            //might give possibility to change that later on to give users the ability to customize voxel shape
            //but boundingbox would need update, as well as exporting, so a LOT of work
            Chunk.verticesArray = [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1];
            Chunk.vertices = new THREE.BufferAttribute(new Float32Array(Chunk.verticesArray), 3);
            Chunk.indicesArray = [0, 1, 3, 6, 0, 2, 5, 0, 4, 6, 4, 0, 0, 3, 2, 5, 1, 0, 3, 1, 5, 7, 4, 6, 4, 7, 5, 7, 6, 2, 7, 2, 3, 7, 3, 5];
            Chunk.indices = new THREE.BufferAttribute(new Uint16Array(Chunk.indicesArray), 1);
            //might change to triangles if culling is worth performance wise
            movementSpeed = 10;
            ColorInput = (function () {
                function ColorInput() {
                    var _this = this;
                    this.isSelected = false;
                    this.domElement = document.createElement("input");
                    this.domElement.type = "color";
                    this.domElement.onchange = function () { _this.onChange(); };
                    this.domElement.addEventListener('click', function (ev) { _this.onClick(ev); }, false);
                    this.domElement.addEventListener('contextmenu', function (ev) { _this.onContextmenu(ev); }, false);
                }
                ColorInput.prototype.setColor = function (red, green, blue) {
                    this.domElement.className = "colorPaletteButton";
                    this.domElement.style.backgroundColor = "rgb(" + red + "," + green + "," + blue + ")";
                    this.domElement.style.borderColor = "rgb(" + red + "," + green + "," + blue + ")";
                    this.domElement.value = rgbToHex(red, green, blue);
                };
                ColorInput.prototype.onChange = function () {
                    this.domElement.style.backgroundColor = this.domElement.value;
                    var col = hexToRGB(this.domElement.value);
                    this.vecColor = new THREE.Vector3(col.red / 255.0, col.green / 255.0, col.blue / 255.0);
                    this.colorPalette.colors[this.index] = this.vecColor;
                };
                //first left click: select, second: open color picker
                //would prefer left select, right open BUT only able to use real left click event :/
                ColorInput.prototype.onClick = function (ev) {
                    if (this.isSelected) {
                        return true;
                    }
                    else {
                        ev.returnValue = false;
                        if (ev.preventDefault)
                            ev.preventDefault();
                        this.colorPalette.setSelectedColor(this.index);
                        return false;
                    }
                };
                //right click select the ColorInput
                ColorInput.prototype.onContextmenu = function (ev) {
                    ev.preventDefault();
                    colorPalette.setSelectedColor(this.index);
                    return false;
                };
                return ColorInput;
            }());
            ColorPalette = (function () {
                function ColorPalette() {
                    this.colorInputs = [];
                    this.colors = [];
                    this.selectedColor = 0;
                    this.domElement = document.getElementById("colorPalette");
                    var color = 0;
                    for (var i = 0; i < 32; i++) {
                        var line = document.createElement("div");
                        line.className = "colorPaletteLine";
                        this.domElement.appendChild(line);
                        for (var j = 0; j < 8; j++) {
                            var red = Math.floor(color / 36) * 36;
                            var green = Math.floor(color % 36 / 6) * 51;
                            var blue = (color % 6) * 51;
                            var vecColor = new THREE.Vector3(red / 255.0, green / 255.0, blue / 255.0);
                            var colorInput = new ColorInput();
                            colorInput.vecColor = vecColor;
                            colorInput.colorPalette = this;
                            colorInput.index = color;
                            colorInput.setColor(red, green, blue);
                            line.appendChild(colorInput.domElement);
                            this.colorInputs.push(colorInput);
                            this.colors[color] = vecColor;
                            color++;
                        }
                    }
                    this.colorInputs[0].domElement.click();
                }
                ColorPalette.prototype.setSelectedColor = function (color) {
                    this.colorInputs[this.selectedColor].domElement.classList.remove("colorPaletteSelected");
                    this.colorInputs[this.selectedColor].isSelected = false;
                    this.colorInputs[color].domElement.classList.add("colorPaletteSelected");
                    this.colorInputs[color].isSelected = true;
                    this.selectedColor = color;
                };
                return ColorPalette;
            }());
            colorPalette = new ColorPalette();
            // static material
            Chunk.staticMaterial = new THREE.RawShaderMaterial({
                uniforms: {
                    colorPalette: { type: 'v3v', value: colorPalette.colors }
                },
                vertexShader: document.getElementById('vertexShader').textContent,
                fragmentShader: document.getElementById('fragmentShader').textContent,
                side: THREE.FrontSide,
                transparent: false
            });
            init();
            setupUI();
            window.requestAnimationFrame(render);
        }
    };
});
//# sourceMappingURL=mainTypeScript.js.map