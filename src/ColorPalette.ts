/// <reference path="ColorInput.ts" />
namespace VoxelEditor{
export class ColorPalette
{
    domElement: HTMLElement;
    colorInputs: ColorInput[] = [];
    colors: THREE.Vector3[] = [];
    selectedColor: number = 0;
    constructor()
    {
        this.domElement = document.getElementById("colorPalette");
        let color = 0;
        for(let i = 0; i < 32; i++)
        {
            let line = document.createElement("div");
            line.className = "colorPaletteLine";
            this.domElement.appendChild(line);
            for(let j = 0; j < 8; j++)
            {
                let red = Math.floor(color/36)*36;
                let green = Math.floor(color%36/6)*51;
                let blue = (color%6)*51;

                let vecColor = new THREE.Vector3(red/255.0,green/255.0,blue/255.0);

                let colorInput = new ColorInput();
                colorInput.vecColor = vecColor;
                colorInput.colorPalette = this;
                colorInput.index = color;
                colorInput.setColor(red,green,blue);


                line.appendChild(colorInput.domElement);


                this.colorInputs.push(colorInput);

                this.colors[color] = vecColor;

                color++;
            }
        }
        this.colorInputs[0].domElement.click();
    }

    setSelectedColor(color:number)
    {
        this.colorInputs[this.selectedColor].domElement.classList.remove("colorPaletteSelected");
        this.colorInputs[this.selectedColor].isSelected = false;
        this.colorInputs[color].domElement.classList.add("colorPaletteSelected"); 
        this.colorInputs[color].isSelected = true;
        this.selectedColor = color;
    }

    

}
}
