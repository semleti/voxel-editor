var container, stats;

var camera, scene, renderer;

var mesh;

var clock = new THREE.Clock();

init();
animate();

function init() {

	container = document.getElementById( 'container' );

	//

	camera = new THREE.PerspectiveCamera( 37, window.innerWidth / window.innerHeight, 1, 8000 );
	camera.position.z = 2750;
	camera.position.y = 1250;

	scene = new THREE.Scene();

	camera.lookAt( scene.position );

	// 150,000 cubes
	// 12 triangles per cube (6 quads)

	var triangles = 12 * 150000;

	// BufferGeometry with unindexed triangles
	// use vertex colors to store centers of rotations

	var geometry = new THREE.BufferGeometry();

	geometry.attributes = {

		position: {
			itemSize: 3,
			array: new Float32Array( triangles * 3 * 3 ),
			numItems: triangles * 3 * 3
		},

		normal: {
			itemSize: 3,
			array: new Float32Array( triangles * 3 * 3 ),
			numItems: triangles * 3 * 3
		},

		color: {
			itemSize: 3,
			array: new Float32Array( triangles * 3 * 3 ),
			numItems: triangles * 3 * 3
		}

	}

	var positions = geometry.attributes.position.array;
	var normals = geometry.attributes.normal.array;
	var colors = geometry.attributes.color.array;

	// Generate a single buffer with all the cubes

	var n = 8000, n2 = n/2;	// triangles spread in the cube
	var d = 10, d2 = d/2;	// individual triangle size

	//

	var color = new THREE.Color();

	var pA = new THREE.Vector3();
	var pB = new THREE.Vector3();
	var pC = new THREE.Vector3();

	var cb = new THREE.Vector3();
	var ab = new THREE.Vector3();

	var m = new THREE.Matrix4();
	var m2 = new THREE.Matrix4();

	var e = new THREE.Vector3( 0, 0, 0 );
	var t = new THREE.Vector3();
	var tt = new THREE.Vector3();
	var u = new THREE.Vector3( 0, 1, 0 );

	var v1 = new THREE.Vector3( 0, 0, 0 );
	var v2 = new THREE.Vector3( d, 0, 0 );
	var v3 = new THREE.Vector3( d, d, 0 );
	var v4 = new THREE.Vector3( 0, d, 0 );

	var v1b = new THREE.Vector3( 0, 0, d );
	var v2b = new THREE.Vector3( d, 0, d );
	var v3b = new THREE.Vector3( d, d, d );
	var v4b = new THREE.Vector3( 0, d, d );

	//

	function addTriangle( k, x, y, z, vc, vb, va ) {

		// positions

		pA.copy( va );
		pB.copy( vb );
		pC.copy( vc );

		t.set( x, y, z );
		t.multiplyScalar( 0.5 );

		m.lookAt( e, tt, u );

		m2.makeTranslation( t );

		m2.multiplySelf( m );

		m2.multiplyVector3( pA );
		m2.multiplyVector3( pB );
		m2.multiplyVector3( pC );

		var ax = pA.x;
		var ay = pA.y;
		var az = pA.z;

		var bx = pB.x;
		var by = pB.y;
		var bz = pB.z;

		var cx = pC.x;
		var cy = pC.y;
		var cz = pC.z;

		var j = k * 9;

		positions[ j ]     = ax;
		positions[ j + 1 ] = ay;
		positions[ j + 2 ] = az;

		positions[ j + 3 ] = bx;
		positions[ j + 4 ] = by;
		positions[ j + 5 ] = bz;

		positions[ j + 6 ] = cx;
		positions[ j + 7 ] = cy;
		positions[ j + 8 ] = cz;

		// flat face normals

		pA.set( ax, ay, az );
		pB.set( bx, by, bz );
		pC.set( cx, cy, cz );

		cb.sub( pC, pB );
		ab.sub( pA, pB );
		cb.crossSelf( ab );

		cb.normalize();

		var nx = cb.x;
		var ny = cb.y;
		var nz = cb.z;

		normals[ j ]     = nx;
		normals[ j + 1 ] = ny;
		normals[ j + 2 ] = nz;

		normals[ j + 3 ] = nx;
		normals[ j + 4 ] = ny;
		normals[ j + 5 ] = nz;

		normals[ j + 6 ] = nx;
		normals[ j + 7 ] = ny;
		normals[ j + 8 ] = nz;

		// colors

		color.setRGB( t.x, t.y, t.z );

		colors[ j ]     = color.r;
		colors[ j + 1 ] = color.g;
		colors[ j + 2 ] = color.b;

		colors[ j + 3 ] = color.r;
		colors[ j + 4 ] = color.g;
		colors[ j + 5 ] = color.b;

		colors[ j + 6 ] = color.r;
		colors[ j + 7 ] = color.g;
		colors[ j + 8 ] = color.b;

	}

	//

	for ( var i = 0; i < triangles; i += 12 ) {

		var x = THREE.Math.randFloat( 0.1 * n, 0.2 * n ) * ( Math.random() > 0.5 ? 1 : -1 ) * THREE.Math.randInt( 0.5, 2 );
		var y = THREE.Math.randFloat( 0.1 * n, 0.2 * n ) * ( Math.random() > 0.5 ? 1 : -1 ) * THREE.Math.randInt( 0.5, 2 );
		var z = THREE.Math.randFloat( 0.1 * n, 0.2 * n ) * ( Math.random() > 0.5 ? 1 : -1 ) * THREE.Math.randInt( 0.5, 2 );

		tt.set( Math.random(), Math.random(), Math.random() );

		//

		addTriangle( i, 	x, y, z, v1, v2, v4 );
		addTriangle( i + 1, x, y, z, v2, v3, v4 );

		addTriangle( i + 2, x, y, z, v4b, v2b, v1b );
		addTriangle( i + 3, x, y, z, v4b, v3b, v2b );

		//

		addTriangle( i + 4, x, y, z, v1b, v2, v1 );
		addTriangle( i + 5, x, y, z, v1b, v2b, v2 );


		addTriangle( i + 6, x, y, z, v2b, v3, v2 );
		addTriangle( i + 7, x, y, z, v2b, v3b, v3 );

		//

		addTriangle( i + 8, x, y, z, v3b, v4, v3 );
		addTriangle( i + 9, x, y, z, v3b, v4b, v4 );

		addTriangle( i + 10, x, y, z, v1, v4, v1b );
		addTriangle( i + 11, x, y, z, v4, v4b, v1b );


	}

	geometry.computeBoundingSphere();

	// Set up custom shader material

	uniforms = {

		amplitude: { type: "f", value: 0.0 }

	};

	var material = new THREE.ShaderMaterial( {

		uniforms: 		uniforms,
		vertexShader:   document.getElementById( 'vertexShader' ).textContent,
		fragmentShader: document.getElementById( 'fragmentShader' ).textContent,
		vertexColors:   THREE.VertexColors

	});

	//

	mesh = new THREE.Mesh( geometry, material );
	scene.add( mesh );

	//

	renderer = new THREE.WebGLRenderer( { antialias: false, clearColor: 0x050505, clearAlpha: 1, alpha: false } );
	renderer.setSize( window.innerWidth, window.innerHeight );

	container.appendChild( renderer.domElement );

	//

	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	container.appendChild( stats.domElement );

	//

	window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight );

}

//

function animate() {

	requestAnimationFrame( animate );

	render();
	stats.update();

}

function render() {

	var time = Date.now() * 0.001;
	var delta = clock.getDelta();

	mesh.rotation.x = time * 0.025;
	mesh.rotation.y = time * 0.05;

	uniforms.amplitude.value += 2 * delta;

	renderer.render( scene, camera );

}
