namespace VoxelEditor{

let movementSpeed = 10;
let cameraPitch = 0;
let cameraYaw = 0;
let invertPitch = true;
let invertYaw = true;
let pitchSensivity = 1;
let yawSensivity = 1;

export function onContainerKeyboard( event:KeyboardEvent ):void {
    //event.preventDefault();
    //left
    let mov = new THREE.Vector3(0,0,0);
    if(event.key == 'q')
    {
        mov.add(new THREE.Vector3(-1,0,0));
    }
    //right
    if(event.key == 'd') {
        mov.add(new THREE.Vector3(1,0,0));
    }
    //up
    if(event.key == 'a')
    {
        mov.add(new THREE.Vector3(0,1,0));
    }
    //down
    if(event.key == 'e') {
        mov.add(new THREE.Vector3(0,-1,0));
    }
    //forward
    if(event.key == 'z')
    {
        mov.add(new THREE.Vector3(0,0,-1));
    }
    //backward
    if(event.key == 's') {
        mov.add(new THREE.Vector3(0,0,1));
    }
    mov.multiplyScalar(movementSpeed);
    mov.applyEuler(Globals.camera.rotation);

    Globals.camera.position.add(mov);

}

export function onContainerMouseWheel( event:MouseWheelEvent ):void {
    //event.preventDefault();
    /*let zoom = -event.wheelDelta/120 * 4;
    camera.fov += zoom;
    camera.fov=clamp(camera.fov,1,170);
    camera.updateProjectionMatrix();*/
    let mov = new THREE.Vector3( 0, 0, -event.wheelDelta/120 * 40 );
    mov.applyEuler(Globals.camera.rotation);
    Globals.camera.position.add(mov);
}

//TODO:
//when mouse pressed and in container border, keep moving
export function onContainerMouseMove( event:MouseEvent ):void {
    event.preventDefault();

    //middle mouse button
    //move camera left/right up/down
    if ( event.buttons & 4  ) {
        let mov = new THREE.Vector3( -(event.movementX), event.movementY, 0 );
        mov.applyEuler(Globals.camera.rotation);
        Globals.camera.position.add(mov);

    }
    //right mouse button
    //rotate camera
    if(event.buttons & 2) {
        cameraPitch = clamp(cameraPitch+event.movementY/100.0*pitchSensivity*(invertPitch?-1:1),-Math.PI/2,Math.PI/2);
        cameraYaw += event.movementX/100.0*yawSensivity*(invertYaw?-1:1);
        Globals.camera.setRotationFromEuler( new THREE.Euler(cameraPitch,cameraYaw,0,'YXZ'));
    }
    //left mouse button
    //move camera forward/backwards
    /*if(event.buttons & 1) {
        let move = new THREE.Vector3( 0,0, -event.movementX + event.movementY);
        move.applyEuler(camera.rotation);
        camera.position.add(move);
    }*/


    Globals.mouse.setPos(( event.offsetX / Globals.renderer.domElement.width ) * 2 - 1, - ( event.offsetY / Globals.renderer.domElement.height ) * 2 + 1);

    ToolSection.instance.mouseMove();
    //raycastHover();
    Globals.stage.raycast();
}


export function onContainerMouseDown(event:MouseEvent):void
{
    if(event.button == 0)
        Globals.mouse.left = true;
    else if(event.button == 1)
        Globals.mouse.middle = true;
    else if(event.button == 2 )
        Globals.mouse.right = true;
}

export function onContainerMouseUp(event:MouseEvent):void
{
    if(event.button == 0)
        Globals.mouse.left = false;
    else if(event.button == 1)
        Globals.mouse.middle = false;
    else if(event.button == 2 )
        Globals.mouse.right = false;
}

export function setupInputs():void
{
    Globals.renderer.domElement.addEventListener("mousedown",onContainerMouseDown);
    Globals.renderer.domElement.addEventListener("mouseup",onContainerMouseUp);

    Globals.renderer.domElement.addEventListener("mousemove",onContainerMouseMove);

    Globals.renderer.domElement.addEventListener("mousewheel", onContainerMouseWheel);

    //needs to be window to get key events
    window.onkeydown = onContainerKeyboard;

    //disable context menu in container
    Globals.renderer.domElement.addEventListener("contextmenu", function(e){
        e.preventDefault();
    }, false);

    
}
}