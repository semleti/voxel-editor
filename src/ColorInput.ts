/// <reference path="ColorPalette.ts" />
/// <reference path="Utils.ts" />
namespace VoxelEditor{
export class ColorInput
{
    domElement: HTMLInputElement;
    vecColor: THREE.Vector3;
    isSelected: boolean = false;
    index: number;
    colorPalette: ColorPalette;

    constructor()
    {
        this.domElement = document.createElement("input");
        this.domElement.type = "color";
        this.domElement.onchange = ()=>{this.onChange();};
        this.domElement.addEventListener('click', (ev)=>{this.onClick(ev);} ,false);
        this.domElement.addEventListener('contextmenu', (ev)=>{this.onContextmenu(ev);}, false);
    }

    setColor(red: number, green: number, blue: number):void
    {
        this.domElement.className = "colorPaletteButton";
        this.domElement.style.backgroundColor= "rgb(" + red +","+ green +","+ blue +")";
        this.domElement.style.borderColor= "rgb(" + red +","+ green +","+ blue +")";
        this.domElement.value = rgbToHex(red,green,blue);
    }

    onChange():any
    {
        this.domElement.style.backgroundColor = this.domElement.value;
        let col = hexToRGB(this.domElement.value);
        this.vecColor = new THREE.Vector3(col.red/255.0,col.green/255.0,col.blue/255.0);
        this.colorPalette.colors[this.index]=this.vecColor;
    }

    //first left click: select, second: open color picker
    //would prefer left select, right open BUT only able to use real left click event :/
    onClick(ev:MouseEvent):boolean
    {
        if(this.isSelected)
        {
            return true;
        }
        else
        {
            ev.returnValue = false;
            if(ev.preventDefault)
                ev.preventDefault();
            this.colorPalette.setSelectedColor(this.index);
            return false;
        }
    }

    //right click select the ColorInput
    onContextmenu(ev):boolean
    {
        ev.preventDefault();
        this.colorPalette.setSelectedColor(this.index);
        return false;
    }

}
}