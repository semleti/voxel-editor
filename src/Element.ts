namespace VoxelEditor{
//an element contains multiple chunks
//it has a rot + position
//handles accessing all the voxels in the chunks
export class Element extends THREE.Mesh{
    //will need to sort/insert/delete chunks if Element resized requires different number of chunks
    chunks:Chunk[] = [];
    cube:THREE.Mesh;
    //size of each chunk
    chunkWidth: number;
    chunkHeight: number;
    chunkDepth: number;
    //size of element
    width: number;
    height: number;
    depth: number;
    //size of element in chunks
    chunksOnX: number;
    chunksOnY: number;
    chunksOnZ: number;
    //displacement of chunks
    //needed to convert from element coord system to chunk coord system
    offsetX: number = 0;
    offsetY: number = 0;
    offsetZ: number = 0;
    constructor(width:number,height:number,depth:number, chunkWidth:number,chunkHeight:number,chunkDepth:number)
    {  
        super();
        let cubeGeometry = new THREE.CubeGeometry( 10, 10, 10 );
        let material = new THREE.MeshPhongMaterial( { color: 0xff0000, wireframe: false } );
        this.cube = new THREE.Mesh( cubeGeometry, material );
        this.add(this.cube);

        //the models Object3D
        this.position.set(0,0,0);

        //store the chunks preferred dims
        this.chunkWidth = chunkWidth;
        this.chunkHeight = chunkHeight;
        this.chunkDepth = chunkDepth;
        //store the desired dims
        //TODO: alllow for dimensions in voxels, not only chunks
        this.width = width;
        this.height = height;
        this.depth = depth;

        //needed to prevent a WebGL warning
        this.geometry = new THREE.PlaneGeometry(0,0);
        this.material = new THREE.MeshBasicMaterial();
        this.material.visible = false;

        this.init();
        Globals.scene.add(this);
    }

    addChunk(width:number,height:number,depth:number, x:number,y:number,z:number):void
    {
        let chunk = new Chunk(width,height,depth, x,y,z);
        this.chunks.push(chunk);
        this.add(chunk);
    }

    //can return null
    getFirstVoxelInRay(raycaster: THREE.Raycaster):VoxelCoords
    {
        if(this.rayIntersectsBoundingBox(raycaster))
        {
            let vox = this.rayToVoxels(null,null);
            if(vox.length > 0)
                return vox[0].voxel;
        }
        return null;
    }

    rayIntersectsBoundingBox(raycaster: THREE.Raycaster):boolean
    {
		let ray = new THREE.Ray();
        let inverseMatrix = new THREE.Matrix4();

        inverseMatrix.getInverse( this.matrixWorld );
        ray.copy( Globals.raycaster.ray ).applyMatrix4( inverseMatrix );


        if ( ray.intersectsSphere( this.geometry.boundingSphere ) !== false )
        {
            // Check boundingBox before continuing

            if ( this.geometry.boundingBox !== null ) {
                return  ray.intersectsBox( this.geometry.boundingBox )
            }
        }
        return false;
    }


    raycast( raycaster: THREE.Raycaster, intersects:any[] ):void
    {
        let inverseMatrix = new THREE.Matrix4();
		let ray = new THREE.Ray();
		let matrixWorld = this.matrixWorld;
        let hit = null;

        inverseMatrix.getInverse( matrixWorld );
        ray.copy( raycaster.ray ).applyMatrix4( inverseMatrix );

        //allows ray to hit even if inside the chunk
        if(this.geometry.boundingBox !== null  && this.geometry.boundingBox.containsPoint(ray.origin))
        {
            hit = ray.origin;
        }
        //outside of chunk, normal raycasting
        else if ( ray.intersectsSphere( this.geometry.boundingSphere ) !== false )
        {
            // Check boundingBox before continuing

            if ( this.geometry.boundingBox !== null ) {
                hit = ray.intersectBox( this.geometry.boundingBox );
            }
        }

        if ( hit !== null )
        {
            this.cube.position.set(hit.x,hit.y,hit.z);
            let distance = new THREE.Vector3().subVectors(ray.origin,hit).lengthSq();
            let hitLocal = hit.clone();
            //hit coordinates to world
            hit.applyMatrix4(matrixWorld);
            //TODO: implement THREE.Intersection!!!
            intersects.push({object:this,distance:distance,point:hit,hitLocal:hitLocal});

            this.rayToVoxels(hitLocal,ray.direction);
        }
    }







    resize()
    {
        //insert/remove chunks if necezssary
        //setBoundingBox
        //allow to resize on the small and big sides
        //need to check chunksOn{N}
    }

    init():void
    {
        this.chunksOnX = Math.ceil((this.width-1) / this.chunkWidth) + 1;
        this.chunksOnY = Math.ceil((this.height-1) / this.chunkHeight) + 1;
        this.chunksOnZ = Math.ceil((this.depth-1) / this.chunkDepth) + 1;

        for(let x = 0; x < this.chunksOnX; x++)
        {
            for(let y = 0; y < this.chunksOnY; y++)
            {
                for(let z = 0; z < this.chunksOnZ; z++)
                {
                    this.addChunk(this.chunkWidth,this.chunkHeight,this.chunkDepth,x*this.chunkWidth,y*this.chunkHeight,z*this.chunkDepth);
                }
            }
        }

        for(let ix = 0; ix < this.width; ix++)
        {
            for(let iy = 0; iy < this.height; iy++)
            {
                for(let iz = 0; iz < this.depth; iz++)
                {
                    this.setVoxelAt(ix,iy,iz,THREE.Math.randInt(0,1)*THREE.Math.randInt(1,256)-1);
                }
            }
        }
        
        this.setBoundingBox();
        this.cleanUp();

    }

    shift(x:number,y:number,z:number):void
    {
        //might want to choose if x > 0 or < 0 depending on the amount to copy
        //make sure values are in range
        x%=this.width;
        y%=this.height;
        z%=this.depth;

        //update offset
        this.offsetX += x;
        this.offsetY += y;
        this.offsetZ += z;

        //still need to copy the voxels crossing the line
        //check if shift an entire chunk (if so, less copy => cheaper)
        //suboptimal
        if(Math.abs(Math.floor(this.offsetX / this.chunkWidth) - Math.floor((this.offsetX + x) / this.chunkWidth)) >= 2)
        {
            //an entire chunk has been shifted
        } 


        //iterate over all chunks to update positions
        for(let chunk of this.chunks)
        {
            //apply offset
            let cx = chunk.position.x+x;
            let cy = chunk.position.y+y;
            let cz = chunk.position.z+z;
            //wrap position around if needed
            if(cx >= this.width) cx -= (this.width + this.chunkWidth);
            if(cx < -this.chunkWidth) cx += this.width + this.chunkWidth;
            if(cy >= this.height) cy -= (this.height + this.chunkHeight);
            if(cy < -this.chunkHeight) cy += this.height + this.chunkHeight;
            if(cz >= this.depth) cz -= (this.depth + this.chunkDepth);
            if(cz < -this.chunkDepth) cz += this.depth + this.chunkDepth;
            //apply the new position
            chunk.position.set(cx,cy,cz)
        }
        
    }


    setBoundingBox():void
    {
        let width = this.width;
        let height = this.height;
        let depth = this.depth;
        this.geometry.boundingBox = new THREE.Box3(new THREE.Vector3(0.0,0.0,0.0), new THREE.Vector3(width,height,depth));
        //required to prevent too early culling
        this.geometry.boundingSphere = new THREE.Sphere(new THREE.Vector3(width/2.0,height/2.0,depth/2.0), Math.sqrt(width*width+height*height+depth*depth)/2.0);
    }

    getVoxelIndexAt(x:number,y:number,z:number)
    {
        x-=this.offsetX;
        y-=this.offsetY;
        z-=this.offsetZ;
        x=modulo(x,this.chunksOnX*this.chunkWidth);
        y=modulo(y,this.chunksOnY*this.chunkHeight);
        z=modulo(z,this.chunksOnZ*this.chunkDepth);
        let chunkX = Math.floor(x/this.chunkWidth);
        let chunkY = Math.floor(y/this.chunkHeight);
        let chunkZ = Math.floor(z/this.chunkDepth);
        x %= this.chunkWidth;
        y %= this.chunkHeight;
        z %= this.chunkDepth;
        let chunk = chunkX*this.chunksOnY*this.chunksOnZ + chunkY*this.chunksOnZ + chunkZ;
        return {chunk:chunk,x:x,y:y,z:z};
    }

    getVoxelAt(x:number,y:number,z:number):number
    {
        let index = this.getVoxelIndexAt(x,y,z);
        return this.chunks[index.chunk].C_getVoxelAt(index.x,index.y,index.z);
    }

    getVoxelSolidAt(x:number,y:number,z:number):boolean
    {
        let index = this.getVoxelIndexAt(x,y,z);
        return this.chunks[index.chunk].C_getVoxelSolidAt(index.x,index.y,index.z);
    }

    setVoxelAt(x:number,y:number,z:number,color:number):void
    {
        let index = this.getVoxelIndexAt(x,y,z);
        return this.chunks[index.chunk].setVoxelAt(index.x,index.y,index.z,color);
    }

    //cleans all the chunks
    cleanUp():void
    {
        for (let chunk of this.chunks)
        {
            chunk.cleanUp();
        } 
    }

    rayToVoxels(origin:THREE.Vector3, direction:THREE.Vector3, onlyFirst = false):VoxelHit[]
    {
        //to make sure origin is an in bound voxel
        direction.normalize();
        origin.add(direction.clone().multiplyScalar(0.00000001))


        //the c * vector at which there is the next coordinate jump
        let cx,cy,cz;

        //store if ascending or descending coordinates
        //either -1 or 1
        let xunit,yunit,zunit;

        if(direction.x > 0)
        {
            cx = ((Math.ceil(origin.x) - origin.x)/direction.x);
            xunit = 1;
        }
        else
        {
            cx = ((origin.x-Math.floor(origin.x))/-direction.x);
            xunit = -1;
        }

        if(direction.y > 0)
        {
            cy = ((Math.ceil(origin.y) - origin.y)/direction.y);
            yunit = 1;
        }
        else
        {
            cy = ((origin.y-Math.floor(origin.y))/-direction.y);
            yunit = -1;
        }

        if(direction.z > 0)
        {
            cz = ((Math.ceil(origin.z) - origin.z)/direction.z);
            zunit = 1;
        }
        else
        {
            cz = ((origin.z-Math.floor(origin.z))/-direction.z);
            zunit = -1;
        }

        //get the first voxel coordinate
        let x = Math.floor(origin.x);
        let y = Math.floor(origin.y);
        let z = Math.floor(origin.z);

        //get the c unit to jump to the next coordinate
        let cxunit = 1.0/Math.abs(direction.x);
        let cyunit = 1.0/Math.abs(direction.y);
        let czunit = 1.0/Math.abs(direction.z);

        //the face hit by the ray
        let face:number;
        //get the first face
        //step back a voxel
        let icx = cx - cxunit;
        let icy = cy - cyunit;
        let icz = cz - czunit;
        //look for the closest voxel coord step (here the biggest since all < 0)
        if(icx >= icy && icx >= icz)face = xunit * 1;
        else if(icy >= icz)face = yunit * 2;
        else face = zunit * 3;
        


        let voxelHits : VoxelHit[] = [];

        while(x >= 0 && x < this.width && y >= 0 && y < this.height && z >=0 && z < this.depth)
        {
            //push the previous voxel, it is in bounds
            //but only if it is solid
            if(this.getVoxelSolidAt(x,y,z))
            {
                voxelHits.push({voxel:{x:x,y:y,z:z},face:face});
                if(onlyFirst)
                    return voxelHits;
            }
            //check which direction is the nex to jump
            if(cx < cy && cx < cz)
            {
                x += xunit;
                cx += cxunit;
                //-1 or 1
                face = xunit * 1;
            }
            else if(cy < cz)
            {
                y += yunit;
                cy += cyunit;
                //-2 or 2
                face = yunit * 2;
            }
            else
            {
                z += zunit;
                cz += czunit;
                //-3 or 2
                face = zunit * 3;
            }
        }

        //TEMP!!!
        if(Globals.mouse.left)
        {
            // temp
            for(let hit of voxelHits)
            {
                this.setVoxelAt(hit.voxel.x,hit.voxel.y,hit.voxel.z,Globals.colorPalette.selectedColor);
            }
            this.cleanUp();
            //temp
        }

        return voxelHits;
    }

}
}