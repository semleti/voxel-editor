/// <reference path="Mouse.ts" />
/// <reference path="ColorPalette.ts" />
/// <reference path="Stage.ts" />
namespace VoxelEditor{
export class Globals{
    static mouse = new Mouse();
    static colorPalette = new ColorPalette();
    static scene = new THREE.Scene();
    static raycaster = new THREE.Raycaster();
    static camera: THREE.Camera;
    static stage = new Stage();
    static renderer: THREE.WebGLRenderer;
}
}