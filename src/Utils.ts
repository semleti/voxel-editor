namespace VoxelEditor{
export function componentToHex(c:number):string {
    let hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

export function rgbToHex(r:number, g:number, b:number):string {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

export function hexToRGB(hex:string){
    hex = hex.replace('#','');
    let r = parseInt(hex.substring(0,2), 16);
    let g = parseInt(hex.substring(2,4), 16);
    let b = parseInt(hex.substring(4,6), 16);
    return {red:r,green:g,blue:b};
}


export function clamp(number:number,min:number,max:number):number{
    if(number<min)
        return min;
    if(number>max)
        return max;
    return number;
}

//helper function for arguments with default value
export function pickValue(arg,def){
    return (typeof arg == 'undefined' ? def : arg);
}

export function modulo(a:number,b:number):number{
    let res = a%b;
    return res<0?res+b:res;
}

}