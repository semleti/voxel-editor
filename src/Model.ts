namespace VoxelEditor{
//a model can contain multiple elements
//it has a rot + position
//not sure about chunkWidth/height..
export class Model extends THREE.Mesh{
    elements:Element[] = [];    
    chunkWidth: number;
    chunkHeight: number;
    chunkDepth: number;
    constructor(chunkWidth:number,chunkHeight:number,chunkDepth:number)
    {  
        super();
        this.chunkWidth = chunkWidth;
        this.chunkHeight = chunkHeight;
        this.chunkDepth = chunkDepth;
        this.init();
        Globals.scene.add(this);
    }

    addElement(width:number, height:number, depth:number, x:number, y:number, z:number):void
    {
        let element = new Element(width,height,depth, this.chunkWidth,this.chunkHeight,this.chunkHeight);
        element.position.set(x,y,z);
        this.elements.push(element);
        this.add(element);
    }

    rayIntersectsBoundingBox(raycaster: THREE.Raycaster):boolean
    {
		let ray = new THREE.Ray();
        let inverseMatrix = new THREE.Matrix4();

        inverseMatrix.getInverse( this.matrixWorld );
        ray.copy( Globals.raycaster.ray ).applyMatrix4( inverseMatrix );


        if ( ray.intersectsSphere( this.geometry.boundingSphere ) !== false )
        {
            // Check boundingBox before continuing

            if ( this.geometry.boundingBox !== null ) {
                return  ray.intersectsBox( this.geometry.boundingBox )
            }
        }
        return false;
    }

    raycast():void
    {

        if(this.rayIntersectsBoundingBox(Globals.raycaster)||true)
        {
            let intersects = Globals.raycaster.intersectObjects( this.elements);
        }

    }

    init():void
    {
        this.addElement(16,32,32,0,0,0);
        this.elements[0].rotateX(1);
        this.addElement(64,64,64,128,0,0);
        this.addElement(32,32,32,0,0,64);
        this.setBoundingBox();
    }

    setBoundingBox():void
    {
        let box = new THREE.Box3();
        box.setFromObject(this);
        this.geometry.boundingBox = box;
        let width = box.max.x-box.min.x;
        let height = box.max.y-box.min.y;
        let depth = box.max.z-box.min.z;
        //required to prevent too early culling
        this.geometry.boundingSphere = new THREE.Sphere(new THREE.Vector3(width/2.0,height/2.0,depth/2.0), Math.sqrt(width*width+height*height+depth*depth)/2.0);
    }

}
}