var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var VoxelEditor;
(function (VoxelEditor) {
    var Mouse = (function () {
        function Mouse() {
            this.position = new THREE.Vector2(1, 1);
            this.previousPosition = new THREE.Vector2(1, 1);
            this.left = false;
            this.middle = false;
            this.right = false;
        }
        Mouse.prototype.setPos = function (x, y) {
            this.previousPosition.x = this.position.x;
            this.previousPosition.y = this.position.y;
            this.position.x = x;
            this.position.y = y;
        };
        return Mouse;
    }());
    VoxelEditor.Mouse = Mouse;
})(VoxelEditor || (VoxelEditor = {}));
var VoxelEditor;
(function (VoxelEditor) {
    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
    VoxelEditor.componentToHex = componentToHex;
    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }
    VoxelEditor.rgbToHex = rgbToHex;
    function hexToRGB(hex) {
        hex = hex.replace('#', '');
        var r = parseInt(hex.substring(0, 2), 16);
        var g = parseInt(hex.substring(2, 4), 16);
        var b = parseInt(hex.substring(4, 6), 16);
        return { red: r, green: g, blue: b };
    }
    VoxelEditor.hexToRGB = hexToRGB;
    function clamp(number, min, max) {
        if (number < min)
            return min;
        if (number > max)
            return max;
        return number;
    }
    VoxelEditor.clamp = clamp;
    //helper function for arguments with default value
    function pickValue(arg, def) {
        return (typeof arg == 'undefined' ? def : arg);
    }
    VoxelEditor.pickValue = pickValue;
    function modulo(a, b) {
        var res = a % b;
        return res < 0 ? res + b : res;
    }
    VoxelEditor.modulo = modulo;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="ColorPalette.ts" />
/// <reference path="Utils.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var ColorInput = (function () {
        function ColorInput() {
            var _this = this;
            this.isSelected = false;
            this.domElement = document.createElement("input");
            this.domElement.type = "color";
            this.domElement.onchange = function () { _this.onChange(); };
            this.domElement.addEventListener('click', function (ev) { _this.onClick(ev); }, false);
            this.domElement.addEventListener('contextmenu', function (ev) { _this.onContextmenu(ev); }, false);
        }
        ColorInput.prototype.setColor = function (red, green, blue) {
            this.domElement.className = "colorPaletteButton";
            this.domElement.style.backgroundColor = "rgb(" + red + "," + green + "," + blue + ")";
            this.domElement.style.borderColor = "rgb(" + red + "," + green + "," + blue + ")";
            this.domElement.value = VoxelEditor.rgbToHex(red, green, blue);
        };
        ColorInput.prototype.onChange = function () {
            this.domElement.style.backgroundColor = this.domElement.value;
            var col = VoxelEditor.hexToRGB(this.domElement.value);
            this.vecColor = new THREE.Vector3(col.red / 255.0, col.green / 255.0, col.blue / 255.0);
            this.colorPalette.colors[this.index] = this.vecColor;
        };
        //first left click: select, second: open color picker
        //would prefer left select, right open BUT only able to use real left click event :/
        ColorInput.prototype.onClick = function (ev) {
            if (this.isSelected) {
                return true;
            }
            else {
                ev.returnValue = false;
                if (ev.preventDefault)
                    ev.preventDefault();
                this.colorPalette.setSelectedColor(this.index);
                return false;
            }
        };
        //right click select the ColorInput
        ColorInput.prototype.onContextmenu = function (ev) {
            ev.preventDefault();
            this.colorPalette.setSelectedColor(this.index);
            return false;
        };
        return ColorInput;
    }());
    VoxelEditor.ColorInput = ColorInput;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="ColorInput.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var ColorPalette = (function () {
        function ColorPalette() {
            this.colorInputs = [];
            this.colors = [];
            this.selectedColor = 0;
            this.domElement = document.getElementById("colorPalette");
            var color = 0;
            for (var i = 0; i < 32; i++) {
                var line = document.createElement("div");
                line.className = "colorPaletteLine";
                this.domElement.appendChild(line);
                for (var j = 0; j < 8; j++) {
                    var red = Math.floor(color / 36) * 36;
                    var green = Math.floor(color % 36 / 6) * 51;
                    var blue = (color % 6) * 51;
                    var vecColor = new THREE.Vector3(red / 255.0, green / 255.0, blue / 255.0);
                    var colorInput = new VoxelEditor.ColorInput();
                    colorInput.vecColor = vecColor;
                    colorInput.colorPalette = this;
                    colorInput.index = color;
                    colorInput.setColor(red, green, blue);
                    line.appendChild(colorInput.domElement);
                    this.colorInputs.push(colorInput);
                    this.colors[color] = vecColor;
                    color++;
                }
            }
            this.colorInputs[0].domElement.click();
        }
        ColorPalette.prototype.setSelectedColor = function (color) {
            this.colorInputs[this.selectedColor].domElement.classList.remove("colorPaletteSelected");
            this.colorInputs[this.selectedColor].isSelected = false;
            this.colorInputs[color].domElement.classList.add("colorPaletteSelected");
            this.colorInputs[color].isSelected = true;
            this.selectedColor = color;
        };
        return ColorPalette;
    }());
    VoxelEditor.ColorPalette = ColorPalette;
})(VoxelEditor || (VoxelEditor = {}));
var VoxelEditor;
(function (VoxelEditor) {
    var Stage = (function () {
        function Stage() {
            this.models = [];
        }
        Stage.prototype.addModel = function (chunkWidth, chunkHeight, chunkDepth) {
            this.models.push(new VoxelEditor.Model(chunkWidth, chunkHeight, chunkDepth));
        };
        Stage.prototype.raycast = function () {
            VoxelEditor.Globals.raycaster.setFromCamera(VoxelEditor.Globals.mouse.position, VoxelEditor.Globals.camera);
            // calculate objects intersecting the picking ray
            var intersects = VoxelEditor.Globals.raycaster.intersectObjects(this.models);
        };
        return Stage;
    }());
    VoxelEditor.Stage = Stage;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="Mouse.ts" />
/// <reference path="ColorPalette.ts" />
/// <reference path="Stage.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var Globals = (function () {
        function Globals() {
        }
        return Globals;
    }());
    Globals.mouse = new VoxelEditor.Mouse();
    Globals.colorPalette = new VoxelEditor.ColorPalette();
    Globals.scene = new THREE.Scene();
    Globals.raycaster = new THREE.Raycaster();
    Globals.stage = new VoxelEditor.Stage();
    VoxelEditor.Globals = Globals;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="Globals.ts" />
/// <reference path="ColorPalette.ts" />
/// <reference path="Mouse.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var InstancedBufferGeometryMaxInstancedCount = (function (_super) {
        __extends(InstancedBufferGeometryMaxInstancedCount, _super);
        function InstancedBufferGeometryMaxInstancedCount() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return InstancedBufferGeometryMaxInstancedCount;
    }(THREE.InstancedBufferGeometry));
    VoxelEditor.InstancedBufferGeometryMaxInstancedCount = InstancedBufferGeometryMaxInstancedCount;
    //should ONLY get used by Element
    //might get replaced at some point, so nothing except Element should use it
    //should be "invisible" to the user/end_user
    var Chunk = (function (_super) {
        __extends(Chunk, _super);
        function Chunk(width, height, depth, x, y, z) {
            var _this = _super.call(this) || this;
            _this.palette = [];
            //no need for a set, voxels should only be added once per frame if the tools don't mess up!
            //and if they do, the computation cost for the few repeats will be <<<< the cost of checking uniqueness of voxels in a set
            _this.voxelsToCheckAndCull = [];
            _this.width = width;
            _this.height = height;
            _this.depth = depth;
            _this.c_voxelCount = _this.width * _this.height * _this.depth;
            _this.voxels = new Uint8Array(_this.c_voxelCount);
            _this.voxelsSolid = [];
            //might be smaller if chunks have a max size (ie: Uint16 if chunk:32*32*32 which sound reasonnable)
            //used to reduce memory swapping when voxels are added/removedfrom/to the gpu_buffer
            //might be a pain in the ass to resize, so maybe do a "virtual" resize if the user wants a smaller chunk to work with?
            _this.voxelsBufferIndices = new Uint32Array(_this.c_voxelCount);
            _this.voxelsReverseBufferIndices = new Uint32Array(_this.c_voxelCount);
            _this.voxelsVisible = 0;
            //might want to change to a Vector3 array
            //this.palette = new Float32Array(255 * 3);
            _this.palette = [0.2, 0.2, 0.2, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0];
            //used to track wether the chunk has been dirtied or not
            _this.dirty = true;
            //worst case scenario: 1/2 of the space is filled (checker pattern) so at max 1/2 of cubes visible
            //GOD DAMMIT!: if 2 full layers + 1 air layer + 2 full layers... a lot more than 1/2! (> 2/3, even with smart culling(not accounting for rotation, but accounting for rotation might be too much work for cpu + constant gpu buffer updating is bad))
            //might need an advanced culling feature
            //let's ignore culling for now!
            _this.g_maxInstances = _this.width * _this.height * _this.depth;
            _this.geometry = new InstancedBufferGeometryMaxInstancedCount();
            _this.geometry.maxInstancedCount = _this.g_maxInstances;
            //the "position"(offest) of each voxel relative to the chunk, needs 3 Uint8 per voxel but glsl wants floats, so there we go
            _this.offsets = new THREE.InstancedBufferAttribute(new Float32Array(_this.g_maxInstances * 3), 3, 1);
            _this.geometry.addAttribute('position', Chunk.vertices);
            _this.geometry.setIndex(Chunk.indices);
            _this.geometry.addAttribute('offset', _this.offsets);
            //color for each voxel
            //need 3 floats per color
            //this.colors = new THREE.InstancedBufferAttribute( new Float32Array( this.g_maxInstances * 3 ), 3, 1 );
            //this.geometry.addAttribute( 'color', this.colors );
            _this.colorIndices = new THREE.InstancedBufferAttribute(new Uint8Array(_this.g_maxInstances), 1, 1);
            _this.geometry.addAttribute('colorIndex', _this.colorIndices);
            _this.material = Chunk.staticMaterial;
            //doesn't seem to work, might have to do it manually
            //it's probably because THREEjs isn't aware of the offset
            //this.mesh.geometry.computeBoundingBox();
            _this.position.set(x, y, z);
            _this.init();
            return _this;
        }
        //initialize some values for testing //temporary
        Chunk.prototype.init = function () {
            //initialize offsets //temporary for testing
            //console.time("first pass");
            /*for(let ix = 0; ix < this.width; ix++)
            {
                for(let iy = 0; iy < this.height; iy++)
                {
                    for(let iz = 0; iz < this.depth; iz++)
                    {
                        this.setVoxelAt(ix,iy,iz,THREE.Math.randInt(1,1)*THREE.Math.randInt(1,256)-1);
                    }
                }
            }*/
            //console.timeEnd("first pass");
            this.setBoundingBox();
            this.cleanUp();
        };
        //index cpu side
        Chunk.prototype.C_getVoxelIndexAt = function (x, y, z) {
            return x * this.height * this.depth + y * this.depth + z;
        };
        //index gpu side
        Chunk.prototype.G_getVoxelBufferIndexAt = function (x, y, z) {
            return this.voxelsBufferIndices[this.C_getVoxelIndexAt(x, y, z)];
        };
        //returns the color of the voxel
        Chunk.prototype.C_getVoxelAt = function (x, y, z) {
            return this.voxels[this.C_getVoxelIndexAt(x, y, z)];
        };
        //returns the solidity of the voxel
        Chunk.prototype.C_getVoxelSolidAt = function (x, y, z) {
            return this.voxelsSolid[this.C_getVoxelIndexAt(x, y, z)];
        };
        //color 0 is invisible/disabled/air
        //up to 255 colors since voxels is Uint8Array
        //maybe later on more? gotta check performance
        Chunk.prototype.setVoxelAt = function (x, y, z, color) {
            //get and store the cpu index
            var c_index = this.C_getVoxelIndexAt(x, y, z);
            if (color == -1) {
                //don't want to remove an already removed voxel
                //if(this.voxels[c_index] != 0)
                if (this.voxelsSolid[c_index]) {
                    this.removeVoxelAt(x, y, z);
                }
            }
            else {
                //voxel was previously air, add it
                //if(this.voxels[c_index] == 0)
                if (!this.voxelsSolid[c_index]) {
                    this.addVoxelAt(x, y, z, color);
                }
                else {
                    this.setVoxelColorAt(x, y, z, color);
                }
            }
        };
        Chunk.prototype.addVoxelAt = function (x, y, z, color) {
            //gonna get added to the G_ in the cleanup phase
            //this.G_addVoxelAt(x,y,z,color);
            var c_index = this.C_getVoxelIndexAt(x, y, z);
            //set the voxel to solid
            this.voxelsSolid[c_index] = true;
            //set the color cpu side
            this.voxels[c_index] = color;
            this.addVoxelAndNeighboursToCullCheck({ x: x, y: y, z: z });
        };
        //do not call directly
        //safety checks are executed by setVoxelAt
        Chunk.prototype.G_addVoxelAt = function (x, y, z, color) {
            //TODO
            //check if visible, else cull it
            //check neighbour culling
            var c_index = this.C_getVoxelIndexAt(x, y, z);
            this.offsets.setXYZ(this.voxelsVisible, x, y, z);
            var vecColor = this.getColorFromPalette(color);
            //this.colors.setXYZ(this.voxelsVisible, vecColor.x,vecColor.y,vecColor.z);
            this.colorIndices.setX(this.voxelsVisible, color);
            this.voxelsBufferIndices[c_index] = this.voxelsVisible + 1;
            this.voxelsReverseBufferIndices[this.voxelsVisible + 1] = c_index;
            /*this.voxelsBufferIndices[this.voxelsVisible+1] = c_index;
            this.voxelsReverseBufferIndices[c_index] = this.voxelsVisible+1;*/
            this.voxelsVisible++;
            this.dirty = true;
        };
        Chunk.prototype.setVoxelColorAt = function (x, y, z, color) {
            this.G_setVoxelColorAt(x, y, z, color);
            var c_index = this.C_getVoxelIndexAt(x, y, z);
            //set the color cpu side
            this.voxels[c_index] = color;
        };
        //do not call directly
        //safety checks are executed by setVoxelAt
        Chunk.prototype.G_setVoxelColorAt = function (x, y, z, color) {
            var g_index = this.G_getVoxelBufferIndexAt(x, y, z);
            //if index is 0, the voxel has been culled or is air, so no need to set it's color
            if (g_index != 0) {
                var col = this.getColorFromPalette(color);
                //this.colors.setXYZ(g_index-1, col.x, col.y, col.z);
                this.colorIndices.setX(g_index - 1, color);
                this.dirty = true;
            }
        };
        Chunk.prototype.removeVoxelAt = function (x, y, z) {
            //gonna get removed from G_ in the cleanup phase
            //this.G_removeVoxelAt(x,y,z);
            var c_index = this.C_getVoxelIndexAt(x, y, z);
            //set the voxel to air
            this.voxelsSolid[c_index] = false;
            //no need to change the color since it's air
            this.addVoxelAndNeighboursToCullCheck({ x: x, y: y, z: z });
        };
        //do not call directly
        //safety checks are executed by setVoxelAt
        Chunk.prototype.G_removeVoxelAt = function (x, y, z) {
            var g_index = this.G_getVoxelBufferIndexAt(x, y, z);
            var c_index = this.C_getVoxelIndexAt(x, y, z);
            //if index is 0, the voxel has been culled or is air, so no need to remove it
            if (g_index != 0) {
                //g_index of last voxel rendered
                var g_index2 = this.voxelsVisible - 1;
                //don't forget that g_buffer indices are 1 too big
                this.offsets.setXYZ(g_index - 1, this.offsets.getX(g_index2), this.offsets.getY(g_index2), this.offsets.getZ(g_index2));
                //this.colors.setXYZ(g_index-1, this.colors.getX(g_index2), this.colors.getY(g_index2), this.colors.getZ(g_index2));
                this.colorIndices.setX(g_index - 1, this.colorIndices.getX(g_index2));
                this.voxelsBufferIndices[this.voxelsReverseBufferIndices[g_index2 + 1]] = this.voxelsBufferIndices[c_index];
                this.voxelsReverseBufferIndices[this.voxelsBufferIndices[c_index]] = this.voxelsReverseBufferIndices[g_index2 + 1];
                this.voxelsBufferIndices[c_index] = 0;
                this.voxelsReverseBufferIndices[0] = c_index;
                this.voxelsVisible--;
                this.dirty = true;
            }
        };
        /*raycast( raycaster: THREE.Raycaster, intersects:any[] ):void
        {
    
            let inverseMatrix = new THREE.Matrix4();
            let ray = new THREE.Ray();
            let matrixWorld = this.matrixWorld;
            let sphere = new THREE.Sphere();
            let hit = null;
    
            sphere.copy( this.geometry.boundingSphere );
            sphere.applyMatrix4( matrixWorld );
    
            inverseMatrix.getInverse( matrixWorld );
            ray.copy( raycaster.ray ).applyMatrix4( inverseMatrix );
    
            //allows ray to hit even if inside the chunk
            if(this.geometry.boundingBox !== null  && this.geometry.boundingBox.containsPoint(ray.origin))
            {
                hit = ray.origin;
            }
            //outside of chunk, normal raycasting
            else if ( raycaster.ray.intersectsSphere( sphere ) !== false )
            {
                // Check boundingBox before continuing
    
                if ( this.geometry.boundingBox !== null ) {
                    hit = ray.intersectBox( this.geometry.boundingBox );
                }
            }
    
            if ( hit !== null )
            {
                let distance = new THREE.Vector3().subVectors(ray.origin,hit).lengthSq();
                let hitLocal = hit.clone();
                //hit coordinates to world
                hit.applyMatrix4(matrixWorld);
                //TODO: implement THREE.Intersection!!!
                intersects.push({object:this,distance:distance,point:hit,hitLocal:hitLocal});
    
                this.rayToVoxels(hitLocal,ray.direction);
            }
        }*/
        /*rayToVoxels(origin:THREE.Vector3, direction:THREE.Vector3, onlyFirst = false):VoxelHit[]
        {
            //to make sure origin is an in bound voxel
            direction.normalize();
            origin.add(direction.clone().multiplyScalar(0.00000001))
    
    
            //the c * vector at which there is the next coordinate jump
            let cx,cy,cz;
    
            //store if ascending or descending coordinates
            //either -1 or 1
            let xunit,yunit,zunit;
    
            if(direction.x > 0)
            {
                cx = ((Math.ceil(origin.x) - origin.x)/direction.x);
                xunit = 1;
            }
            else
            {
                cx = ((origin.x-Math.floor(origin.x))/-direction.x);
                xunit = -1;
            }
    
            if(direction.y > 0)
            {
                cy = ((Math.ceil(origin.y) - origin.y)/direction.y);
                yunit = 1;
            }
            else
            {
                cy = ((origin.y-Math.floor(origin.y))/-direction.y);
                yunit = -1;
            }
    
            if(direction.z > 0)
            {
                cz = ((Math.ceil(origin.z) - origin.z)/direction.z);
                zunit = 1;
            }
            else
            {
                cz = ((origin.z-Math.floor(origin.z))/-direction.z);
                zunit = -1;
            }
    
            //get the first voxel coordinate
            let x = Math.floor(origin.x);
            let y = Math.floor(origin.y);
            let z = Math.floor(origin.z);
    
            //get the c unit to jump to the next coordinate
            let cxunit = 1.0/Math.abs(direction.x);
            let cyunit = 1.0/Math.abs(direction.y);
            let czunit = 1.0/Math.abs(direction.z);
    
            //the face hit by the ray
            let face:number;
            //get the first face
            //step back a voxel
            let icx = cx - cxunit;
            let icy = cy - cyunit;
            let icz = cz - czunit;
            //look for the closest voxel coord step (here the biggest since all < 0)
            if(icx >= icy && icx >= icz)face = xunit * 1;
            else if(icy >= icz)face = yunit * 2;
            else face = zunit * 3;
            
    
    
            let voxels : VoxelHit[] = [];
    
            while(x >= 0 && x < this.width && y >= 0 && y < this.height && z >=0 && z < this.depth)
            {
                //push the previous voxel, it is in bounds
                //but only if it is solid
                if(this.voxelsSolid[this.C_getVoxelIndexAt(x,y,z)])
                {
                    voxels.push({voxel:{x:x,y:y,z:z},face:face});
                    if(onlyFirst)
                        return voxels;
                }
                //check which direction is the nex to jump
                if(cx < cy && cx < cz)
                {
                    x += xunit;
                    cx += cxunit;
                    //-1 or 1
                    face = xunit * 1;
                }
                else if(cy < cz)
                {
                    y += yunit;
                    cy += cyunit;
                    //-2 or 2
                    face = yunit * 2;
                }
                else
                {
                    z += zunit;
                    cz += czunit;
                    //-3 or 2
                    face = zunit * 3;
                }
            }
    
            //TEMP!!!
            if(Globals.mouse.left)
            {
                // temp
                for(let i = 0; i < voxels.length; i++)
                {
                    this.setVoxelAt(voxels[i].voxel.x,voxels[i].voxel.y,voxels[i].voxel.z,Globals.colorPalette.selectedColor);
                }
                this.cleanUp();
                //temp
            }
    
            return voxels;
        }*/
        Chunk.prototype.isVoxelVisibleAt = function (x, y, z) {
            return false;
        };
        //get the color from the palette and return it as Vector3
        Chunk.prototype.getColorFromPalette = function (color) {
            //color--;
            return VoxelEditor.Globals.colorPalette.colors[color];
        };
        //called after the chunk has been dirtied to clean it up
        //has a list of tasks to perform/check
        //updates the gpu buffers
        Chunk.prototype.cleanUp = function () {
            var _this = this;
            //go over all voxels to check and checks them
            this.voxelsToCheckAndCull.forEach(function (voxel) {
                _this.checkAndCullVoxel(voxel);
            });
            this.voxelsToCheckAndCull = [];
            if (this.dirty) {
                this.offsets.needsUpdate = true;
                this.colorIndices.needsUpdate = true;
                this.geometry.maxInstancedCount = this.voxelsVisible;
                this.dirty = false;
                //might want to update bounding box
            }
        };
        Chunk.prototype.addVoxelAndNeighboursToCullCheck = function (v) {
            this.voxelsToCheckAndCull.push(v);
        };
        Chunk.prototype.checkAndCullVoxel = function (v) {
            var neighbours = false;
            var g_index = this.G_getVoxelBufferIndexAt(v.x, v.y, v.z);
            if (v.x > 0 && v.x < this.width - 1 && v.y > 0 && v.y < this.height - 1 && v.z > 0 && v.z < this.depth - 1) {
                neighbours = this.C_getVoxelSolidAt(v.x - 1, v.y, v.z) && this.C_getVoxelSolidAt(v.x + 1, v.y, v.z) && this.C_getVoxelSolidAt(v.x, v.y - 1, v.z) && this.C_getVoxelSolidAt(v.x, v.y + 1, v.z) && this.C_getVoxelSolidAt(v.x, v.y, v.z - 1) && this.C_getVoxelSolidAt(v.x, v.y, v.z + 1);
            }
            else {
                //might want to check the neighbour chunks to increase culling even more
            }
            if (neighbours) {
                if (g_index != 0) {
                    this.G_removeVoxelAt(v.x, v.y, v.z);
                }
            }
            else {
                if (g_index == 0) {
                    this.G_addVoxelAt(v.x, v.y, v.z, this.C_getVoxelAt(v.x, v.y, v.z));
                }
            }
        };
        Chunk.prototype.setBoundingBox = function () {
            this.geometry.boundingBox = new THREE.Box3(new THREE.Vector3(0.0, 0.0, 0.0), new THREE.Vector3(this.width, this.height, this.depth));
            //required to prevent too early culling
            this.geometry.boundingSphere = new THREE.Sphere(new THREE.Vector3(this.width / 2.0, this.height / 2.0, this.depth / 2.0), Math.sqrt(this.width * this.width + this.height * this.height + this.depth * this.depth) / 2.0);
        };
        return Chunk;
    }(THREE.Mesh));
    //cube geometry, static because not going to change
    //might give possibility to change that later on to give users the ability to customize voxel shape
    //but boundingbox would need update, as well as exporting, so a LOT of work
    Chunk.verticesArray = [0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1];
    Chunk.vertices = new THREE.BufferAttribute(new Float32Array(Chunk.verticesArray), 3);
    Chunk.indicesArray = [0, 1, 3, 6, 0, 2, 5, 0, 4, 6, 4, 0, 0, 3, 2, 5, 1, 0, 3, 1, 5, 7, 4, 6, 4, 7, 5, 7, 6, 2, 7, 2, 3, 7, 3, 5];
    Chunk.indices = new THREE.BufferAttribute(new Uint16Array(Chunk.indicesArray), 1);
    Chunk.staticMaterial = new THREE.RawShaderMaterial({
        uniforms: {
            colorPalette: { type: 'v3v', value: VoxelEditor.Globals.colorPalette.colors }
        },
        vertexShader: document.getElementById('vertexShader').textContent,
        fragmentShader: document.getElementById('fragmentShader').textContent,
        side: THREE.FrontSide,
        transparent: false
    });
    VoxelEditor.Chunk = Chunk;
})(VoxelEditor || (VoxelEditor = {}));
var VoxelEditor;
(function (VoxelEditor) {
    //an element contains multiple chunks
    //it has a rot + position
    //handles accessing all the voxels in the chunks
    var Element = (function (_super) {
        __extends(Element, _super);
        function Element(width, height, depth, chunkWidth, chunkHeight, chunkDepth) {
            var _this = _super.call(this) || this;
            //will need to sort/insert/delete chunks if Element resized requires different number of chunks
            _this.chunks = [];
            //displacement of chunks
            //needed to convert from element coord system to chunk coord system
            _this.offsetX = 0;
            _this.offsetY = 0;
            _this.offsetZ = 0;
            var cubeGeometry = new THREE.CubeGeometry(10, 10, 10);
            var material = new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: false });
            _this.cube = new THREE.Mesh(cubeGeometry, material);
            _this.add(_this.cube);
            //the models Object3D
            _this.position.set(0, 0, 0);
            //store the chunks preferred dims
            _this.chunkWidth = chunkWidth;
            _this.chunkHeight = chunkHeight;
            _this.chunkDepth = chunkDepth;
            //store the desired dims
            //TODO: alllow for dimensions in voxels, not only chunks
            _this.width = width;
            _this.height = height;
            _this.depth = depth;
            //needed to prevent a WebGL warning
            _this.geometry = new THREE.PlaneGeometry(0, 0);
            _this.material = new THREE.MeshBasicMaterial();
            _this.material.visible = false;
            _this.init();
            VoxelEditor.Globals.scene.add(_this);
            return _this;
        }
        Element.prototype.addChunk = function (width, height, depth, x, y, z) {
            var chunk = new VoxelEditor.Chunk(width, height, depth, x, y, z);
            this.chunks.push(chunk);
            this.add(chunk);
        };
        //can return null
        Element.prototype.getFirstVoxelInRay = function (raycaster) {
            if (this.rayIntersectsBoundingBox(raycaster)) {
                var vox = this.rayToVoxels(null, null);
                if (vox.length > 0)
                    return vox[0].voxel;
            }
            return null;
        };
        Element.prototype.rayIntersectsBoundingBox = function (raycaster) {
            var ray = new THREE.Ray();
            var inverseMatrix = new THREE.Matrix4();
            inverseMatrix.getInverse(this.matrixWorld);
            ray.copy(VoxelEditor.Globals.raycaster.ray).applyMatrix4(inverseMatrix);
            if (ray.intersectsSphere(this.geometry.boundingSphere) !== false) {
                // Check boundingBox before continuing
                if (this.geometry.boundingBox !== null) {
                    return ray.intersectsBox(this.geometry.boundingBox);
                }
            }
            return false;
        };
        Element.prototype.raycast = function (raycaster, intersects) {
            var inverseMatrix = new THREE.Matrix4();
            var ray = new THREE.Ray();
            var matrixWorld = this.matrixWorld;
            var hit = null;
            inverseMatrix.getInverse(matrixWorld);
            ray.copy(raycaster.ray).applyMatrix4(inverseMatrix);
            //allows ray to hit even if inside the chunk
            if (this.geometry.boundingBox !== null && this.geometry.boundingBox.containsPoint(ray.origin)) {
                hit = ray.origin;
            }
            else if (ray.intersectsSphere(this.geometry.boundingSphere) !== false) {
                // Check boundingBox before continuing
                if (this.geometry.boundingBox !== null) {
                    hit = ray.intersectBox(this.geometry.boundingBox);
                }
            }
            if (hit !== null) {
                this.cube.position.set(hit.x, hit.y, hit.z);
                var distance = new THREE.Vector3().subVectors(ray.origin, hit).lengthSq();
                var hitLocal = hit.clone();
                //hit coordinates to world
                hit.applyMatrix4(matrixWorld);
                //TODO: implement THREE.Intersection!!!
                intersects.push({ object: this, distance: distance, point: hit, hitLocal: hitLocal });
                this.rayToVoxels(hitLocal, ray.direction);
            }
        };
        Element.prototype.resize = function () {
            //insert/remove chunks if necezssary
            //setBoundingBox
            //allow to resize on the small and big sides
            //need to check chunksOn{N}
        };
        Element.prototype.init = function () {
            this.chunksOnX = Math.ceil((this.width - 1) / this.chunkWidth) + 1;
            this.chunksOnY = Math.ceil((this.height - 1) / this.chunkHeight) + 1;
            this.chunksOnZ = Math.ceil((this.depth - 1) / this.chunkDepth) + 1;
            for (var x = 0; x < this.chunksOnX; x++) {
                for (var y = 0; y < this.chunksOnY; y++) {
                    for (var z = 0; z < this.chunksOnZ; z++) {
                        this.addChunk(this.chunkWidth, this.chunkHeight, this.chunkDepth, x * this.chunkWidth, y * this.chunkHeight, z * this.chunkDepth);
                    }
                }
            }
            for (var ix = 0; ix < this.width; ix++) {
                for (var iy = 0; iy < this.height; iy++) {
                    for (var iz = 0; iz < this.depth; iz++) {
                        this.setVoxelAt(ix, iy, iz, THREE.Math.randInt(0, 1) * THREE.Math.randInt(1, 256) - 1);
                    }
                }
            }
            this.setBoundingBox();
            this.cleanUp();
        };
        Element.prototype.shift = function (x, y, z) {
            //might want to choose if x > 0 or < 0 depending on the amount to copy
            //make sure values are in range
            x %= this.width;
            y %= this.height;
            z %= this.depth;
            //update offset
            this.offsetX += x;
            this.offsetY += y;
            this.offsetZ += z;
            //still need to copy the voxels crossing the line
            //check if shift an entire chunk (if so, less copy => cheaper)
            //suboptimal
            if (Math.abs(Math.floor(this.offsetX / this.chunkWidth) - Math.floor((this.offsetX + x) / this.chunkWidth)) >= 2) {
                //an entire chunk has been shifted
            }
            //iterate over all chunks to update positions
            for (var _i = 0, _a = this.chunks; _i < _a.length; _i++) {
                var chunk = _a[_i];
                //apply offset
                var cx = chunk.position.x + x;
                var cy = chunk.position.y + y;
                var cz = chunk.position.z + z;
                //wrap position around if needed
                if (cx >= this.width)
                    cx -= (this.width + this.chunkWidth);
                if (cx < -this.chunkWidth)
                    cx += this.width + this.chunkWidth;
                if (cy >= this.height)
                    cy -= (this.height + this.chunkHeight);
                if (cy < -this.chunkHeight)
                    cy += this.height + this.chunkHeight;
                if (cz >= this.depth)
                    cz -= (this.depth + this.chunkDepth);
                if (cz < -this.chunkDepth)
                    cz += this.depth + this.chunkDepth;
                //apply the new position
                chunk.position.set(cx, cy, cz);
            }
        };
        Element.prototype.setBoundingBox = function () {
            var width = this.width;
            var height = this.height;
            var depth = this.depth;
            this.geometry.boundingBox = new THREE.Box3(new THREE.Vector3(0.0, 0.0, 0.0), new THREE.Vector3(width, height, depth));
            //required to prevent too early culling
            this.geometry.boundingSphere = new THREE.Sphere(new THREE.Vector3(width / 2.0, height / 2.0, depth / 2.0), Math.sqrt(width * width + height * height + depth * depth) / 2.0);
        };
        Element.prototype.getVoxelIndexAt = function (x, y, z) {
            x -= this.offsetX;
            y -= this.offsetY;
            z -= this.offsetZ;
            x = VoxelEditor.modulo(x, this.chunksOnX * this.chunkWidth);
            y = VoxelEditor.modulo(y, this.chunksOnY * this.chunkHeight);
            z = VoxelEditor.modulo(z, this.chunksOnZ * this.chunkDepth);
            var chunkX = Math.floor(x / this.chunkWidth);
            var chunkY = Math.floor(y / this.chunkHeight);
            var chunkZ = Math.floor(z / this.chunkDepth);
            x %= this.chunkWidth;
            y %= this.chunkHeight;
            z %= this.chunkDepth;
            var chunk = chunkX * this.chunksOnY * this.chunksOnZ + chunkY * this.chunksOnZ + chunkZ;
            return { chunk: chunk, x: x, y: y, z: z };
        };
        Element.prototype.getVoxelAt = function (x, y, z) {
            var index = this.getVoxelIndexAt(x, y, z);
            return this.chunks[index.chunk].C_getVoxelAt(index.x, index.y, index.z);
        };
        Element.prototype.getVoxelSolidAt = function (x, y, z) {
            var index = this.getVoxelIndexAt(x, y, z);
            return this.chunks[index.chunk].C_getVoxelSolidAt(index.x, index.y, index.z);
        };
        Element.prototype.setVoxelAt = function (x, y, z, color) {
            var index = this.getVoxelIndexAt(x, y, z);
            return this.chunks[index.chunk].setVoxelAt(index.x, index.y, index.z, color);
        };
        //cleans all the chunks
        Element.prototype.cleanUp = function () {
            for (var _i = 0, _a = this.chunks; _i < _a.length; _i++) {
                var chunk = _a[_i];
                chunk.cleanUp();
            }
        };
        Element.prototype.rayToVoxels = function (origin, direction, onlyFirst) {
            if (onlyFirst === void 0) { onlyFirst = false; }
            //to make sure origin is an in bound voxel
            direction.normalize();
            origin.add(direction.clone().multiplyScalar(0.00000001));
            //the c * vector at which there is the next coordinate jump
            var cx, cy, cz;
            //store if ascending or descending coordinates
            //either -1 or 1
            var xunit, yunit, zunit;
            if (direction.x > 0) {
                cx = ((Math.ceil(origin.x) - origin.x) / direction.x);
                xunit = 1;
            }
            else {
                cx = ((origin.x - Math.floor(origin.x)) / -direction.x);
                xunit = -1;
            }
            if (direction.y > 0) {
                cy = ((Math.ceil(origin.y) - origin.y) / direction.y);
                yunit = 1;
            }
            else {
                cy = ((origin.y - Math.floor(origin.y)) / -direction.y);
                yunit = -1;
            }
            if (direction.z > 0) {
                cz = ((Math.ceil(origin.z) - origin.z) / direction.z);
                zunit = 1;
            }
            else {
                cz = ((origin.z - Math.floor(origin.z)) / -direction.z);
                zunit = -1;
            }
            //get the first voxel coordinate
            var x = Math.floor(origin.x);
            var y = Math.floor(origin.y);
            var z = Math.floor(origin.z);
            //get the c unit to jump to the next coordinate
            var cxunit = 1.0 / Math.abs(direction.x);
            var cyunit = 1.0 / Math.abs(direction.y);
            var czunit = 1.0 / Math.abs(direction.z);
            //the face hit by the ray
            var face;
            //get the first face
            //step back a voxel
            var icx = cx - cxunit;
            var icy = cy - cyunit;
            var icz = cz - czunit;
            //look for the closest voxel coord step (here the biggest since all < 0)
            if (icx >= icy && icx >= icz)
                face = xunit * 1;
            else if (icy >= icz)
                face = yunit * 2;
            else
                face = zunit * 3;
            var voxelHits = [];
            while (x >= 0 && x < this.width && y >= 0 && y < this.height && z >= 0 && z < this.depth) {
                //push the previous voxel, it is in bounds
                //but only if it is solid
                if (this.getVoxelSolidAt(x, y, z)) {
                    voxelHits.push({ voxel: { x: x, y: y, z: z }, face: face });
                    if (onlyFirst)
                        return voxelHits;
                }
                //check which direction is the nex to jump
                if (cx < cy && cx < cz) {
                    x += xunit;
                    cx += cxunit;
                    //-1 or 1
                    face = xunit * 1;
                }
                else if (cy < cz) {
                    y += yunit;
                    cy += cyunit;
                    //-2 or 2
                    face = yunit * 2;
                }
                else {
                    z += zunit;
                    cz += czunit;
                    //-3 or 2
                    face = zunit * 3;
                }
            }
            //TEMP!!!
            if (VoxelEditor.Globals.mouse.left) {
                // temp
                for (var _i = 0, voxelHits_1 = voxelHits; _i < voxelHits_1.length; _i++) {
                    var hit = voxelHits_1[_i];
                    this.setVoxelAt(hit.voxel.x, hit.voxel.y, hit.voxel.z, VoxelEditor.Globals.colorPalette.selectedColor);
                }
                this.cleanUp();
                //temp
            }
            return voxelHits;
        };
        return Element;
    }(THREE.Mesh));
    VoxelEditor.Element = Element;
})(VoxelEditor || (VoxelEditor = {}));
var VoxelEditor;
(function (VoxelEditor) {
    var movementSpeed = 10;
    var cameraPitch = 0;
    var cameraYaw = 0;
    var invertPitch = true;
    var invertYaw = true;
    var pitchSensivity = 1;
    var yawSensivity = 1;
    function onContainerKeyboard(event) {
        //event.preventDefault();
        //left
        var mov = new THREE.Vector3(0, 0, 0);
        if (event.key == 'q') {
            mov.add(new THREE.Vector3(-1, 0, 0));
        }
        //right
        if (event.key == 'd') {
            mov.add(new THREE.Vector3(1, 0, 0));
        }
        //up
        if (event.key == 'a') {
            mov.add(new THREE.Vector3(0, 1, 0));
        }
        //down
        if (event.key == 'e') {
            mov.add(new THREE.Vector3(0, -1, 0));
        }
        //forward
        if (event.key == 'z') {
            mov.add(new THREE.Vector3(0, 0, -1));
        }
        //backward
        if (event.key == 's') {
            mov.add(new THREE.Vector3(0, 0, 1));
        }
        mov.multiplyScalar(movementSpeed);
        mov.applyEuler(VoxelEditor.Globals.camera.rotation);
        VoxelEditor.Globals.camera.position.add(mov);
    }
    VoxelEditor.onContainerKeyboard = onContainerKeyboard;
    function onContainerMouseWheel(event) {
        //event.preventDefault();
        /*let zoom = -event.wheelDelta/120 * 4;
        camera.fov += zoom;
        camera.fov=clamp(camera.fov,1,170);
        camera.updateProjectionMatrix();*/
        var mov = new THREE.Vector3(0, 0, -event.wheelDelta / 120 * 40);
        mov.applyEuler(VoxelEditor.Globals.camera.rotation);
        VoxelEditor.Globals.camera.position.add(mov);
    }
    VoxelEditor.onContainerMouseWheel = onContainerMouseWheel;
    //TODO:
    //when mouse pressed and in container border, keep moving
    function onContainerMouseMove(event) {
        event.preventDefault();
        //middle mouse button
        //move camera left/right up/down
        if (event.buttons & 4) {
            var mov = new THREE.Vector3(-(event.movementX), event.movementY, 0);
            mov.applyEuler(VoxelEditor.Globals.camera.rotation);
            VoxelEditor.Globals.camera.position.add(mov);
        }
        //right mouse button
        //rotate camera
        if (event.buttons & 2) {
            cameraPitch = VoxelEditor.clamp(cameraPitch + event.movementY / 100.0 * pitchSensivity * (invertPitch ? -1 : 1), -Math.PI / 2, Math.PI / 2);
            cameraYaw += event.movementX / 100.0 * yawSensivity * (invertYaw ? -1 : 1);
            VoxelEditor.Globals.camera.setRotationFromEuler(new THREE.Euler(cameraPitch, cameraYaw, 0, 'YXZ'));
        }
        //left mouse button
        //move camera forward/backwards
        /*if(event.buttons & 1) {
            let move = new THREE.Vector3( 0,0, -event.movementX + event.movementY);
            move.applyEuler(camera.rotation);
            camera.position.add(move);
        }*/
        VoxelEditor.Globals.mouse.setPos((event.offsetX / VoxelEditor.Globals.renderer.domElement.width) * 2 - 1, -(event.offsetY / VoxelEditor.Globals.renderer.domElement.height) * 2 + 1);
        VoxelEditor.ToolSection.instance.mouseMove();
        //raycastHover();
        VoxelEditor.Globals.stage.raycast();
    }
    VoxelEditor.onContainerMouseMove = onContainerMouseMove;
    function onContainerMouseDown(event) {
        if (event.button == 0)
            VoxelEditor.Globals.mouse.left = true;
        else if (event.button == 1)
            VoxelEditor.Globals.mouse.middle = true;
        else if (event.button == 2)
            VoxelEditor.Globals.mouse.right = true;
    }
    VoxelEditor.onContainerMouseDown = onContainerMouseDown;
    function onContainerMouseUp(event) {
        if (event.button == 0)
            VoxelEditor.Globals.mouse.left = false;
        else if (event.button == 1)
            VoxelEditor.Globals.mouse.middle = false;
        else if (event.button == 2)
            VoxelEditor.Globals.mouse.right = false;
    }
    VoxelEditor.onContainerMouseUp = onContainerMouseUp;
    function setupInputs() {
        VoxelEditor.Globals.renderer.domElement.addEventListener("mousedown", onContainerMouseDown);
        VoxelEditor.Globals.renderer.domElement.addEventListener("mouseup", onContainerMouseUp);
        VoxelEditor.Globals.renderer.domElement.addEventListener("mousemove", onContainerMouseMove);
        VoxelEditor.Globals.renderer.domElement.addEventListener("mousewheel", onContainerMouseWheel);
        //needs to be window to get key events
        window.onkeydown = onContainerKeyboard;
        //disable context menu in container
        VoxelEditor.Globals.renderer.domElement.addEventListener("contextmenu", function (e) {
            e.preventDefault();
        }, false);
    }
    VoxelEditor.setupInputs = setupInputs;
})(VoxelEditor || (VoxelEditor = {}));
var VoxelEditor;
(function (VoxelEditor) {
    //a model can contain multiple elements
    //it has a rot + position
    //not sure about chunkWidth/height..
    var Model = (function (_super) {
        __extends(Model, _super);
        function Model(chunkWidth, chunkHeight, chunkDepth) {
            var _this = _super.call(this) || this;
            _this.elements = [];
            _this.chunkWidth = chunkWidth;
            _this.chunkHeight = chunkHeight;
            _this.chunkDepth = chunkDepth;
            _this.init();
            VoxelEditor.Globals.scene.add(_this);
            return _this;
        }
        Model.prototype.addElement = function (width, height, depth, x, y, z) {
            var element = new VoxelEditor.Element(width, height, depth, this.chunkWidth, this.chunkHeight, this.chunkHeight);
            element.position.set(x, y, z);
            this.elements.push(element);
            this.add(element);
        };
        Model.prototype.rayIntersectsBoundingBox = function (raycaster) {
            var ray = new THREE.Ray();
            var inverseMatrix = new THREE.Matrix4();
            inverseMatrix.getInverse(this.matrixWorld);
            ray.copy(VoxelEditor.Globals.raycaster.ray).applyMatrix4(inverseMatrix);
            if (ray.intersectsSphere(this.geometry.boundingSphere) !== false) {
                // Check boundingBox before continuing
                if (this.geometry.boundingBox !== null) {
                    return ray.intersectsBox(this.geometry.boundingBox);
                }
            }
            return false;
        };
        Model.prototype.raycast = function () {
            if (this.rayIntersectsBoundingBox(VoxelEditor.Globals.raycaster) || true) {
                var intersects = VoxelEditor.Globals.raycaster.intersectObjects(this.elements);
            }
        };
        Model.prototype.init = function () {
            this.addElement(16, 32, 32, 0, 0, 0);
            this.elements[0].rotateX(1);
            this.addElement(64, 64, 64, 128, 0, 0);
            this.addElement(32, 32, 32, 0, 0, 64);
            this.setBoundingBox();
        };
        Model.prototype.setBoundingBox = function () {
            var box = new THREE.Box3();
            box.setFromObject(this);
            this.geometry.boundingBox = box;
            var width = box.max.x - box.min.x;
            var height = box.max.y - box.min.y;
            var depth = box.max.z - box.min.z;
            //required to prevent too early culling
            this.geometry.boundingSphere = new THREE.Sphere(new THREE.Vector3(width / 2.0, height / 2.0, depth / 2.0), Math.sqrt(width * width + height * height + depth * depth) / 2.0);
        };
        return Model;
    }(THREE.Mesh));
    VoxelEditor.Model = Model;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="Mouse.ts" />
/// <reference path="ColorPalette.ts" />
/// <reference path="ColorInput.ts" />
/// <reference path="Utils.ts" />
/// <reference path="Model.ts" />
/// <reference path="Stage.ts" />
/// <reference path="Globals.ts" />
/// <reference path="Inputs.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var stats;
    var container = document.getElementById('container');
    /*let CUBEvertices = new Float32Array( [
        -1.0,-1.0,-1.0,
        -1.0,-1.0, 1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0,-1.0,
        -1.0,-1.0,-1.0,
        -1.0, 1.0,-1.0,
        1.0,-1.0, 1.0,
        -1.0,-1.0,-1.0,
        1.0,-1.0,-1.0,
        1.0, 1.0,-1.0,
        1.0,-1.0,-1.0,
        -1.0,-1.0,-1.0,
        -1.0,-1.0,-1.0,
        -1.0, 1.0, 1.0,
        -1.0, 1.0,-1.0,
        1.0,-1.0, 1.0,
        -1.0,-1.0, 1.0,
        -1.0,-1.0,-1.0,
        -1.0, 1.0, 1.0,
        -1.0,-1.0, 1.0,
        1.0,-1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0,-1.0,-1.0,
        1.0, 1.0,-1.0,
        1.0,-1.0,-1.0,
        1.0, 1.0, 1.0,
        1.0,-1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0,-1.0,
        -1.0, 1.0,-1.0,
        1.0, 1.0, 1.0,
        -1.0, 1.0,-1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        -1.0, 1.0, 1.0,
        1.0,-1.0, 1.0
    ] );*/
    function init() {
        // on initialise le moteur de rendu
        VoxelEditor.Globals.renderer = new THREE.WebGLRenderer();
        if (VoxelEditor.Globals.renderer.extensions.get('ANGLE_instanced_arrays') === false) {
            console.log("NOT SUPPORTED");
            return;
        }
        // si WebGL ne fonctionne pas sur votre navigateur vous pouvez utiliser le moteur de rendu Canvas à la place
        // renderer = new THREE.CanvasRenderer();
        VoxelEditor.Globals.renderer.setClearColor(0x101010);
        VoxelEditor.Globals.renderer.setSize(container.offsetWidth, container.offsetHeight);
        //useless?
        //renderer.setFaceCulling(THREE.CullFaceFront,THREE.FrontFaceDirectionCw);
        container.appendChild(VoxelEditor.Globals.renderer.domElement);
        // on initialise la camera que l’on place ensuite sur la scène
        VoxelEditor.Globals.camera = new THREE.PerspectiveCamera(50, container.offsetWidth / container.offsetHeight, 1, 10000);
        VoxelEditor.Globals.camera.position.set(0, 0, 1000);
        VoxelEditor.Globals.scene.add(VoxelEditor.Globals.camera);
        // on créé un  cube au quel on définie un matériau puis on l’ajoute à la scène
        var cubeGeometry = new THREE.CubeGeometry(100, 100, 100);
        var wireframeMaterial = new THREE.MeshPhongMaterial({ color: 0xff0000, wireframe: false });
        var cubeMesh = new THREE.Mesh(cubeGeometry, wireframeMaterial);
        //scene.add( cubeMesh );
        var planeGeometry = new THREE.PlaneGeometry(2000, 2000, 20, 20);
        var planeMaterial = new THREE.MeshPhongMaterial({ color: 0x0000ff, wireframe: true });
        var planeMeshBack = new THREE.Mesh(planeGeometry, planeMaterial);
        planeMeshBack.position.set(0, 0, -1000);
        planeMeshBack.setRotationFromAxisAngle(new THREE.Vector3(0, 0, 1), Math.PI);
        VoxelEditor.Globals.scene.add(planeMeshBack);
        var planeMeshRight = new THREE.Mesh(planeGeometry, planeMaterial);
        planeMeshRight.position.set(1000, 0, 0);
        planeMeshRight.setRotationFromAxisAngle(new THREE.Vector3(0, 1, 0), -Math.PI / 2);
        VoxelEditor.Globals.scene.add(planeMeshRight);
        var planeMeshBottom = new THREE.Mesh(planeGeometry, planeMaterial);
        planeMeshBottom.position.set(0, -1000, 0);
        planeMeshBottom.setRotationFromAxisAngle(new THREE.Vector3(1, 0, 0), -Math.PI / 2);
        VoxelEditor.Globals.scene.add(planeMeshBottom);
        //lights
        var ambientLight = new THREE.AmbientLight(0xA0A080);
        VoxelEditor.Globals.scene.add(ambientLight);
        var directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
        directionalLight.position.set(500, 500, 500);
        VoxelEditor.Globals.scene.add(directionalLight);
        stats = new Stats();
        //one or the other works, depending on Stats version?
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.top = '0px';
        stats.domElement.style.right = '0px';
        stats.domElement.style = "position:absolute;right:0px;top:0px;";
        container.appendChild(stats.domElement);
        //the more individual chunks, the more time intensive (bottleneck transfer of data?)
        //big chunks better?
        VoxelEditor.Globals.stage.addModel(32, 32, 32);
        // on effectue le rendu de la scène
        window.requestAnimationFrame(render);
    }
    //worst case scenario: >12 million cubes (>2/3 space filled so no cubes are culled(2 full layers, 1 air, 2 ful, 1 air....) + some on the edges) if terrain is 256*256*256
    //problem: only <10 FPS
    //maybe if subdivided in chunks FPS boost?
    //if not, might enable to cull entire chunks (definitely able to cull what is off-screen, so that's a huge+)
    function createInstancedCubes(x, y, z, pos) {
        var triangles = 12;
        var instances = x * y * z;
        var geometry = new THREE.InstancedBufferGeometry();
        geometry.maxInstancedCount = instances;
        var verticesArray = new Float32Array([0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1]);
        var vertices = new THREE.BufferAttribute(verticesArray, 3);
        geometry.addAttribute('position', vertices);
        var indicesArray = new Uint16Array([0, 1, 3, 6, 0, 2, 5, 0, 4, 6, 4, 0, 0, 3, 2, 5, 1, 0, 3, 1, 5, 7, 4, 6, 4, 7, 5, 7, 6, 2, 7, 2, 3, 7, 3, 5]);
        geometry.setIndex(new THREE.BufferAttribute(indicesArray, 1));
        var offsets = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        var i = 0;
        for (var ix = 0; ix < x; ix++) {
            for (var iy = 0; iy < y; iy++) {
                for (var iz = 0; iz < z; iz++) {
                    //ix*y*z+iy*z+iz
                    offsets.setXYZ(i, ix, iy, iz);
                    i++;
                }
            }
        }
        geometry.addAttribute('offset', offsets);
        var colors = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        for (var i_1 = 0, ul = colors.count; i_1 < ul; i_1++) {
            var r = Math.random();
            var g = Math.random();
            var b = Math.random();
            colors.setXYZ(i_1, r, g, b);
        }
        geometry.addAttribute('color', colors);
        // material
        var material = new THREE.RawShaderMaterial({
            /*uniforms: {
                time: { value: 1.0 },
                sineTime: { value: 1.0 }
            },*/
            vertexShader: document.getElementById('vertexShader').textContent,
            fragmentShader: document.getElementById('fragmentShader').textContent,
            side: THREE.BackSide,
            transparent: false
        });
        var mesh = new THREE.Mesh(geometry, material);
        mesh.geometry.computeBoundingSphere();
        VoxelEditor.Globals.scene.add(mesh);
        mesh.position.set(pos.x, pos.y, pos.z);
    }
    //probably only worth it if divides triangles by over 6(could happen if a lot of cubes have 4 neighbours(difficult))
    //maybe optimize rotation code?
    //maybe one buffer per rotation?
    //one issue is the duplication of data: color, offset -> instancedBufferAttribute?
    //not worth investigating right now, maybe later when I try to squeeze every drop of power of the gpu
    function createInstancedTriangles(x, y, z, pos) {
        var triangles = 12;
        var instances = x * y * z * 6;
        var geometry = new THREE.InstancedBufferGeometry();
        geometry.maxInstancedCount = instances;
        var verticesArray = new Float32Array([-1, -1, -1, -1, -1, 1, -1, 1, -1, -1, -1, 1, -1, 1, 1, -1, 1, -1]);
        var vertices = new THREE.BufferAttribute(verticesArray, 3);
        geometry.addAttribute('position', vertices);
        var offsets = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        var i = 0;
        for (var ix = 0; ix < x; ix++) {
            for (var iy = 0; iy < y; iy++) {
                for (var iz = 0; iz < z; iz++) {
                    //ix*y*z+iy*z+iz
                    offsets.setXYZ(i, ix, iy, iz);
                    offsets.setXYZ(i + 1, ix, iy, iz);
                    offsets.setXYZ(i + 2, ix, iy, iz);
                    offsets.setXYZ(i + 3, ix, iy, iz);
                    offsets.setXYZ(i + 4, ix, iy, iz);
                    offsets.setXYZ(i + 5, ix, iy, iz);
                    i += 6;
                }
            }
        }
        geometry.addAttribute('offset', offsets);
        var colors = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        for (var i_2 = 0, ul = colors.count; i_2 < ul; i_2 += 6) {
            var r = Math.random();
            var g = Math.random();
            var b = Math.random();
            colors.setXYZ(i_2, r, g, b);
            colors.setXYZ(i_2 + 1, r, g, b);
            colors.setXYZ(i_2 + 2, r, g, b);
            colors.setXYZ(i_2 + 3, r, g, b);
            colors.setXYZ(i_2 + 4, r, g, b);
            colors.setXYZ(i_2 + 5, r, g, b);
        }
        geometry.addAttribute('color', colors);
        var rotations = new THREE.InstancedBufferAttribute(new Float32Array(instances * 3), 3, 1);
        for (var i_3 = 0, ul = rotations.count; i_3 < ul; i_3 += 6) {
            rotations.setXYZ(i_3, 0, 0, 0);
            rotations.setXYZ(i_3 + 1, 1, 0, 0);
            rotations.setXYZ(i_3 + 2, 0, 1, 0);
            rotations.setXYZ(i_3 + 3, 0, 0, 1);
            rotations.setXYZ(i_3 + 4, 0, 0, 0);
            rotations.setXYZ(i_3 + 5, 0, 0, 0);
        }
        geometry.addAttribute('rotation', rotations);
        // material
        var material = new THREE.RawShaderMaterial({
            /*uniforms: {
                time: { value: 1.0 },
                sineTime: { value: 1.0 }
            },*/
            vertexShader: document.getElementById('vertexShaderTriangles').textContent,
            fragmentShader: document.getElementById('fragmentShader').textContent,
            side: THREE.BackSide,
            transparent: false
        });
        var mesh = new THREE.Mesh(geometry, material);
        mesh.geometry.computeBoundingSphere();
        VoxelEditor.Globals.scene.add(mesh);
        mesh.position.set(pos.x, pos.y, pos.z);
    }
    function render() {
        //console.time("renderingTheGame");
        VoxelEditor.Globals.renderer.render(VoxelEditor.Globals.scene, VoxelEditor.Globals.camera);
        //console.timeEnd("renderingTheGame");
        requestAnimationFrame(render);
        stats.update();
    }
    init();
    VoxelEditor.setupInputs();
    window.requestAnimationFrame(render);
})(VoxelEditor || (VoxelEditor = {}));
//might want to bake the HTML for the native Sections and just fetch priority
//or maybe not, because it looses a lot of flexibility
var VoxelEditor;
(function (VoxelEditor) {
    var Section = (function () {
        function Section() {
            this.priority = 0;
        }
        return Section;
    }());
    VoxelEditor.Section = Section;
    var SectionContainer = (function (_super) {
        __extends(SectionContainer, _super);
        function SectionContainer() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.sections = [];
            return _this;
        }
        SectionContainer.prototype.addSection = function (section) {
            this.sections.push(section);
            this.domElement.appendChild(section.domElement);
            //needs optimisation maybe to only add elements all at once?
            this.sortSections();
        };
        SectionContainer.prototype.sortSections = function () {
            var _this = this;
            this.sections.sort(function (a, b) { return a.priority - b.priority; });
            this.sections.forEach(function (section) { _this.domElement.appendChild(section.domElement); });
        };
        return SectionContainer;
    }(Section));
    VoxelEditor.SectionContainer = SectionContainer;
    var SectionSelectableContainer = (function (_super) {
        __extends(SectionSelectableContainer, _super);
        function SectionSelectableContainer() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.selectedSection = null;
            return _this;
        }
        SectionSelectableContainer.prototype.addSection = function (section) {
            var _this = this;
            _super.prototype.addSection.call(this, section);
            this.selectSection(this.sections[0]);
            section.domElement.onclick = function () { _this.selectSection(section); };
        };
        SectionSelectableContainer.prototype.selectSection = function (section) {
            //first remove then add, so if brush allready selected, no problem
            if (this.selectedSection !== null)
                this.selectedSection.domElement.classList.remove("sectionSelected");
            section.domElement.classList.add("sectionSelected");
            this.selectedSection = section;
        };
        return SectionSelectableContainer;
    }(SectionContainer));
    VoxelEditor.SectionSelectableContainer = SectionSelectableContainer;
    var SectionRegisterer = (function (_super) {
        __extends(SectionRegisterer, _super);
        function SectionRegisterer() {
            var _this = _super.call(this) || this;
            _this.domElement = document.getElementById("sections");
            return _this;
        }
        SectionRegisterer.prototype.registerSection = function (section) {
            this.addSection(section);
        };
        return SectionRegisterer;
    }(SectionContainer));
    SectionRegisterer.instance = new SectionRegisterer();
    VoxelEditor.SectionRegisterer = SectionRegisterer;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="SectionRegisterer.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var Brush = (function (_super) {
        __extends(Brush, _super);
        function Brush() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Brush.prototype.getVoxels = function () { };
        ;
        return Brush;
    }(VoxelEditor.Section));
    VoxelEditor.Brush = Brush;
    var BrushSection = (function (_super) {
        __extends(BrushSection, _super);
        function BrushSection() {
            var _this = _super.call(this) || this;
            _this.sections = [];
            _this.priority = 10;
            _this.domElement = document.createElement("div");
            _this.domElement.textContent = "BrushSection";
            VoxelEditor.SectionRegisterer.instance.registerSection(_this);
            return _this;
        }
        BrushSection.prototype.registerBrush = function (brush) {
            this.addSection(brush);
        };
        return BrushSection;
    }(VoxelEditor.SectionSelectableContainer));
    BrushSection.instance = new BrushSection();
    VoxelEditor.BrushSection = BrushSection;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="SectionRegisterer.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var Modifier = (function () {
        function Modifier() {
        }
        return Modifier;
    }());
    VoxelEditor.Modifier = Modifier;
    var ModifierSection = (function (_super) {
        __extends(ModifierSection, _super);
        function ModifierSection() {
            var _this = _super.call(this) || this;
            _this.modifiers = [];
            _this.priority = 30;
            _this.domElement = document.createElement("div");
            _this.domElement.textContent = "ModifierSection";
            VoxelEditor.SectionRegisterer.instance.registerSection(_this);
            return _this;
        }
        ModifierSection.prototype.registerModifier = function (modifier) {
            this.modifiers.push(modifier);
            this.domElement.appendChild(modifier.domElement);
        };
        return ModifierSection;
    }(VoxelEditor.Section));
    ModifierSection.instance = new ModifierSection();
    VoxelEditor.ModifierSection = ModifierSection;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="SectionRegisterer.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var Option = (function () {
        function Option() {
        }
        return Option;
    }());
    VoxelEditor.Option = Option;
    var OptionSection = (function (_super) {
        __extends(OptionSection, _super);
        function OptionSection() {
            var _this = _super.call(this) || this;
            _this.option = [];
            _this.priority = 40;
            _this.domElement = document.createElement("div");
            _this.domElement.textContent = "OptionSection";
            VoxelEditor.SectionRegisterer.instance.registerSection(_this);
            return _this;
        }
        OptionSection.prototype.registerBrush = function (option) {
            this.option.push(option);
            this.domElement.appendChild(option.domElement);
        };
        return OptionSection;
    }(VoxelEditor.Section));
    OptionSection.instance = new OptionSection();
    VoxelEditor.OptionSection = OptionSection;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="SectionRegisterer.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var Tool = (function () {
        function Tool() {
            this.priority = 0;
        }
        Tool.prototype.apply = function (elt, coords) { };
        ;
        return Tool;
    }());
    VoxelEditor.Tool = Tool;
    var ToolSection = (function (_super) {
        __extends(ToolSection, _super);
        function ToolSection() {
            var _this = _super.call(this) || this;
            _this.sections = [];
            _this.priority = 20;
            _this.domElement = document.createElement("div");
            _this.domElement.textContent = "ToolSection";
            VoxelEditor.SectionRegisterer.instance.registerSection(_this);
            return _this;
        }
        ToolSection.prototype.registerTool = function (tool) {
            _super.prototype.addSection.call(this, tool);
        };
        ToolSection.prototype.mouseMove = function () {
            VoxelEditor.Globals.raycaster.setFromCamera(VoxelEditor.Globals.mouse.position, VoxelEditor.Globals.camera);
            // calculate objects intersecting the picking ray
            var intersects = VoxelEditor.Globals.raycaster.intersectObjects(VoxelEditor.Globals.stage.models);
        };
        ToolSection.prototype.getSelectedTool = function () {
            return this.selectedSection;
        };
        return ToolSection;
    }(VoxelEditor.SectionSelectableContainer));
    ToolSection.instance = new ToolSection();
    VoxelEditor.ToolSection = ToolSection;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="../BrushSection.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var Face = (function (_super) {
        __extends(Face, _super);
        function Face() {
            var _this = _super.call(this) || this;
            _this.priority = 20;
            _this.domElement = document.createElement("button");
            _this.domElement.textContent = "Face";
            VoxelEditor.BrushSection.instance.registerBrush(_this);
            return _this;
        }
        return Face;
    }(VoxelEditor.Brush));
    Face.instance = new Face();
    VoxelEditor.Face = Face;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="../BrushSection.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var SingleVoxel = (function (_super) {
        __extends(SingleVoxel, _super);
        function SingleVoxel() {
            var _this = _super.call(this) || this;
            _this.priority = 10;
            _this.domElement = document.createElement("button");
            _this.domElement.textContent = "SingleVoxel";
            VoxelEditor.BrushSection.instance.registerBrush(_this);
            return _this;
        }
        return SingleVoxel;
    }(VoxelEditor.Brush));
    SingleVoxel.instance = new SingleVoxel();
    VoxelEditor.SingleVoxel = SingleVoxel;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="../ModifierSection.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var Mirror = (function (_super) {
        __extends(Mirror, _super);
        function Mirror() {
            var _this = _super.call(this) || this;
            _this.domElement = document.createElement("input");
            _this.domElement.type = "radio";
            //this.domElement.textContent = "Mirror";
            VoxelEditor.ModifierSection.instance.registerModifier(_this);
            return _this;
        }
        return Mirror;
    }(VoxelEditor.Modifier));
    Mirror.instance = new Mirror();
    VoxelEditor.Mirror = Mirror;
})(VoxelEditor || (VoxelEditor = {}));
/// <reference path="../ToolSection.ts" />
var VoxelEditor;
(function (VoxelEditor) {
    var Eraser = (function (_super) {
        __extends(Eraser, _super);
        function Eraser() {
            var _this = _super.call(this) || this;
            _this.domElement = document.createElement("button");
            _this.domElement.textContent = "Eraser";
            VoxelEditor.ToolSection.instance.registerTool(_this);
            return _this;
        }
        Eraser.prototype.apply = function (elt, coords) {
            elt.setVoxelAt(coords.x, coords.y, coords.z, -1);
        };
        return Eraser;
    }(VoxelEditor.Tool));
    Eraser.instance = new Eraser();
    VoxelEditor.Eraser = Eraser;
})(VoxelEditor || (VoxelEditor = {}));
//# sourceMappingURL=main.js.map